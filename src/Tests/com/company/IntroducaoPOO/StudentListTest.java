package com.company.IntroducaoPOO;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentListTest {

    @Test
    public void createStudentList() {
        StudentList stList = new StudentList();
        Student[] result = stList.toArray();

        assertEquals(0, result.length);
    }

    @Test
    public void createStudentListWithSomeElements(){
        Student student1 = new Student("Sampaio", 1200145);
        Student student2 = new Student("Moreira", 1200054);
        Student student3 = new Student("Silva", 1200086);

        Student[] students = {student1, student2, student3};
        Student[] expected = {student1, student2, student3};

        StudentList stList = new StudentList(students);
        students[2] = student1;
        Student[] result = stList.toArray();

        assertArrayEquals(expected, result);
        assertNotSame(students, result);
    }

    @Test
    public void sortByGradeDescWithSeveralElementsIncorrectlyOrdered(){
        Student student1 = new Student("Sampaio", 1200145);
        student1.doEvaluation(12);
        Student student2 = new Student("Moreira", 1200054);
        student2.doEvaluation(17);
        Student student3 = new Student("Silva", 1200086);
        student3.doEvaluation(15);

        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student3, student1};

        studentList.sortStudentByGradeDesc();
        Student[] result = studentList.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    public void testeToAddWhenNotEmptyButDifferent(){
        Student student1 = new Student("Moreira", 1200054);
        Student student2 = new Student( "Sampaio", 1200145);
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        boolean result = stList.addStudent(student2);
        Student[] content = stList.toArray();

        Assert.assertTrue(result);
        Assert.assertEquals(2,content.length);
    }

    @Test
    public void addIngSameStudentTwice(){
        Student student1 = new Student( "Moreira", 1200054);
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        boolean result = stList.addStudent(student1);
        Student[] content = stList.toArray();

        Assert.assertFalse(result);
        Assert.assertEquals(1,content.length);
    }

    @Test
    public void addIngStudentWithSameNumber(){
        Student student1 = new Student( "Moreira", 1200054);
        Student student2 = new Student( "Sampaio", 1200054);
        StudentList stList = new StudentList();
        stList.addStudent(student1);

        boolean result = stList.addStudent(student2);
        Student[] content = stList.toArray();

        Assert.assertFalse(result);
        Assert.assertEquals(1,content.length);
    }
}