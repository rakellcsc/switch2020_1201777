package com.company.IntroducaoPOO;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentTest {

    @Test
    public void createValidStudent() {
        Student student = new Student("Paulo", 1078673);
        Assert.assertNotNull(student);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createInvalidStudentWithNumberInvalid() {
        new Student("Paulo", 178673);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createInvalidStudentWithNameInvalid() {
        new Student("RUI", 1786730);
    }

    @Test
    public void doEvaluation() {
        Student student = new Student("Raquel", 1456234);
        int expected = 15;
        student.doEvaluation(15);
        int result = student.getGrade();
        assertEquals(expected, result);
    }

    @Test
    public void testEqualsTrue() {
        Student student1 = new Student("Raquel", 1456234);
        Student student2= new Student("Raquel Camacho", 1456234);

        boolean result = student1.equals(student2);

        assertTrue(result);
    }

    @Test
    public void testEqualsFalse() {
        Student student1 = new Student("Raquel", 1456234);

        boolean result = student1.equals(null);

        assertFalse(result);
    }

    @Test
    public void testEqualsFalseDueToDifferentType() {
        Student student1 = new Student("Raquel", 1456234);

        boolean result = student1.equals(new String ("1456234"));

        assertFalse(result);
    }

    @Test
    public void testEqualsFalseDueToDifferentNumbers() {
        Student student1 = new Student("Raquel", 1456234);
        Student student2= new Student("Raquel Camacho", 1456235);

        boolean result = student1.equals(student2);

        assertFalse(result);
    }




    /*@Test
    public void sortStudentByNumberAsc() {
        Student student1 = new Student("Sampaio", 1200145);
        Student student2 = new Student("Moreira", 1200054);
        Student student3 = new Student("Silva", 1200186);

        Student[] students = {student1, student2, student3};
        Student[] expected = {student2, student1, student3};

        boolean result = Student.sortStudentByNumberAsc(students);

        Assert.assertTrue(result);
        Assert.assertArrayEquals(expected, students);
    }

    @Test
    public void sortStudentByGradeDesc() {
        Student student1 = new Student("Sampaio", 1200145);
        student1.doEvaluation(9);
        Student student2 = new Student("Moreira", 1200054);
        student2.doEvaluation(16);
        Student student3 = new Student("Silva", 1200186);
        student3.doEvaluation(10);

        Student[] students = {student1, student2, student3};
        Student[] expected = {student2, student3, student1};

        boolean result = Student.sortStudentByGradeDesc(students);

        Assert.assertTrue(result);

        Assert.assertArrayEquals(students, expected);
    }
     */
}