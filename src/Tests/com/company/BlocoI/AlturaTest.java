package com.company.BlocoI;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AlturaTest {

    @Test
    public void testeParaCalcularAlturaPredio() {
        double tempo = 2;
        double expected = 19.6;
        Altura altura = new Altura(tempo);
        double result = altura.calculaAlturaPredio();
        assertEquals(expected, result, 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeParaExcecaoSeTempoEInvalido_V1() {
        double tempo = -5;
        new Altura(tempo);
    }

    @Test
    public void testeParaExcecaoSeTempoEInvalido_V2() {
        double tempo = -5;
        assertThrows(IllegalArgumentException.class, () -> {
            new Altura(tempo);
        });
    }
}