package com.company.BlocoI;

import org.junit.Test;

import static org.junit.Assert.*;

public class BlocoITest {

    @Test
    public void exercicio03_V1() {
        double raio = 3;
        double altura = 6;

        double expected = 169646;

        double result = BlocoI.exercicio03(raio, altura);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio03_V2() {
        double raio = 2.5;
        double altura = 10.6;

        double expected = 208130.51;

        double result = BlocoI.exercicio03(raio, altura);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio03_V3() {
        double raio = 6;
        double altura = 3.2;

        double expected = 5;

        double result = BlocoI.exercicio03(raio, altura);

        assertNotEquals(expected, result, 0.01);
    }


    @Test
    public void exercicio04_V1() {
        double tempo = 3;

        double expected = 1020;

        double result = BlocoI.exercicio04(tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio04_V2() {
        double tempo = 6;

        double expected = 2040;

        double result = BlocoI.exercicio04(tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio04_V3() {
        double tempo = 2;

        double expected = 1000000;

        double result = BlocoI.exercicio04(tempo);

        assertNotEquals(expected, result, 0.01);
    }


    @Test
    public void exercicio05_V1() {
        double tempo = 2;

        double expected = 19.6;

        double result = BlocoI.exercicio05(tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio05_V2() {
        double tempo = 5;

        double expected = 122.5;

        double result = BlocoI.exercicio05(tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio05_V3() {
        double tempo = 1;

        double expected = 30;

        double result = BlocoI.exercicio05(tempo);

        assertNotEquals(expected, result, 0.01);
    }


    @Test
    public void exercicio06_V1() {
        double alturaPessoa = 2;
        double sombraPessoa = 4;
        double sombraEdificio = 40;

        double expected = 20;

        double result = BlocoI.exercicio06(alturaPessoa, sombraPessoa, sombraEdificio);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio06_V2() {
        double alturaPessoa = 1.6;
        double sombraPessoa = 3.5;
        double sombraEdificio = 30;

        double expected = 13.72;

        double result = BlocoI.exercicio06(alturaPessoa, sombraPessoa, sombraEdificio);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio06_V3() {
        double alturaPessoa = 1.7;
        double sombraPessoa = 5;
        double sombraEdificio = 50;

        double expected = 100;

        double result = BlocoI.exercicio06(alturaPessoa, sombraPessoa, sombraEdificio);

        assertNotEquals(expected, result, 0.01);
    }


    @Test
    public void exercicio07_V1() {
        double velocidade = 10.455;
        double horas = 4;
        double minutos = 2;
        double segundos = 10;

        double expected = 42.195;

        double result = BlocoI.exercicio07(velocidade,horas, minutos, segundos);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio07_V2() {
        double velocidade = 10.455;
        double horas = 1;
        double minutos = 5;
        double segundos = 0;

        double expected = 11.32;

        double result = BlocoI.exercicio07(velocidade,horas, minutos, segundos);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio07_V3() {
        double velocidade = 20;
        double horas = 4;
        double minutos = 2;
        double segundos = 10;

        double expected = 800;

        double result = BlocoI.exercicio07(velocidade,horas, minutos, segundos);
        assertNotEquals(expected, result, 0.01);
    }


    @Test
    public void exercicio08_V1() {
        double cabo1 = 1.4;
        double cabo2 = 2.3;
        int angulo = 30;

        double expected = 1.29;

        double result = BlocoI.exercicio08(cabo1, cabo2, angulo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio08_V2() {
        double cabo1 = 30;
        double cabo2 = 40;
        double angulo = 70;

        double expected = 40.98;

        double result = BlocoI.exercicio08(cabo1, cabo2, angulo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio08_V3() {
        double cabo1 = 0.7;
        double cabo2 = 8.63;
        double angulo = 14;

        double expected = 86;

        double result = BlocoI.exercicio08(cabo1, cabo2, angulo);

        assertNotEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio09_V1() {
        double a = 4;
        double b = 6;

        double expected = 20;

        double result = BlocoI.exercicio09(a, b);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio09_V2() {
        double a = 1;
        double b = 8;

        double expected = 18;

        double result = BlocoI.exercicio09(a, b);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio09_V3() {
        double a = 16;
        double b = 90;

        double expected = 1000;

        double result = BlocoI.exercicio09(a, b);

        assertNotEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio10_V1() {
        double cateto1 = 4;
        double cateto2 = 3;

        double expected = 5;

        double result = BlocoI.exercicio10(cateto1, cateto2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio10_V2() {
        double cateto1 = 9;
        double cateto2 = 4;

        double expected = 9.85;

        double result = BlocoI.exercicio10(cateto1, cateto2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio10_V3() {
        double cateto1 = 0.57;
        double cateto2 = 6;

        double expected = 20;

        double result = BlocoI.exercicio10(cateto1, cateto2);

        assertNotEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio11_FuncaoV1() {
        double x = 4;

        double expected = 5;

        double result = BlocoI.exercicio11_Funcao(x);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio11_FuncaoV2() {
        double x = 1;

        double expected = -1;

        double result = BlocoI.exercicio11_Funcao(x);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio11_FormulaResolventeV1() {
        double a = 1;
        double b = -3;
        double c = 1;

        double[] expected = {2.61, 0.38};

        double[] result = BlocoI.exercicio11_FormulaResolvente(a, b, c);

        assertArrayEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio11_FormulaResolventeV2() {
        double a = 2;
        double b = 4;
        double c = 2;

        double[] expected = {-1, -1};

        double[] result = BlocoI.exercicio11_FormulaResolvente(a, b, c);

        assertArrayEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio12_V1() {
        double celsius = 5;

        double expected = 41;

        double result = BlocoI.exercicio12(celsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio12_V2() {
        double celsius = 30;

        double expected = 86;

        double result = BlocoI.exercicio12(celsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio12_V3() {
        double celsius = 10;

        double expected = 60;

        double result = BlocoI.exercicio12(celsius);

        assertNotEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio13_V1() {
        double horas = 2;
        double minutos = 30;

        double expected = 150;

        double result = BlocoI.exercicio13(horas, minutos);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio13_V2() {
        double horas = 1;
        double minutos = 42;

        double expected = 102;

        double result = BlocoI.exercicio13(horas, minutos);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio13_V3() {
        double horas = 7;
        double minutos = 23;

        double expected = 5000;

        double result = BlocoI.exercicio13(horas, minutos);

        assertNotEquals(expected, result, 0.01);
    }
}