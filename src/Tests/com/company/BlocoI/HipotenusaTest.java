package com.company.BlocoI;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class HipotenusaTest {

    @Test
    public void testeParaCalcularHipotenusaSeCatetosTemValoresValidos() {
        double cateto1 = 4;
        double cateto2 = 3;
        Hipotenusa hipotenusa = new Hipotenusa(cateto1, cateto2);
        double expected = 5;
        double result = hipotenusa.calcularHipotenusa();
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeExcecaoSeCatetosTemValoresInvalidos() {
        double cateto1 = -4;
        double cateto2 = 3;
        assertThrows(IllegalArgumentException.class, () -> {
            new Hipotenusa(cateto1, cateto2);
        });
    }

}