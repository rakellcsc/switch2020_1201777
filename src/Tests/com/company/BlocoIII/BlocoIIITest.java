package com.company.BlocoIII;

import org.junit.Test;

import static org.junit.Assert.*;


public class BlocoIIITest {

    /*---------------Testes Exercicio 01---------------*/

    @Test
    public void testeParaCalcularFatorialDeUmNumero_V1() {
        int numero = 5;

        int expected = 120;

        int result = BlocoIII.fatorialNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularFatorialDeUmNumero_V2() {
        int numero = 10;

        int expected = 3628800;

        int result = BlocoIII.fatorialNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularFatorialDeUmNumero_V3() {
        int numero = -5;

        int expected = 0;

        int result = BlocoIII.fatorialNumero(numero);

        assertEquals(expected, result);
    }

    /*---------------Testes Exercicio 02---------------*/

    @Test
    public void testeParaCalcularPercentagemPositivasMediaNegativasAlunos_V1() {
        int numero = 6;
        double[] notas = {13, 15, 5, 7, 18, 6};
        double percentagemPositivas = 50;
        double mediaNotasNegativas = 6;

        String expected = "Percentagem de positivas: " + String.format("%.2f", percentagemPositivas) + ". Média de negativas: " + String.format("%.2f",mediaNotasNegativas);

        String result = BlocoIII.percentagemPositivasMediaNegativasAlunos(numero, notas);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularPercentagemPositivasMediaNegativasAlunos_V2() {
        int numero = 10;
        double[] notas = {-5, 15, 5, 7, 18, 6, 10, 20, 14, 3};

        String expected = "Dados inválidos.";

        String result = BlocoIII.percentagemPositivasMediaNegativasAlunos(numero, notas);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularPercentagemPositivasMediaNegativasAlunos_V3() {
        int numero = -10;
        double[] notas = {5, 15, 5, 7, 18, 6, 10, 20, 14, 3};

        String expected = "Introduza um número de alunos válido.";

        String result = BlocoIII.percentagemPositivasMediaNegativasAlunos(numero, notas);

        assertEquals(expected, result);
    }

    /*---------------Testes Exercicio 03---------------*/

    @Test
    public void testeParaCalcularPercentagemNumerosParesEMediaNumerosImparesNumaListaTerminadaComNumeroNegativo_V1() {
        double[] listaNumeros = {150, 32, 13, 56, 109, 15, 20, 302, -1};
        double percentagemPares = 62.5;
        double mediaImpares = 45.67;

        String expected = "A percentagem de números pares é " + String.format("%.2f", percentagemPares) + "% e a média dos números ímpares é " + String.format("%.2f", mediaImpares);

        String result = BlocoIII.percentagemParesMediaImparesNumaLista(listaNumeros);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularPercentagemNumerosParesEMediaNumerosImparesNumaListaTerminadaComNumeroNegativo_V2() {
        double[] listaNumeros = {150, 32, 13, 56, 109, -15, 20, 302};
        double percentagemPares = 60;
        double mediaImpares = 61;

        String expected = "A percentagem de números pares é " + String.format("%.2f", percentagemPares) + "% e a média dos números ímpares é " + String.format("%.2f", mediaImpares);

        String result = BlocoIII.percentagemParesMediaImparesNumaLista(listaNumeros);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularPercentagemNumerosParesEMediaNumerosImparesNumaListaTerminadaComNumeroNegativo_V3() {
        double[] listaNumeros = {1, 2, 3, 4};
        double percentagemPares = 50;
        double mediaImpares = 2;

        String expected = "A percentagem de números pares é " + String.format("%.2f", percentagemPares) + "% e a média dos números ímpares é " + String.format("%.2f", mediaImpares);

        String result = BlocoIII.percentagemParesMediaImparesNumaLista(listaNumeros);

        assertEquals(expected, result);
    }

    /*---------------Testes Exercicio 04---------------*/

    @Test
    public void testeParaCalcularMultiplosDeTresNumIntervalo_V1() {
        int minimoIntervalo = 100;
        int maximoIntervalo = 200;

        int expected = 33;

        int result = BlocoIII.multiplosDeTresNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMultiplosDeTresNumIntervalo_V2() {
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 3;

        int result = BlocoIII.multiplosDeTresNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMultiplosDeUmNumeroNumIntervalo_V1() {
        int nrADeterminarMultiplo = 4;
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 2;

        int result = BlocoIII.multiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMultiplosDeTresEDeCincoNumIntervalo_V1() {
        int minimoIntervalo = 50;
        int maximoIntervalo = 100;

        int expected = 25;

        int result = BlocoIII.multiplosDeTresEDeCincoNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMultiplosDeUmNumeroNumIntervalo_V2() {
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 5;

        int result = BlocoIII.multiplosDeTresEDeCincoNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMultiplosDeDoisNumerosNumIntervalo() {
        int numero1 = 2;
        int numero2 = 7;
        int minimoIntervalo = 20;
        int maximoIntervalo = 50;

        int expected = 19;

        int result = BlocoIII.multiplosDeDoisNumerosNumIntervalo(numero1, numero2, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularSomaDosMultiplosDeDoisNumerosNumIntervalo_V1() {
        int numero1 = 3;
        int numero2 = 5;
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 33;

        int result = BlocoIII.somaDosMultiplosDeDoisNumerosNumIntervalo(numero1, numero2, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularSomaDosMultiplosDeDoisNumerosNumIntervalo_V2() {
        int numero1 = 2;
        int numero2 = 7;
        int minimoIntervalo = 20;
        int maximoIntervalo = 50;

        int expected = 665;

        int result = BlocoIII.somaDosMultiplosDeDoisNumerosNumIntervalo(numero1, numero2, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    /*---------------Testes Exercicio 05---------------*/

    @Test
    public void testeParaSomarNumerosParesNumIntervalo_V1() {
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 30;

        int result = BlocoIII.somaNumerosParesNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarNumerosParesNumIntervalo_V2() {
        int minimoIntervalo = 100;
        int maximoIntervalo = 120;

        int expected = 1210;

        int result = BlocoIII.somaNumerosParesNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularQuantidadeNumerosParesNumIntervalo() {
        int minimoIntervalo = 100;
        int maximoIntervalo = 120;

        int expected = 11;

        int result = BlocoIII.quantidadeNumerosParesNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarNumerosImparesNumIntervalo() {
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 25;

        int result = BlocoIII.somaNumerosImparesNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularQuantidadeNumerosImparesNumIntervalo() {
        int minimoIntervalo = 1;
        int maximoIntervalo = 10;

        int expected = 5;

        int result = BlocoIII.quantidadeNumerosImparesNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularSomaMultiplosDeUmNumeroNumIntervalo_V1() {
        int nrADeterminarMultiplo = 4;
        int minimoIntervalo = 1;
        int maximoIntervalo = 16;

        int expected = 40;

        int result = BlocoIII.somaMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularSomaMultiplosDeUmNumeroNumIntervalo_V2() {
        int nrADeterminarMultiplo = 3;
        int minimoIntervalo = 10;
        int maximoIntervalo = 1;

        int expected = 18;

        int result = BlocoIII.somaMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularProdutoMultiplosDeUmNumeroNumIntervalo_V1() {
        int nrADeterminarMultiplo = 3;
        int minimoIntervalo = 10;
        int maximoIntervalo = 1;

        int expected = 162;

        int result = BlocoIII.produtoMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularProdutoMultiplosDeUmNumeroNumIntervalo_V2() {
        int nrADeterminarMultiplo = 5;
        int minimoIntervalo = 10;
        int maximoIntervalo = 20;

        int expected = 3000;

        int result = BlocoIII.produtoMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMediaMultiplosDeUmNumeroNumIntervalo_V1() {
        double nrADeterminarMultiplo = 5;
        double minimoIntervalo = 10;
        double maximoIntervalo = 20;

        double expected = 15;

        double result = BlocoIII.mediaMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaMultiplosDeUmNumeroNumIntervalo_V2() {
        double nrADeterminarMultiplo = 2;
        double minimoIntervalo = 95;
        double maximoIntervalo = 90;

        double expected = 92;

        double result = BlocoIII.mediaMultiplosDeUmNumeroNumIntervalo(nrADeterminarMultiplo, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaDosMultiplosDeDoisNumerosNumIntervalo_V1() {
        double numero1 = 2;
        double numero2 = 7;
        double minimoIntervalo = 1;
        double maximoIntervalo = 10;

        double expected = 6.17;

        double result = BlocoIII.mediaDosMultiplosDeDoisNumerosNumIntervalo(numero1, numero2, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaDosMultiplosDeDoisNumerosNumIntervalo_V2() {
        double numero1 = 3;
        double numero2 = 5;
        double minimoIntervalo = 15;
        double maximoIntervalo = 8;

        double expected = 11.5;

        double result = BlocoIII.mediaDosMultiplosDeDoisNumerosNumIntervalo(numero1, numero2, minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result, 0.01);
    }

    /*---------------Testes Exercicio 06---------------*/

    @Test
    public void testeParaContarOsAlgarismosDeUmNumeroLong_V1() {
        long numero = 582728280;

        int expected = 9;

        int result = BlocoIII.quantidadeAlgarismos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarOsAlgarismosDeUmNumeroLong_V2() {
        long numero = -1234498484;

        int expected = 10;

        int result = BlocoIII.quantidadeAlgarismos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarAlgarismosParDeUmNumero_v1() {
        long numero = 5045;

        int expected = 2;

        int result = BlocoIII.qtsAlgarismosParDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarAlgarismosParDeUmNumero_V2() {
        long numero = 2098764731;

        int expected = 5;

        int result = BlocoIII.qtsAlgarismosParDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarAlgarismosImparesDeUmNumero() {
        long numero = 209876473;

        int expected = 4;

        int result = BlocoIII.qtsAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosDeUmNumero_V1() {
        long numero = 1234567890;

        double expected = 45;

        double result = BlocoIII.somaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarAlgarismosDeUmNumero_V2() {
        long numero = 568920100;

        double expected = 31;

        double result = BlocoIII.somaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarAlgarismosParesDeUmNumero_V1() {
        long numero = 568920100;

        double expected = 16;

        double result = BlocoIII.somaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarAlgarismosParesDeUmNumero_V2() {
        long numero = -568920100;

        double expected = 16;

        double result = BlocoIII.somaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarAlgarismosImparesDeUmNumero_V1() {
        long numero = 568920100;

        double expected = 15;

        double result = BlocoIII.somaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarAlgarismosImparesDeUmNumero_V2() {
        long numero = -1234567892;

        double expected = 25;

        double result = BlocoIII.somaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaDeterminarComprimentoDeUmNumero_V1() {
        long numero = 34521346;

        int expected = 8;

        int result = BlocoIII.comprimentoDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarComprimentoDeUmNumero_V2() {
        long numero = -34521346;

        int expected = 8;

        int result = BlocoIII.comprimentoDeUmNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosdeUmNumero_V1() {
        long numero = 342516;

        double expected = 3.5;

        double result = BlocoIII.mediaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosdeUmNumero_V2() {
        long numero = -98637;

        double expected = 6.6;

        double result = BlocoIII.mediaAlgarismosDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosParesDeUmNumero_V1() {
        long numero = 98637;

        double expected = 7;

        double result = BlocoIII.mediaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosParesDeUmNumero_V2() {
        long numero = 46801;

        double expected = 4.5;

        double result = BlocoIII.mediaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosParesDeUmNumero_V3() {
        long numero = -4532122;

        double expected = 2.5;

        double result = BlocoIII.mediaAlgarismosParesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosImparesDeUmNumero_V1() {
        long numero = 365829109;

        double expected = 5.4;

        double result = BlocoIII.mediaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaAlgarismosImparesDeUmNumero_V2() {
        long numero = -365829109;

        double expected = 5.4;

        double result = BlocoIII.mediaAlgarismosImparesDeUmNumero(numero);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaInverterOrdemNumero_V1() {
        long numero = 34526754;

        int expected = 45762543;

        int result = BlocoIII.ordemInversaNumero(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaInverterOrdemNumero_V2() {
        long numero = -5647;

        int expected = -7465;

        int result = BlocoIII.ordemInversaNumero(numero);

        assertEquals(expected, result);
    }

    /*---------------Testes Exercicio 07---------------*/

    @Test
    public void testeParaVerificarSeNumeroCapicua_V1() {
        int numero = 34543;

        boolean expected = true;

        boolean result = BlocoIII.numeroCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarSeNumeroCapicua_V2() {
        int numero = 45280201;

        boolean expected = false;

        boolean result = BlocoIII.numeroCapicua(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaEncontrarPrimeiroCapicuaNumIntervalo_V1() {
        int minimoIntervalo = 100;
        int maximoIntervalo = 200;

        int expected = 101;

        int result = BlocoIII.primeiroCapicuaNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaEncontrarPrimeiroCapicuaNumIntervalo_V2() {
        int minimoIntervalo = 500;
        int maximoIntervalo = 1000;

        int expected = 505;

        int result = BlocoIII.primeiroCapicuaNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaEncontrarPrimeiroCapicuaNumIntervalo_V3() {
        int minimoIntervalo = 105;
        int maximoIntervalo = 110;

        int expected = 0;

        int result = BlocoIII.primeiroCapicuaNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaEncontrarMaiorCapicuaNumIntervalo() {
        int minimoIntervalo = 500;
        int maximoIntervalo = 1000;

        int expected = 989;

        int result = BlocoIII.maiorCapicuaNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeCapicuasNumIntervalo() {
        int minimoIntervalo = 100;
        int maximoIntervalo = 120;

        int expected = 2;

        int result = BlocoIII.qtsCapicuasNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNumeroAmstrong_V1() {
        int numero = 370;

        boolean expected = true;

        boolean result = BlocoIII.numeroAmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNumeroAmstrong_V2() {
        int numero = 140;

        boolean expected = false;

        boolean result = BlocoIII.numeroAmstrong(numero);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaEncontrarPrimeiroNrAmstrongNumIntervalo() {
        int minimoIntervalo = 350;
        int maximoIntervalo = 500;

        int expected = 370;

        int result = BlocoIII.primeiroNrAmstrongNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarAQuantidadeNumerosAmstrongNumIntervalo() {
        int minimoIntervalo = 360;
        int maximoIntervalo = 400;

        int expected = 2;

        int result = BlocoIII.qtsAmstrongNumIntervalo(minimoIntervalo, maximoIntervalo);

        assertEquals(expected, result);
    }

    /*--------------- Testes Exercicio 11---------------*/

    @Test
    public void testeManeirasPossiveisObterXParaValoresSuperioresADez() {
        int x = 15;
        String expected = "As combinaçoes possiveis sao: " + "5+10" + " " + "6+9" + " " + "7+8" + " ";
        String result = BlocoIII.maneirasPossiveisObterX(x);
        assertEquals(expected, result);
    }

    @Test
    public void testeManeirasPossiveisObterXParaValoresInferioresADez() {
        int x = 5;
        String expected = "As combinaçoes possiveis sao: " + "0+5" + " " + "1+4" + " " + "2+3" + " ";
        String result = BlocoIII.maneirasPossiveisObterX(x);
        assertEquals(expected, result);
    }

    @Test
    public void testeManeirasPossiveisObterXParaValoresInferioresAZero() {
        int x = -1;
        String expected = "Erro. Insira um numero inteiro entre 0 e 20";
        String result = BlocoIII.maneirasPossiveisObterX(x);
        assertEquals(expected, result);
    }

    @Test
    public void testeManeirasPossiveisObterXParaValoresSUperioresA20() {
        int x = 21;
        String expected = "Erro. Insira um numero inteiro entre 0 e 20";
        String result = BlocoIII.maneirasPossiveisObterX(x);
        assertEquals(expected, result);
    }

    /*--------------- Testes Exercicio 12---------------*/

    @Test
    public void teste_FormulaResolvente_EquacaoNaoSegundoGrau() {
        double a = 0.0, b = 1.0, c = 2.0;
        String expected = "A equação não é de segundo grau.";
        String result = BlocoIII.formulaResolvente(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    public void teste_FormulaResolvente_Equacao2RaizesImaginarias1() {
        double a = 1.0, b = 1.0, c = 2.0;
        String expected = "A equação tem duas raízes imaginárias.";
        String result = BlocoIII.formulaResolvente(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    public void teste_FormulaResolvente_Equacao2RaizesImaginarias2() {
        double a = 1.0, b = 2.0, c = 3.0;
        String expected = "A equação tem duas raízes imaginárias.";
        String result = BlocoIII.formulaResolvente(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    public void teste_FormulaResolvente_EquacaoRaizDupla1() {
        double a = 1.0, b = -6.0, c = 9.0;
        String expected = "A equação tem raíz dupla: 3.0";
        String result = BlocoIII.formulaResolvente(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    public void teste_FormulaResolvente_EquacaoRaizReal1() {
        double a = 1.0, b = -3.0, c = -10.0;
        String expected = "A equação tem duas raízes reais: 5.0 e -2.0";
        String result = BlocoIII.formulaResolvente(a, b, c);
        assertEquals(expected, result);
    }

    @Test
    public void teste_ArredondarNumeros_Arredondar2CasasDecimais() {
        double numero = 1.013;
        int casasDecimaisPretendidas = 2;
        double expected = 1.01;
        double result = BlocoIII.arredondarNumeros(numero, casasDecimaisPretendidas);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void teste_ArredondarNumeros_Arredondar1CasaDecimal() {
        double numero = 1.59;
        int casasDecimaisPretendidas = 1;
        double expected = 1.6;
        double result = BlocoIII.arredondarNumeros( numero, casasDecimaisPretendidas);
        assertEquals(expected, result, 0.01);
    }

    /*--------------- Testes Exercicio 13---------------*/

    @Test
    public void testeParaClassifcarAlimentosParaCodigoSuperiorA20() {
        int codigo = 21;
        String expected = "Código inválido";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoInferiorA0() {
        int codigo = -1;
        String expected = "Código inválido";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoIgualAUm() {
        int codigo = 1;
        String expected = "Classificação do produto: Alimento não perecível";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoEntreDoisEQuatro() {
        int codigo = 4;
        String expected = "Classificação do produto: Alimento perecível";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoEntreCincoESeis() {
        int codigo = 6;
        String expected = "Classificação do produto: Vestuário";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoIgualASete() {
        int codigo = 7;
        String expected = "Classificação do produto: Higiene pessoal";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaClassifcarAlimentosParaCodigoEntreOitoEQuinze() {
        int codigo = 10;
        String expected = "Classificação do produto: Limpeza e utensílios domésticos";
        String result = BlocoIII.verificaProduto(codigo);
        assertEquals(expected, result);
    }

    /*--------------- Testes Exercicio 14---------------*/

    @Test
    public void teste_Cambio300EurosParaDolares() {
        double quantia = 300;
        String moeda = "D";
        double cambio = 460.2;
        String expected = quantia + " euros " + " = " + cambio + " " + moeda;
        String result = BlocoIII.cambioMoeda(quantia, moeda);
        assertEquals(expected, result);
    }

    @Test
    public void teste_Cambio100EurosParaLibras() {
        double quantia = 100;
        String moeda = "L";
        double cambio = 77.4;
        String expected = quantia + " euros " + " = " + cambio + " " + moeda;
        String result = BlocoIII.cambioMoeda(quantia, moeda);
        assertEquals(expected, result);
    }

    @Test
    public void teste_Cambio10EurosParaIenes() {
        double quantia = 10;
        String moeda = "I";
        double cambio = 1614.8;
        String expected = quantia + " euros " + " = " + cambio + " " + moeda;
        String result = BlocoIII.cambioMoeda(quantia, moeda);
        assertEquals(expected, result);
    }

    @Test
    public void teste_Cambio150EurosParaCoroasSuecas() {
        double quantia = 150;
        String moeda = "C";
        double cambio = 1438.95;
        String expected = quantia + " euros " + " = " + cambio + " " + moeda;
        String result = BlocoIII.cambioMoeda(quantia, moeda);
        assertEquals(expected, result);
    }

    @Test
    public void teste_Cambio200EurosParaFrancosSuicos() {
        double quantia = 200;
        String moeda = "F";
        double cambio = 320.2;
        String expected = quantia + " euros " + " = " + cambio + " " + moeda;
        String result = BlocoIII.cambioMoeda(quantia, moeda);
        assertEquals(expected, result);
    }

    /*--------------- Testes Exercicio 15---------------*/

    @Test
    public void testeParaVerificarNotaQualitativaEntreZeroEQuatro() {
        int nota = 3;
        String expected = "Mau";
        String result = BlocoIII.verificaNotaQualitativa(nota);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNotaQualitativaEntreCincoENove() {
        int nota = 9;
        String expected = "Medíocre";
        String result = BlocoIII.verificaNotaQualitativa(nota);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNotaQualitativaEntreDezETreze() {
        int nota = 13;
        String expected = "Suficiente";
        String result = BlocoIII.verificaNotaQualitativa(nota);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNotaQualitativaEntreCatorzeEDezassete() {
        int nota = 17;
        String expected = "Bom";
        String result = BlocoIII.verificaNotaQualitativa(nota);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNotaQualitativaEntreDezoitoeEVinte() {
        int nota = 19;
        String expected = "Muito bom";
        String result = BlocoIII.verificaNotaQualitativa(nota);
        assertEquals(expected, result);
    }

    /*--------------- Testes Exercicio 16---------------*/

    @Test
    public void teste_CalcularSalarioLiquido_300Euros() {
        double salarioIliquido = 300;
        double expected = 270;
        double result = BlocoIII.calculoSalarioLiquido(salarioIliquido);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void teste_CalcularSalarioLiquido_600Euros() {
        double salarioIliquido = 600;
        double expected = 510;
        double result = BlocoIII.calculoSalarioLiquido(salarioIliquido);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void teste_CalcularSalarioLiquido_1100Euros() {
        double salarioIliquido = 1100;
        double expected = 880;
        double result = BlocoIII.calculoSalarioLiquido(salarioIliquido);
        assertEquals(expected, result, 0.01);
    }

    /*--------------- Testes Exercicio 17---------------*/

    @Test
    public void testeParaVerificarRacaCaoPequena() {
        double peso = 10;
        String expected = "Pequena";
        String result = BlocoIII.verificaRacaCanino(peso);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarRacaCaoMedia() {
        double peso = 25;
        String expected = "Media";
        String result = BlocoIII.verificaRacaCanino(peso);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarRacaCaoGrande() {
        double peso = 45;
        String expected = "Grande";
        String result = BlocoIII.verificaRacaCanino(peso);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarRacaCaoGigante() {
        double peso = 46;
        String expected = "Gigante";
        String result = BlocoIII.verificaRacaCanino(peso);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarQuantidadeDeCemGramasEPesoPequeno() {
        String raca = "Pequena";
        int pesoDadoRacaoCanino = 100;
        assertTrue(BlocoIII.verificaPesoRacaoAdequada(raca, pesoDadoRacaoCanino));
    }

    @Test
    public void testeParaVerificarQuantidadeDeDuzentasECinquentaGramasEPesoMedio() {
        String raca = "Media";
        int pesoDadoRacaoCanino = 250;
        assertTrue(BlocoIII.verificaPesoRacaoAdequada(raca, pesoDadoRacaoCanino));
    }

    @Test
    public void testeParaVerificarQuantidadeDeTrezentasGramasEPesoGrande() {
        String raca = "Grande";
        int pesoDadoRacaoCanino = 300;
        assertTrue(BlocoIII.verificaPesoRacaoAdequada(raca, pesoDadoRacaoCanino));
    }

    @Test
    public void testeParaVerificarQuantidadeDeQuinhentasGramasEPesoGigante() {
        String raca = "Gigante";
        int pesoDadoRacaoCanino = 500;
        assertTrue(BlocoIII.verificaPesoRacaoAdequada(raca, pesoDadoRacaoCanino));
    }

    @Test
    public void testeParaFalharQuantidadeEPeso() {
        String raca = "Gigante";
        int pesoDadoRacaoCanino = 100;
        assertFalse(BlocoIII.verificaPesoRacaoAdequada(raca, pesoDadoRacaoCanino));
    }

    /*--------------- Testes Exercicio 18---------------*/

    @Test
    public void teste_VerificarCC_True() {
        int numeroCC = 11111119; // a soma ponderada dos 8 algarismos dá 44, que é um multiplo de 11
        boolean result = BlocoIII.exercicio18VerificarCC(numeroCC);
        assertTrue(result);
    }

    @Test
    public void teste_VerificarCC_False() {
        int numeroCC = 11111111; // a soma ponderada dos 8 algarismos dá 36, que não é um multiplo de 11
        boolean result =  BlocoIII.exercicio18VerificarCC(numeroCC);
        assertFalse(result);
    }

    /*--------------- Testes Exercicio 19---------------*/

    @Test
    public void testeParaOrganizarNumerosImparesEsquerdaParesDireita() {
        int numero = 63271;
        int expected = 13726;
        int result = BlocoIII.organizaNumeros(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaOrganizarNumerosImparesEsquerdaParesDireitaComZero() {
        int numero = 603271;
        int expected = 137026;
        int result = BlocoIII.organizaNumeros(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaFalharOrganizarNumerosImparesEsquerdaParesDireita() {
        int numero = 18754;
        int expected = 48157;
        int result = BlocoIII.organizaNumeros(numero);
        assertNotEquals(expected, result);
    }

    /*--------------- Testes Exercicio 20---------------*/

    @Test
    public void teste_MainClassificarNumeros_10_Reduzido() {
        int numero = 10;
        String expected = "O número é reduzido.";
        String result = BlocoIII.classificarNumeros(numero);
        assertEquals(expected, result);
    }

    @Test
    public void teste_MainClassificarNumeros_0_Perfeito() {
        int numero = 0;
        String expected = "O número é perfeito.";
        String result = BlocoIII.classificarNumeros(numero);
        assertEquals(expected, result);
    }

    @Test
    public void teste_MainClassificarNumeros_20_Abundante() {
        int numero = 20;
        String expected = "O número é abundante.";
        String result = BlocoIII.classificarNumeros(numero);
        assertEquals(expected, result);
    }
}