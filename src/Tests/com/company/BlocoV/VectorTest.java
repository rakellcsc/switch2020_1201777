package com.company.BlocoV;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VectorTest {

    @Test
    public void testeParaAdicionarElementoAVetorVazio() {
        Vector vetor = new Vector();
        vetor.addElement(4);

        int[] expected = {4};

        assertArrayEquals(vetor.toArray(), expected);
    }

    @Test
    public void testeParaAdicionarElementoAVetorComElementos() {
        int[] vetor = {1, 2, 6};
        Vector vetorDado = new Vector(vetor);
        vetorDado.addElement(4);
        vetorDado.addElement(9);

        int[] expected = {1, 2, 6, 4, 9};

        assertArrayEquals(vetorDado.toArray(), expected);
    }

    @Test
    public void testeParaRemoverPrimeiroElementoDeUmArrayComElementos_V1() {
        int[] vetor = {2, 5, 6};
        Vector vetorDado = new Vector(vetor);

        vetorDado.removeElement(2);

        int[] expected = {5, 6};

        assertArrayEquals(vetorDado.toArray(), expected);
    }

    @Test
    public void testeParaRemoverPrimeiroElementoDeUmArrayComElementos_V2() {
        int[] vetor = {2, 5, 6};
        Vector vetorDado = new Vector(vetor);

        vetorDado.removeElement(5);

        int[] expected = {2, 6};

        assertArrayEquals(vetorDado.toArray(), expected);
    }

    @Test
    public void testeParaRemoverPrimeiroElementoDeUmArrayVazio() {
        Vector vetor = new Vector();

        assertThrows(IllegalArgumentException.class, () -> {
            vetor.removeElement(2);
        });
    }

    @Test
    public void testeParaRetornarElementoNumaPosicao() {
        int[] vetor = {1, 4, 7, 8};
        Vector vetorDado = new Vector(vetor);
        int posicao = 2;

        int expected = 7;

        int result = vetorDado.returnElementGivenPosition(posicao);

        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarIllegalArgumentQuandoPosicaoInvalida() {
        int[] vetor = {1, 4, 7, 8};
        Vector vetorDado = new Vector(vetor);
        int posicao = -2;

        assertThrows(IllegalArgumentException.class, () -> {
            vetorDado.returnElementGivenPosition(posicao);
        });
    }

    @Test
    public void testeParaRetornarNumeroElementosVetor_V1() {
        int[] vetor = {1, 5, 6, 8, 7};
        Vector vetorDado = new Vector(vetor);

        int expected = 5;

        int result = vetorDado.length();

        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNumeroElementosVetor_V2() {
        Vector vetor = new Vector();

        int expected = 0;

        int result = vetor.length();

        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarMaiorValorDoArray() {
        int[] vetor = {1, 4, 7, 10, 5};
        Vector vetorDado = new Vector(vetor);

        int expected = 10;

        int result = vetorDado.returnMaxValue();

        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarMenorValorDoArray() {
        int[] vetor = {1, 4, 7, 10, 5};
        Vector vetorDado = new Vector(vetor);

        int expected = 1;

        int result = vetorDado.returnMinValue();

        assertEquals(expected, result);
    }

    @Test
    public void mediaElementosVetor() {
        int[] vetor = {2, 5, 3, 6, 10};
        Vector vetorDado = new Vector(vetor);

        double expected = 5.2;

        double result = vetorDado.averageElementsOfArray();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfEvenNumbersInTheArray_V1() {
        int[] array = {1, 2, 5, 6, 10, 3, 8};
        Vector givenArray = new Vector(array);

        double expected = 6.5;

        double result = givenArray.averageEvenNumbers();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfEvenNumbersInTheArray_V2() {
        Vector givenArray = new Vector();
        givenArray.addElement(3);
        givenArray.addElement(-5);
        givenArray.addElement(2);
        givenArray.addElement(20);
        givenArray.addElement(4);

        double expected = 8.67;

        double result = givenArray.averageEvenNumbers();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfOddNumbersInTheArray_V1() {
        int[] array = {1, 2, 5, 6, 10, 3, 8};
        Vector givenArray = new Vector(array);

        double expected = 3;

        double result = givenArray.averageOddNumbers();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfOddNumbersInTheArray_V2() {
        Vector givenArray = new Vector();
        givenArray.addElement(3);
        givenArray.addElement(-5);
        givenArray.addElement(2);
        givenArray.addElement(20);
        givenArray.addElement(4);

        double expected = -1;

        double result = givenArray.averageOddNumbers();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfMultiplesOfANumber_V1() {
        int[] array = {1, 2, 5, 6, 10, 3, 30};
        Vector givenArray = new Vector(array);

        double expected = 15;

        double result = givenArray.averageOfMultiplesOfANumber(5);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAverageOfMultiplesOfANumber_V2() {
        Vector givenArray = new Vector();
        givenArray.addElement(6);
        givenArray.addElement(12);
        givenArray.addElement(0);
        givenArray.addElement(5);
        givenArray.addElement(8);

        double expected = 6;

        double result = givenArray.averageOfMultiplesOfANumber(3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToSortTheArrayAscending() {
        int[] array = {1, 2, 5, 6, 10, 3, 30};
        Vector givenArray = new Vector(array);
        givenArray.sortAscending();

        int[] expected = {1, 2, 3, 5, 6, 10, 30};

        assertArrayEquals(expected, givenArray.toArray());
    }

    @Test
    public void testSortDescending() {
        Vector givenArray = new Vector();
        givenArray.addElement(1);
        givenArray.addElement(2);
        givenArray.addElement(5);
        givenArray.addElement(6);
        givenArray.addElement(10);
        givenArray.addElement(3);
        givenArray.addElement(30);
        givenArray.sortDescending();

        int[] expected = {30, 10, 6, 5, 3, 2, 1};

        assertArrayEquals(expected, givenArray.toArray());
    }

    @Test
    public void testToReturnTrueIfArrayIsEmpty() {
        Vector givenArray = new Vector();

        assertTrue(givenArray.checkIfArrayIsEmpty());
    }

    @Test
    public void testToReturnFalseIfArrayIsNotEmpty() {
        int[] array = {1, 2, 5, 6, 10, 3, 30};
        Vector givenArray = new Vector(array);

        assertFalse(givenArray.checkIfArrayIsEmpty());
    }

    @Test
    public void testToReturnTrueIfArrayHasOneElement() {
        int[] array = {30};
        Vector givenArray = new Vector(array);

        assertTrue(givenArray.checkIfArrayHasOneElement());
    }

    @Test
    public void testToReturnFalseIfArrayHasLessThanOneElement() {
        Vector givenArray = new Vector();

        assertFalse(givenArray.checkIfArrayHasOneElement());
    }


    @Test
    public void testToReturnTrueIfArrayHasOnlyEvenNumbers() {
        int[] array = {2, 12, 6, 10, 20, 30};
        Vector givenArray = new Vector(array);

        assertTrue(givenArray.checkIfArrayHasOnlyEvenNumbers());
    }

    @Test
    public void testToReturnFalseIfArrayHasOddAndEvenNumbers() {
        int[] array = {1, 2, 12, 6, 10, 20, 30};
        Vector givenArray = new Vector(array);

        assertFalse(givenArray.checkIfArrayHasOnlyEvenNumbers());
    }

    @Test
    public void testToReturnTrueIfArrayHasOnlyOddNumbers() {
        int[] array = {1, 3, 15, 13, 45};
        Vector givenArray = new Vector(array);

        assertTrue(givenArray.checkIfArrayHasOnlyOddNumbers());
    }

    @Test
    public void testToReturnFalseIfArrayHasEvenAndOddNumbers() {
        int[] array = {1, 3, 15, 20, 13, 45};
        Vector givenArray = new Vector(array);

        assertFalse(givenArray.checkIfArrayHasOnlyOddNumbers());
    }

    @Test
    public void testToReturnTrueIfArrayHasDuplicates_V1() {
        int[] array = {1, 3, 1, 20, 13, 45};
        Vector givenArray = new Vector(array);

        assertTrue(givenArray.checkIfArrayHasDuplicates());
    }

    @Test
    public void testToReturnTrueIfArrayHasDuplicates_V2() {
        int[] array = {1, 3, 4, 20, 45, 45};
        Vector givenArray = new Vector(array);

        assertTrue(givenArray.checkIfArrayHasDuplicates());
    }

    @Test
    public void testToReturnFalseIfArrayDoesNotHaveDuplicates() {
        int[] array = {1, 3, 4, 20, 45, 100};
        Vector givenArray = new Vector(array);

        assertFalse(givenArray.checkIfArrayHasDuplicates());
    }

    @Test
    public void testToReturnElementsWithMostDigitsThanAverageDigits_V1() {
        int[] array = {1, 13, 15, 125, 45};
        Vector givenArray = new Vector(array);

        int[] expected = {125};

        Vector result = givenArray.returnElementsWithMostDigitsThanAverageDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnElementsWithMostDigitsThanAverageDigits_V2() {
        int[] array = {1, 13, 15, 125, 45, 6, 3, 89, 12345};
        Vector givenArray = new Vector(array);

        int[] expected = {125, 12345};

        Vector result = givenArray.returnElementsWithMostDigitsThanAverageDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnElementsWithPercentageOfEvenNumbersBiggerThanAverage() {
        int[] array = {1, 13, 15, 125, 45, 6, 3, 89, 12345};
        Vector givenArray = new Vector(array);

        int[] expected = {125, 45, 6, 89, 12345};

        Vector result = givenArray.returnElementsWithPercentageOfEvenNumbersBiggerThanAverage();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnEmptyArray() {
        int[] array = {1, 13, 15, 35, 75, 9};
        Vector givenArray = new Vector(array);

        int[] expected = {};

        Vector result = givenArray.returnElementsWithPercentageOfEvenNumbersBiggerThanAverage();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnElementsWithOnlyEvenDigits() {
        int[] array = {1, 12, 24, 35, 86, 9};
        Vector givenArray = new Vector(array);

        int[] expected = {24, 86};

        Vector result = givenArray.returnElementsWithOnlyEvenDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnElementsWithSortDigits_V1() {
        int[] array = {123, 42, 35, 86};
        Vector givenArray = new Vector(array);

        int[] expected = {123, 35};

        Vector result = givenArray.returnElementsWithSortDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnElementsWithSortDigits_V2() {
        int[] array = {219, 147, 935, 589, 36789, 153};
        Vector givenArray = new Vector(array);

        int[] expected = {147, 589, 36789};

        Vector result = givenArray.returnElementsWithSortDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnPalindromeElementsOfTheArray_V1() {
        int[] array = {219, 747, 935, 456654};
        Vector givenArray = new Vector(array);

        int[] expected = {747, 456654};

        Vector result = givenArray.returnPalindromeElements();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnEmptyArrayIfThereAreNotPalindromeNumbersIntheArray() {
        int[] array = {219, 740, 935, 4574};
        Vector givenArray = new Vector(array);

        int[] expected = {};

        Vector result = givenArray.returnPalindromeElements();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnArrayOfElementsWithEqualDigits_1() {
        int[] array = {222, 45, 78, 66};
        Vector givenArray= new Vector(array);

        int[] expected = {222, 66};

        Vector result = givenArray.returnElementsWithEqualDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnArrayOfElementsWithEqualDigits_V2() {
        int[] array = {222, 45, 78, 66, 313};
        Vector givenArray= new Vector(array);

        int[] expected = {222, 66};

        Vector result = givenArray.returnElementsWithEqualDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnAnEmptyArray() {
        int[] array = {221, 45, 78, 96, 313};
        Vector givenArray= new Vector(array);

        int[] expected = {};

        Vector result = givenArray.returnElementsWithEqualDigits();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToGetAmstrongNumberFromArray_V1() {
        int[] array = {222, 45, 78, 370, 313};
        Vector givenArray= new Vector(array);

        int[] expected = {370};

        Vector result = givenArray.getAmstrongNumberFromArray();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToGetAmstrongNumberFromArray_V2() {
        int[] array = {222, 45, 78, 370, 313, 153};
        Vector givenArray= new Vector(array);

        int[] expected = {370, 153};

        Vector result = givenArray.getAmstrongNumberFromArray();

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToGetAscendingElementsWithMoreThanThreeDigits() {
        int[] array = {236, 45, 78, 70, 3136, 513, 153, 5689};
        Vector givenArray= new Vector(array);

        int[] expected = {236, 5689};

        Vector result = givenArray.getAscendingElementsWithMoreThanNDigits(3);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToGetAscendingElementsWithMoreThanTwoDigits() {
        int[] array = {236, 45, 78, 70, 3136};
        Vector givenArray= new Vector(array);

        int[] expected = {236, 45, 78};

        Vector result = givenArray.getAscendingElementsWithMoreThanNDigits(2);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnAnEmptyArrayIfThereAreNotAscendingElementsWithMoreThanThreeDigits() {
        int[] array = {36, 45, 78, 70, 1};
        Vector givenArray= new Vector(array);

        int[] expected = {};

        Vector result = givenArray.getAscendingElementsWithMoreThanNDigits(3);

        assertArrayEquals(expected, result.toArray());
    }

    @Test
    public void testToReturnTrueIfVectorsAreEqual() {
        int[] array = {36, 45, 78, 70, 1};
        Vector givenArray= new Vector(array);

        Vector newArray = new Vector();
        newArray.addElement(36);
        newArray.addElement(45);
        newArray.addElement(78);
        newArray.addElement(70);
        newArray.addElement(1);

        assertTrue(givenArray.verifyIfEqualVector(newArray));
    }

    @Test
    public void testToReturnFalseIfVectorsAreNotEqual() {
        int[] array = {36, 45, 78, 70, 1};
        Vector givenArray= new Vector(array);

        Vector newArray = new Vector();
        newArray.addElement(36);
        newArray.addElement(45);
        newArray.addElement(78);
        newArray.addElement(70);
        newArray.addElement(80);

        assertFalse(givenArray.verifyIfEqualVector(newArray));
    }

    @Test
    public void testToReturnFalseIfVectorsHaveDifferentLength() {
        int[] array = {36, 45, 78, 70, 1};
        Vector givenArray= new Vector(array);

        Vector newArray = new Vector();
        newArray.addElement(36);
        newArray.addElement(45);
        newArray.addElement(78);
        newArray.addElement(70);

        assertFalse(givenArray.verifyIfEqualVector(newArray));
    }
}