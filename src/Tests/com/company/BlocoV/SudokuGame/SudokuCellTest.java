package com.company.BlocoV.SudokuGame;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SudokuCellTest {

    @Test
    public void testToThrowIllegalArgumentIfNumberIsInvalidInTheConstructor() {

        assertThrows(IllegalArgumentException.class, () -> {
            SudokuCell cell = new SudokuCell(10, false);
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testToThrowIllegalArgumentIfNumberIsInvalidInTheConstructor2() {
        SudokuCell cell = new SudokuCell(10, false);
    }

    @Test
    public void testToVerifyTheNumberIsFixed() {
        SudokuCell cell = new SudokuCell(2, true);

        assertTrue(cell.checkIfIsFixedNumber());
    }

    @Test
    public void testToVerifyTheNumberIsNotFixed() {
        SudokuCell cell = new SudokuCell(2, false);

        assertFalse(cell.checkIfIsFixedNumber());
    }

    @Test
    public void testToGetNumberIfNonFixedNumber() {
        SudokuCell cell = new SudokuCell(2, false);

        int expected = 2;

        assertEquals(expected, cell.getOtherNumber());
    }

    @Test
    public void testToGetNumberIfFixedNumber() {
        SudokuCell cell = new SudokuCell(2, true);

        int expected = 2;

        assertEquals(expected, cell.getOtherNumber());
    }

    @Test
    public void testToCheckEqualsObjects() {
        SudokuCell cell1 = new SudokuCell(2, true);
        SudokuCell cell2 = new SudokuCell(2, true);

        assertEquals(cell1, cell2);
    }

    @Test
    public void testToCheckNotEqualsObjects() {
        SudokuCell cell1 = new SudokuCell(2, true);
        SudokuCell cell2 = new SudokuCell(2, false);

        assertNotEquals(cell1, cell2);
    }

    @Test
    public void testToChangeNonFixedNumber() {
        SudokuCell cell = new SudokuCell(2, false);

        cell.changeNumber(4);

        SudokuCell expected = new SudokuCell(4, false);

        assertEquals(expected, cell);
    }

    @Test
    public void testToThrowIllegalArgumentIfTryingToChangeFixedNumber() {
        SudokuCell cell = new SudokuCell(2, true);

        assertThrows(IllegalArgumentException.class, () -> {
            cell.changeNumber(4);
        });
    }

    @Test
    public void testToThrowIllegalArgumentIfTryingToAddAnInvalidNumber() {
        SudokuCell cell = new SudokuCell(4, false);

        assertThrows(IllegalArgumentException.class, () -> {
            cell.changeNumber(10);
        });
    }

    @Test
    public void testToRemoveNonFixedValue() {
        SudokuCell cell = new SudokuCell(4, false);

        cell.removeNonFixedValue();

        SudokuCell expected = new SudokuCell(0, false);

        assertEquals(cell, expected);
    }

    @Test
    public void testToThrowIllegalArgumentIfTryingToRemoveFixedValue() {
        SudokuCell cell = new SudokuCell(4, true);

        assertThrows(IllegalArgumentException.class, () -> {
            cell.removeNonFixedValue();
        });
    }
}