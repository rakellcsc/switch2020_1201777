package com.company.BlocoV.SudokuGame;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SudokuGameGridTest {

    @Test
    public void testToValidateInitialMatrix() {
        int[][] gameMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        SudokuGameGrid gameGrid = new SudokuGameGrid(gameMatrix);

        gameGrid.validateInitialMatrix(gameMatrix);

        SudokuGameGrid expected = new SudokuGameGrid(new int[][]{
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        });

        assertEquals(gameGrid, expected);
    }

    @Test
    public void testToThrowIllegalArgumentIfNotValidInitialMatrix() {
        int[][] gameMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 10, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        assertThrows(IllegalArgumentException.class, () -> {
            new SudokuGameGrid(gameMatrix);
        });
    }

    @Test
    public void testToAddNumberToTheGrid() {
        int[][] gameMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        SudokuGameGrid gameGrid = new SudokuGameGrid(gameMatrix);

        gameGrid.addCell(3, 4, 6);

        int[][] gameTransformMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 3, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        SudokuGameGrid gameGridValueAdd = new SudokuGameGrid(gameTransformMatrix);

        assertEquals(gameGrid, gameGridValueAdd);
    }

    @Test
    public void testToThrowIllegalArgumentIfTryingToAddInvalidNumberToTheGrid() {
        int[][] gameMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        SudokuGameGrid gameGrid = new SudokuGameGrid(gameMatrix);

        assertThrows(IllegalArgumentException.class, () -> {
            gameGrid.addCell(11, 4, 6);
        });
    }

    @Test
    public void testToThrowIllegalArgumentIfTryingToAddCellToFixedNumber() {
        int[][] gameMatrix = {
                {1, 0, 5, 0, 0, 0, 6, 9, 0},
                {0, 2, 0, 0, 1, 0, 7, 0, 0},
                {0, 0, 0, 3, 0, 0, 0, 0, 1},
                {0, 1, 4, 0, 0, 0, 8, 0, 0},
                {0, 3, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 6, 0, 9, 0, 4, 1, 0},
                {3, 0, 0, 2, 8, 5, 0, 0, 0},
                {0, 5, 0, 0, 0, 6, 0, 3, 8},
                {9, 0, 7, 0, 5, 0, 0, 0, 4}
        };

        SudokuGameGrid gameGrid = new SudokuGameGrid(gameMatrix);

        assertThrows(IllegalArgumentException.class, () -> {
            gameGrid.addCell(6, 0, 0);
        });
    }
}