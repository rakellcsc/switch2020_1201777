package com.company.BlocoV;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MatrixTest {

    @Test
    public void testToAddElementToMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        givenMatrix.addElementToMatrix(5, 2);

        int[][] expected = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3, 5}};

        assertArrayEquals(givenMatrix.toMatrix(), expected);
    }

    @Test
    public void testToAddElementToVectorInTheMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        givenMatrix.addElementToVectorInMatrix(5, 2);

        int[][] expected = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3, 5}};

        assertArrayEquals(givenMatrix.toMatrix(), expected);
    }

    @Test
    public void testToThrowIllegalArgumentIfLineIsInvalid() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        assertThrows(IllegalArgumentException.class, () -> {
            givenMatrix.addElementToMatrix(5, 3);
        });
    }

    @Test
    public void testToRemoveFirstNumberTwoFromMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        givenMatrix.removeFirstElementFromMatrix(2);

        int[][] expected = {{1, 4}, {5, 2, 4}, {1, 8, 3}};

        assertArrayEquals(givenMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRemoveFirstNumberThreeFromMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        givenMatrix.removeFirstElementFromMatrix(3);

        int[][] expected = {{1, 2, 4}, {5, 2, 4}, {1, 8}};

        assertArrayEquals(givenMatrix.toMatrix(), expected);
    }

    @Test
    public void testToReturnInitialMatrixIfElementToRemoveIsNotInTheMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        givenMatrix.removeFirstElementFromMatrix(7);

        int[][] expected = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};

        assertArrayEquals(givenMatrix.toMatrix(), expected);
    }

    @Test
    public void testToThrowIllegalArgumentIfMatrixIsEmpty() {
        int[][] matrix = {};
        Matrix givenMatrix = new Matrix(matrix);

        assertThrows(IllegalArgumentException.class, () -> {
            givenMatrix.removeFirstElementFromMatrix(5);
        });
    }

    @Test
    public void testToReturnTrueIfEmptyMatrix() {
        int[][] matrix = {};
        Matrix givenMatrix = new Matrix(matrix);

        assertTrue(givenMatrix.checkMatrixEmpty());
    }

    @Test
    public void testToReturnFalseIfNotEmptyMatrix() {
        int[][] matrix = {{1, 2, 3}, {6, 7}};
        Matrix givenMatrix = new Matrix(matrix);

        assertFalse(givenMatrix.checkMatrixEmpty());
    }

    @Test
    public void testToReturnMaxValueOfTheMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        int expected = 8;

        int result = givenMatrix.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    public void testToThrowIllegalArgumentIfEmptyMatrix() {
        int[][] matrix = {};
        Matrix givenMatrix = new Matrix(matrix);

        assertThrows(IllegalArgumentException.class, givenMatrix::getMaxValue);
    }

    @Test
    public void testToReturnMinValueOfTheMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        int expected = -2;

        int result = givenMatrix.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnMeanOfNumbersInTheMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, 2, 4}};
        Matrix givenMatrix = new Matrix(matrix);

        double expected = 3;

        double result = givenMatrix.getMean();

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testToReturnAVectorWithTheSumOfEachRowOfTheMatrix_V1() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{7, 7, 12});

        Vector result = givenMatrix.getSumOfEachRow();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnAVectorWithTheSumOfEachRowOfTheMatrix_V2() {
        int[][] matrix = {{1, 2, 4}, {5, 5, 4}};
        Matrix givenMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{7, 14});

        Vector result = givenMatrix.getSumOfEachRow();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnAVectorWithTheSumOfEachColumnOfTheMatrix_V1() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{7, 8, 11});

        Vector result = givenMatrix.getSumOfEachColumn();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnAVectorWithTheSumOfEachColumnOfTheMatrix_V2() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}};
        Matrix givenMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{6, 0, 8});

        Vector result = givenMatrix.getSumOfEachColumn();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnIndexOfTheRowWithBiggestSum_V1() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        int expected = 2;

        int result = givenMatrix.indexBiggestSumRow();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnIndexOfTheRowWithBiggestSum_V2() {
        int[][] matrix = {{1, 2, 4}, {9, 4}, {5}};
        Matrix givenMatrix = new Matrix(matrix);

        int expected = 1;

        int result = givenMatrix.indexBiggestSumRow();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnIndexOfTheRowWithBiggestSum_V3() {
        int[][] matrix = {{1, 2, 4}, {5, 2}, {1, 5}};
        Matrix givenMatrix = new Matrix(matrix);

        int expected = 0;

        int result = givenMatrix.indexBiggestSumRow();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnTrueIfSquareMatrix() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {1, 8, 3}};
        Matrix givenMatrix = new Matrix(matrix);

        boolean result = givenMatrix.checkIfSquareMatrix();

        assertTrue(result);
    }

    @Test
    public void testToReturnFalseIfNotSquareMatrix_V1() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}};
        Matrix givenMatrix = new Matrix(matrix);

        boolean result = givenMatrix.checkIfSquareMatrix();

        assertFalse(result);
    }

    @Test
    public void testToReturnFalseIfNotSquareMatrix_V2() {
        int[][] matrix = {{1, 2, 4}, {5, -2, 4}, {}};
        Matrix givenMatrix = new Matrix(matrix);

        boolean result = givenMatrix.checkIfSquareMatrix();

        assertFalse(result);
    }


    @Test
    public void testToReturnFalseIfNotASymmetricMatrix() {
        int[][] matrix = {{1, 2},
                {5, -2}};
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfSymmetricMatrix();

        assertFalse(result);
    }

    @Test
    public void testToReturnTrueIfNotASymmetricMatrix_V1() {
        int[][] matrix = {{2, 2}, {2, 2}};
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfSymmetricMatrix();

        assertTrue(result);
    }

    @Test
    public void testToReturnTrueIfNotASymmetricMatrix_V2() {
        int[][] matrix = {{3, -2, 4},
                {-2, 6, 2},
                {4, 2, 3}};
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfSymmetricMatrix();

        assertTrue(result);
    }

    @Test
    public void testToReturnQtsOfElementsDiferentThanZeroInDiagonal_V1() {
        int[][] matrix = {{3, -2, 4},
                          {-2, 6, 2},
                          {4, 2, 3}};
        Matrix originalMatrix = new Matrix(matrix);

        int expected = 3;

        int result = originalMatrix.qtsElementsDiferentThanZeroInDiagonal();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnQtsOfElementsDiferentThanZeroInDiagonal_V2() {
        int[][] matrix = {{3, -2, 4},
                          {-2, 0, 2},
                          {4, 2, 3}};
        Matrix originalMatrix = new Matrix(matrix);

        int expected = 2;

        int result = originalMatrix.qtsElementsDiferentThanZeroInDiagonal();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnQtsOfElementsDiferentThanZeroInDiagonal_V3() {
        int[][] matrix = {{0, -2, 4},
                          {-2, 0, 2},
                          {4, 2, 0}};
        Matrix originalMatrix = new Matrix(matrix);

        int expected = 0;

        int result = originalMatrix.qtsElementsDiferentThanZeroInDiagonal();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnMinusOneIfNotSquareMatrix() {
        int[][] matrix = {{0, -2, 4},
                          {-2, 0, 2}};
        Matrix originalMatrix = new Matrix(matrix);

        int expected = -1;

        int result = originalMatrix.qtsElementsDiferentThanZeroInDiagonal();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnTrueIfMainDiagonalIsEqualToSecondaryDiagonal_V1() {
        int[][] matrix = {
                {1, -2, 1},
                {-2, 3, 2},
                {5, 2, 5}
        };
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfEqualMainAndSecondaryDiagonal();

        assertTrue(result);
    }

    @Test
    public void testToReturnTrueIfMainDiagonalIsEqualToSecondaryDiagonal_V2() {
        int[][] matrix = {
                {1, -2, 1},
                {-2, 3, 2},
        };
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfEqualMainAndSecondaryDiagonal();

        assertTrue(result);
    }

    @Test
    public void testToReturnFalseIfMainDiagonalIsNotEqualToSecondaryDiagonal() {
        int[][] matrix = {
                {1, -2, 2},
                {-2, 3, 2},
                {5, 2, 5}
        };
        Matrix originalMatrix = new Matrix(matrix);

        boolean result = originalMatrix.checkIfEqualMainAndSecondaryDiagonal();

        assertFalse(result);
    }

    @Test
    public void testToThrowAnIllegalArgumentIfMatrixIsNotSquareOrRectangular() {
        int[][] matrix = {
                {1, -2, 2},
                {-2, 3, 2},
                {5, 2}
        };
        Matrix originalMatrix = new Matrix(matrix);

        assertThrows(IllegalArgumentException.class, originalMatrix::checkIfEqualMainAndSecondaryDiagonal);
    }

    @Test
    public void testToReturnElementsWithHigherNumberOfDigitsThanAverage() {
        int[][] matrix = {
                {22, 453, 1},
                {14, 48, 1350},
                {9, 1, 3}
        };
        Matrix originalMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{22, 453, 14, 48, 1350});

        Vector result = originalMatrix.getBiggestThanAverage();

        assertEquals(expected, result);
    }

    @Test
    public void returnElementsWithPercentageOfEvenNumbersBiggerThanAverage() {
        int[][] matrix = {
                {22, 453, 1},
                {14, 48, 1350},
                {8, 51, 30}
        };
        Matrix originalMatrix = new Matrix(matrix);

        Vector expected = new Vector(new int[]{22, 48, 8});

        Vector result = originalMatrix.getElementsWithPercentageOfEvenNumbersBiggerThanAverage();

        assertEquals(expected, result);
    }

    @Test
    public void testToReturnInvertedElementsRows() {
        int[][] matrix = {
                {22, 453, 1},
                {14, 48, 1350},
                {8, 51, 30}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.getInvertedElementsRows();

        int[][] expected = {{1, 453, 22},
                            {1350, 48, 14},
                            {30, 51, 8}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToInvertColumns() {
        int[][] matrix = {
                {22, 453, 1},
                {14, 48, 1350},
                {8, 51, 30}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.getInvertedElementsColumns();

        int[][] expected = {{8, 51, 30},
                            {14, 48, 1350},
                            {22, 453, 1}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixNinetyDegreesPositive_V1() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixNinetyDegreesPositive();

        int[][] expected = {{7, 4, 1},
                            {8, 5, 2},
                            {9, 6, 3}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixNinetyDegreesPositive_V2() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixNinetyDegreesPositive();

        int[][] expected = {{4, 1},
                            {5, 2},
                            {6, 3}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixNinetyDegreesPositive_V3() {
        int[][] matrix = {};
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixNinetyDegreesPositive();

        int[][] expected = {};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixNinetyDegreesNegative_V1() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixNinetyDegreesNegative();

        int[][] expected = {{3, 6, 9},
                            {2, 5, 8},
                            {1, 4, 7}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixNinetyDegreesNegative_V2() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixNinetyDegreesNegative();

        int[][] expected = {{3, 6},
                            {2, 5},
                            {1, 4}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixAHundredAndEightyDegrees_V1() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixHundredEighty();

        int[][] expected = {{9, 8, 7},
                            {6, 5, 4},
                            {3, 2, 1}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }

    @Test
    public void testToRotateMatrixAHundredAndEightyDegrees_V2() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
        };
        Matrix originalMatrix = new Matrix(matrix);

        originalMatrix.rotateMatrixHundredEighty();

        int[][] expected = {{6, 5, 4},
                            {3, 2, 1}};

        assertArrayEquals(originalMatrix.toMatrix(), expected);
    }
}

