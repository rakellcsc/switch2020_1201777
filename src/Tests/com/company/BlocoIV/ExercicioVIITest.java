package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioVIITest {

    @Test
    public void testeParaContarOsMultiplosDeTresNoIntervaloDeDezAVinte() {
        int minimo = 10;
        int maximo = 20;
        int expected = 3;
        int result = ExercicioVII.contarMultiplosDeTresNumIntervalo(minimo, maximo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarOsMultiplosDeTresNoIntervaloDeDezAVinteInvertendoOsExtremos() {
        int minimo = 20;
        int maximo = 10;
        int expected = 3;
        int result = ExercicioVII.contarMultiplosDeTresNumIntervalo(minimo, maximo);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornaArrayComMultiplosDeTresNoIntervaloEntreDezEVinte() {
        int minimo = 10;
        int maximo = 20;
        int[] expected = {12, 15, 18};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeTres(minimo, maximo);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornaArrayComMultiplosDeTresIntervaloInvertido() {
        int minimo = 20;
        int maximo = 10;
        int[] expected = {12, 15, 18};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeTres(minimo, maximo);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornaArrayComMultiplosDeTresNumIntervaloComNumerosNegativos() {
        int minimo = -10;
        int maximo = 10;
        int[] expected = {0, 3, 6, 9};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeTres(minimo, maximo);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaContarMultiplosDeUmNumeroNumIntervaloDeNumerosPositivos() {
        int minimo = 20;
        int maximo = 10;
        int numero = 2;
        int expected = 6;
        int result = ExercicioVII.contarMultiplosDeUmNumeroNumIntervalo(minimo, maximo, numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaContarMultiplosDeUmNumeroNumIntervaloDeNumerosNegativosEPositivos() {
        int minimo = -10;
        int maximo = 10;
        int numero = 4;
        int expected = 3;
        int result = ExercicioVII.contarMultiplosDeUmNumeroNumIntervalo(minimo, maximo, numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComMultiplosDeUmNumeroPositivoNumIntervaloDeNumerosPositivos() {
        int minimo = 5;
        int maximo = 30;
        int numero = 5;
        int[] expected = {5, 10, 15, 20, 25, 30};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeUmNumero(minimo, maximo, numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComMultiplosDeUmNumeroNegativoNumIntervalo() {
        int minimo = 5;
        int maximo = 30;
        int numero = -5;
        int[] expected = {};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeUmNumero(minimo, maximo, numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComMultiplosDeUmNumeroPositivoNumIntervaloDeNumerosNegativosEPositivos() {
        int minimo = -30;
        int maximo = 10;
        int numero = 6;
        int[] expected = {0, 6};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeUmNumero(minimo, maximo, numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComMultiplosDeZero() {
        int minimo = -30;
        int maximo = 10;
        int numero = 0;
        int[] expected = {};
        int[] result = ExercicioVII.retornaArrayComMultiplosDeUmNumero(minimo, maximo, numero);
        assertArrayEquals(expected, result);
    }
}