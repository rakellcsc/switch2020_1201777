package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXVTest {

    @Test
    public void testeParaDescobrirMenorValorMatrizSemArraysVazios() {
        int[][] matriz = {{1, 2, 3}, {2, 6, 7}, {10, 15}};
        int expected = 1;
        int result = ExercicioXV.descobreMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMenorValorMatrizComArraysVazios_v1() {
        int[][] matriz = {{6, 6, 2}, {10, 15}, {}};
        int expected = 2;
        int result = ExercicioXV.descobreMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMenorValorMatrizComArraysVazios_V2() {
        int[][] matriz = {{}, {-7, 3}};
        int expected = -7;
        int result = ExercicioXV.descobreMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMenorValorMatrizComArraysVazios_V3() {
        int[][] matriz = {{-7, 3}, {}, {1, -100, 250}};
        int expected = -100;
        int result = ExercicioXV.descobreMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMenorValorMatrizComValoresNegativos() {
        int[][] matriz = {{2, 6, 7}, {10, -15}};
        int expected = -15;
        int result = ExercicioXV.descobreMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMaiorValorMatriz() {
        int[][] matriz = {{1, 2, 3}, {2, 6, 7}, {10, 15}};
        int expected = 15;
        int result = ExercicioXV.descobreMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMaiorValorMatrizComArraysVazios_v1() {
        int[][] matriz = {{6, 6, 20}, {10, 15}, {}};
        int expected = 20;
        int result = ExercicioXV.descobreMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMaiorValorMatrizComArraysVazios_v2() {
        int[][] matriz = {{-7, 3}, {}, {1, -100, 250}};
        int expected = 250;
        int result = ExercicioXV.descobreMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMaiorValorMatrizComArraysVazios_v3() {
        int[][] matriz = {{}, {-7, 3}};
        int expected = 3;
        int result = ExercicioXV.descobreMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDescobrirMaiorValorMatrizComValoresNegativosEZero() {
        int[][] matriz = {{-2, -6, 0}, {-10, -15}};
        int expected = 0;
        int result = ExercicioXV.descobreMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminaMediaElementosMatriz() {
        int[][] matriz = {{2, 6, 0}, {10, 5}};
        double expected = 4.6;
        Double result = ExercicioXV.determinaMediaElementosMatriz(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaDeterminaMediaElementosMatrizComArrayVazio() {
        int[][] matriz = {{2, 6, 0}, {}, {10, 5}};
        double expected = 4.6;
        Double result = ExercicioXV.determinaMediaElementosMatriz(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaDeterminaMediaElementosMatrizSóArrayVazios() {
        int[][] matriz = {{}, {}, {}};
        double expected = 0;
        Double result = ExercicioXV.determinaMediaElementosMatriz(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaDeterminaMediaElementosMatrizVazia() {
        int[][] matriz = {};
        Double result = ExercicioXV.determinaMediaElementosMatriz(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaCalcularProdutoDosElementosDaMatriz() {
        int[][] matriz = {{1, 4, 5}, {2, 6}};
        Integer expected = 240;
        Integer result = ExercicioXV.calcularProdutoDosElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularProdutoDosElementosDaMatrizComArrayVazio() {
        int[][] matriz = {{}, {1, 4, 5}, {2, 6}};
        Integer expected = 240;
        Integer result = ExercicioXV.calcularProdutoDosElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularProdutoDosElementosDaMatrizComTodosArraysVazio() {
        int[][] matriz = {{}, {}, {3}};
        Integer expected = 3;
        Integer result = ExercicioXV.calcularProdutoDosElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularProdutoElementosMatrizVazia() {
        int[][] matriz = {};
        Integer result = ExercicioXV.calcularProdutoDosElementosMatriz(matriz);
        assertNull(result);
    }

    /*@Test
    public void retornaMatrizSemElementosRepetidos() {
        int[][] matriz = {{4, 5, 5, 3}, {6, 4, 3,1}};
        int[][] expected = {{4, 5, 5, 3}, {6, 4, 3,1}};
        int[][] result = ExercicioXV.retornaMatrizSemElementosRepetidos(matriz);
        assertArrayEquals(expected, result);
    }
    */

    @Test
    public void testeParaCalcularTotalElementosMatriz_V1() {
        int[][] matriz = {{2, 3, 6}, {7, 9}, {5}};
        int expected = 6;
        int result = ExercicioXV.calcularTotalElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularTotalElementosMatriz_V2() {
        int[][] matriz = {{2, 3, 6}, {7, 9}, {5}, {}, {10, -9, -49, 0, 4}};
        int expected = 11;
        int result = ExercicioXV.calcularTotalElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularTotalElementosMatrizVazia() {
        int[][] matriz = {};
        int expected = 0;
        int result = ExercicioXV.calcularTotalElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularTotalElementosMatrizComArraysVazios() {
        int[][] matriz = {{}, {}};
        int expected = 0;
        int result = ExercicioXV.calcularTotalElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsElementosPrimosDaMatriz() {
        int[][] matriz = {{2, 3, 6}, {7, 9}, {5}};
        int[] expected = {2, 3, 7, 5};
        int[] result = ExercicioXV.determinaOsElementosPrimosDaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsElementosPrimosDaMatrizVazia() {
        int[][] matriz = {};
        int[] result = ExercicioXV.determinaOsElementosPrimosDaMatriz(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsElementosPrimosDaMatrizNull() {
        int[][] matriz = null;
        int[] result = ExercicioXV.determinaOsElementosPrimosDaMatriz(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsElementosPrimosDaMatrizComArraysVazios() {
        int[][] matriz = {{}, {}};
        int[] expected = {};
        int[] result = ExercicioXV.determinaOsElementosPrimosDaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayDeElementosNaoRepetidos_V1() {
        int[][] matriz = {{1, 5, 1, 8, 8}, {9, 1, 50}, {8, 5, 5}};
        int[] expected = {1, 5, 8, 9, 50};
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayDeElementosNaoRepetidos_V2() {
        int[][] matriz = {{1, 5, 1, 8, 0, 8}, {9, 1, 50}};
        int[] expected = {1, 5, 8, 0, 9, 50};
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayDeElementosUmaMatrizSemElementosRepetidos() {
        int[][] matriz = {{1, 5, 8, 101}, {9, 50}};
        int[] expected = {1, 5, 8, 101, 9, 50};
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayDeElementosNaoRepetidosDeUmaMatrizComArraysVazios() {
        int[][] matriz = {{1, 5, 1, 8, 101}, {9, 1, 50}, {}, {5, 4, 4}};
        int[] expected = {1, 5, 8, 101, 9, 50, 4};
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullMatrizVazia() {
        int[][] matriz = {};
        int[] expected = {};
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullDeUmaMatrizNull() {
        int[][] matriz = null;
        int[] result = ExercicioXV.determinaElementosNaoRepetidos(matriz);
        assertNull(result);
    }

    @Test
    public void testeParRetornarArrayComElementosDiagonalPrincipalDeMatrizQuadrada() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 6}, {9, 0, 1}};
        int[] expected = {1, 5, 1};
        int[] result = ExercicioXV.indicarElementosDiagonalPrincipal(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParRetornarArrayComElementosDiagonalPrincipalDeMatrizRetangular() {
        int[][] matriz = {{1, 3}, {4, 5}, {9, 0}, {3, 4}};
        int[] expected = {1, 5};
        int[] result = ExercicioXV.indicarElementosDiagonalPrincipal(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParRetornarNullConsiderandoAMatrizVazia() {
        int[][] matriz = {};
        int[] result = ExercicioXV.indicarElementosDiagonalPrincipal(matriz);
        assertNull(result);
    }

    @Test
    public void testeParRetornarNullConsiderandoAMatrizNull() {
        int[][] matriz = null;
        int[] result = ExercicioXV.indicarElementosDiagonalPrincipal(matriz);
        assertNull(result);
    }

    @Test
    public void testeParRetornarArrayComElementosDiagonalSecundariaDeMatrizQuadrada() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {9, 0, 1}};
        int[] expected = {5, 5, 9};
        int[] result = ExercicioXV.indicarElementosDiagonalSecundaria(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParRetornarArrayComElementosDiagonalSecundariaDeMatrizRetangulat() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {9, 0, 1}, {0, 5, -4}};
        int[] expected = {5, 5, 9};
        int[] result = ExercicioXV.indicarElementosDiagonalSecundaria(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParRetornarNullSendoAMatrizVazia() {
        int[][] matriz = {};
        int[] result = ExercicioXV.indicarElementosDiagonalSecundaria(matriz);
        assertNull(result);
    }

    @Test
    public void testeParRetornarNullSendoAMatrizNull() {
        int[][] matriz = null;
        int[] result = ExercicioXV.indicarElementosDiagonalSecundaria(matriz);
        assertNull(result);
    }

    @Test
    public void testeParRetornarNullQuandoAMatrizNaoEQuadradaNemRetangular() {
        int[][] matriz = {{1, 2}, {1, 0, 9, 8}};
        int[] result = ExercicioXV.indicarElementosDiagonalSecundaria(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaVerificarQueMatrizEIdentidade_V1() {
        int[][] matriz = {{1, 0}, {0, 1}};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertTrue(result);
    }

    @Test
    public void testeParaVerificarQueMatrizEIdentidade_V2() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertTrue(result);
    }

    @Test
    public void testeParaVerificarQueMatrizNaoEIdentidade_V1() {
        int[][] matriz = {{1, 5, 6, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    public void testeParaVerificarQueMatrizNaoEIdentidade_V2() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 5}};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarNullPorMatrizNaoSerQuadrada() {
        int[][] matriz = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 0, 1}};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarNullPorqueMatrizVazia() {
        int[][] matriz = {};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullPorqueMatrizNull() {
        int[][] matriz = {};
        Boolean result = ExercicioXV.verificarMatrizIdentidade(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaDeterminarMatrizTranspostaDeUmaMatrizRetangular() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}};
        int[][] expected = {{1, 4}, {3, 5}, {5, 0}};
        int[][] result = ExercicioXV.determinaMatrizTransposta(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizTranspostaDeUmaMatrizQuadrada() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}};
        int[][] expected = {{1, 4, 4}, {3, 5, 0}, {5, 0, 0}};
        int[][] result = ExercicioXV.determinaMatrizTransposta(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizTranspostaDeUmaMatrizNaoQuadradaNemRetangular() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}, {3}};
        int[][] result = ExercicioXV.determinaMatrizTransposta(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaDeterminarMatrizCofatores_V1() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}};
        int[][] expected = {{0, 0, -20}, {0, -20, 12}, {-25, 20, -7}};
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesNPorN(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizCofatores_V2() {
        int[][] matriz = {{1, 5}, {0, 2}};
        int[][] expected = {{2, 0}, {-5, 1}};
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesDoisPorDois(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizCofatores_V3() {
        int[][] matriz = {{1, 5}, {0, 2}};
        int[][] expected = {{2, 0}, {-5, 1}};
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesNPorN(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaReturnarNullQuandoMatrizENull() {
        int[][] matriz = null;
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesNPorN(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaReturnarNullQuandoMatrizEVazia() {
        int[][] matriz = {};
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesNPorN(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaReturnarNullQuandoMatrizNaoEQuadrada() {
        int[][] matriz = {{1, -1, 5}, {1, 3, 5}};
        int[][] result = ExercicioXV.determinaMatrizCofatoresMatrizesNPorN(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaDeterminarMatrizAdjunta() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}};
        int[][] expected = {{0, 0, -25}, {0, -20, 20}, {-20, 12, -7}};
        int[][] result = ExercicioXV.determinaMatrizAdjunta(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaReturnarNullSeMatrizNaoEQuadrada() {
        int[][] matriz = {{1, -1, 5}, {1, 3, 5}};
        int[][] result = ExercicioXV.determinaMatrizAdjunta(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaReturnarNullCasoMatrizSejaNull() {
        int[][] matriz = null;
        int[][] result = ExercicioXV.determinaMatrizAdjunta(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaReturnarNullCasoMatrizSejaVazia() {
        int[][] matriz = {};
        int[][] result = ExercicioXV.determinaMatrizAdjunta(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaDeterminarMatrizInversaDeMatriz2Por2() {
        int[][] matriz = {{2, 1}, {5, 3}};
        double[][] expected = {{3, -1}, {-5, 2}};
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizInversaDeMatriz3Por3() {
        int[][] matriz = {{1, 2, 3}, {0, 1, 4}, {0, 0, 1}};
        double[][] expected = {{1, -2, 5}, {0, 1, -4}, {0, 0, 1}};
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMatrizInversaDeMatriz4Por4() {
        int[][] matriz = {{1, 1, 1, 1}, {1, 2, -1, 2}, {1, -1, 2, 1}, {1, 3, 3, 2}};
        double[][] expected = {{2.33, -0.33, -0.33, -0.67}, {0.44, -0.11, -0.44, 0.11}, {-0.11, -0.22, 0.11, 0.22}, {-1.67, 0.67, 0.67, 0.33}};
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullSeDeterminanteForZero() {
        int[][] matriz = {{1, 0}, {5, 0}};
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullSeMatrizForNull() {
        int[][] matriz = null;
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullSeMatrizForVazia() {
        int[][] matriz = {};
        double[][] result = ExercicioXV.determinaMatrizInversa(matriz);
        assertNull(result);
    }
}