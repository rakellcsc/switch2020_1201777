package com.company.BlocoIV;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioVITest {

    @Test
    public void testeParaRetornarArrayComOsQuatroPrimeirosElementosDeUmArray() {
        int[] arrayInicial = {1, 2, 6, 7, 9, 0, 3};
        int numero = 4;
        int[] expected = {1,2,6,7};
        int[] result = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayInicial, numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComMaisElementosDeOArrayInicial() {
        int[] arrayInicial = {1, 2, 6, 7, 9, 0, 3};
        int numero = 10;
        int[] expected = {1, 2, 6, 7, 9, 0, 3};
        int[] result = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayInicial, numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComNumeroNegativoDeElementos() {
        int[] arrayInicial = {1, 2, 6, 7, 9, 0, 3};
        int numero = -1;
        int[] expected = {};
        int[] result = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayInicial, numero);
        assertArrayEquals(expected, result);
    }
}