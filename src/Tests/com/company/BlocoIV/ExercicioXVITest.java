package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXVITest {


    // Testes para o método identificaMatrizReduzida
    @Test
    public void testeParaRetornarMatrizReduzida_V1() {
        int[][] matriz = {{1, 2, 4}, {4, 5, 6}, {1, 4, 1}};
        int coordenada1 = 0;
        int coordenada2 = 0;
        int[][] expected = {{5, 6}, {4, 1}};
        int[][] result = ExercicioXVI.identificaMatrizReduzida(matriz, coordenada1, coordenada2);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarMatrizReduzida_V2() {
        int[][] matriz = {{1, 2, 4, 8}, {4, 5, 6, 4}, {1, 4, 1, 0}, {5, 6, 8, 1}};
        int coordenada1 = 2;
        int coordenada2 = 1;
        int[][] expected = {{1, 4, 8}, {4, 6, 4}, {5, 8, 1}};
        int[][] result = ExercicioXVI.identificaMatrizReduzida(matriz, coordenada1, coordenada2);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullQuandoMatrizNull() {
        int[][] matriz = null;
        int coordenada1 = 2;
        int coordenada2 = 1;
        int[][] result = ExercicioXVI.identificaMatrizReduzida(matriz, coordenada1, coordenada2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullQuandoMatrizNaoEQuadrada() {
        int[][] matriz = {{1, 2}, {3, 5}, {1, 2}};
        int coordenada1 = 2;
        int coordenada2 = 1;
        int[][] result = ExercicioXVI.identificaMatrizReduzida(matriz, coordenada1, coordenada2);
        assertNull(result);
    }

    // Testes para o método calculadeterminanteMatrizDoisPorDois
    @Test
    public void testeParaCalcularDeteterminanteDeMatrizDoisPorDois() {
        int[][] matriz = {{1, 2}, {3, 5}};
        int expected = -1;
        int result = ExercicioXVI.calculadeterminanteMatrizDoisPorDois(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullCasoMatrizNaoSejaDoisPorDois() {
        int[][] matriz = {{1, 2, 1}, {3, 5, 0}, {6, 5, 0}};
        Integer result = ExercicioXVI.calculadeterminanteMatrizDoisPorDois(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullCasoMatrizSejaVazia() {
        int[][] matriz = null;
        Integer result = ExercicioXVI.calculadeterminanteMatrizDoisPorDois(matriz);
        assertNull(result);
    }

    // Testes para o método calculadeterminanteMatrizNPorN
    @Test
    public void testeParaCalcularDeterminanteMatriz3Por3() {
        int[][] matriz = {{1,3,4}, {5,0,1},{1,2,3}};
        Integer expected = -4;
        Integer result = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaCalcularDeterminanteMatriz5Por5() {
        int[][] matriz = {{1,2,5, 3, 2}, {1, 3, 7, 3, 4},{0, 5, 2, 2, 1}, {1, 3, 0, 1, 2}, {0, 6, 7, 4, 7}};
        Integer expected = 148;
        Integer result = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullQuandoMatrizENull() {
        int[][] matriz = null;
        Integer result = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullQuandoMatrizEVazia() {
        int[][] matriz = {};
        Integer result = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullSeMatrizNaoEQuadrada() {
        int[][] matriz = {{1,2,4}, {3,5,6}};
        Integer result = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);
        assertNull(result);
    }
}