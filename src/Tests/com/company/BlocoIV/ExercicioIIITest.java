package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ExercicioIIITest {

    @Test
    public void testeParaSomarElementosDeUmArrayDeNumerosPositivos() {
        double[] listaNumeros = {3, 6, 7, 8, 1};
        double expected = 25;
        double result = ExercicioIII.somaElementosArrayDeNumeros(listaNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarElementosDeUmArrayComNumerosNegativosEPositivos() {
        double[] listaNumeros = {-5, -3, -2, 8, 1};
        double expected = -1;
        double result = ExercicioIII.somaElementosArrayDeNumeros(listaNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaSomarElementosDeUmArrayDeNumerosComElementoZero() {
        double[] listaNumeros = {0, -1, 1, 8, 1};
        double expected = 9;
        double result = ExercicioIII.somaElementosArrayDeNumeros(listaNumeros);
        assertEquals(expected, result, 0.01);
    }
}