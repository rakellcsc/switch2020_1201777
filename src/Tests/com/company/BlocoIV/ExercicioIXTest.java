package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioIXTest {

    @Test
    public void testeParaRetornarArrayComCapicuasDadoUmArrayDeNumeros() {
        int[] arrayNumeros= {1, 345, 343, 22, 980, 1001};
        int[] expected = {343, 22, 1001};
        int[] result = ExercicioIX.arrayDeCapicuas(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayVazioDadoUmArrayDeNumerosSemCapicua() {
        int[] arrayNumeros= {1, 345, 980, 1002};
        int[] expected = {};
        int[] result = ExercicioIX.arrayDeCapicuas(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComCapicuasDadoUmArrayComNumerosNegativos() {
        int[] arrayNumeros= {1, 345, -343, 22, 980, 1001};
        int[] expected = {22, 1001};
        int[] result = ExercicioIX.arrayDeCapicuas(arrayNumeros);
        assertArrayEquals(expected, result);
    }
}