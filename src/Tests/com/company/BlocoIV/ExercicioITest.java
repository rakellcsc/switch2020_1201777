package com.company.BlocoIV;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioITest {

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosDeUmNumeroPositivo() {
        int numero = 36781;
        int expected = 5;
        int result = ExercicioI.qtsAlgarismosDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosDoNumeroZero() {
        int numero = 0;
        int expected = 1;
        int result = ExercicioI.qtsAlgarismosDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosDeUmNumeroNegativo() {
        int numero = -36781;
        int expected = 0;
        int result = ExercicioI.qtsAlgarismosDeUmNumero(numero);
        assertEquals(expected, result);
    }
}