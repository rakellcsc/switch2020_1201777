package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXIIITest {

    @Test
    public void testeParaVerificarQueMatrizEQuadrada() {
        int[][] matriz = {{1, 2, 3}, {1, 5, 3}, {-4, -5, 0}};
        assertTrue(ExercicioXIII.verificarMatrizQuadrada(matriz));
    }

    @Test
    public void testeParaVerificarQueMatrizNaoEQuadrada_V1() {
        int[][] matriz = {{1, 2, 3}, {1, 5, 3}, {-4, -5, 0}, {1, 4}};
        assertFalse(ExercicioXIII.verificarMatrizQuadrada(matriz));
    }

    @Test
    public void testeParaVerificarQueMatrizNaoEQuadrada_V2() {
        int[][] matriz = {{1, 2}, {1, 0, 9, 8}};
        boolean result = ExercicioXIII.verificarMatrizQuadrada(matriz);
        assertFalse(result);
    }

    @Test
    public void testeParaVerificarQueMatrizSeMatrizVaziaEQuadrada() {
        int[][] matriz = {};
        assertFalse(ExercicioXIII.verificarMatrizQuadrada(matriz));
    }
}