package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXVIITest {

    @Test
    public void testeParaObterProdutoDeUmaMatrizPorConstante() {
        int[][] matriz = {{2, 0, 1}, {1, 3, 5}, {-5, 6, 10}};
        int constante = 3;
        int[][] expected = {{6, 0, 3}, {3, 9, 15}, {-15, 18, 30}};
        int[][] result = ExercicioXVII.multiplicarMatrizPorConstante(matriz, constante);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaObterProdutoDeUmaMatrizComArraysVaziosPorConstante() {
        int[][] matriz = {{}, {}, {}};
        int constante = 3;
        int[][] expected = {{}, {}, {}};
        int[][] result = ExercicioXVII.multiplicarMatrizPorConstante(matriz, constante);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaReturnarNullQuandoMatrizENull() {
        int[][] matriz = null;
        int constante = 3;
        int[][] result = ExercicioXVII.multiplicarMatrizPorConstante(matriz, constante);
        assertNull(result);
    }

    @Test
    public void testeParaReturnarNullQuandoMatrizNaoERetangularNemQuadrada() {
        int[][] matriz = {{2, 0, 1}, {1, 3}, {-5, 6, 10}};
        ;
        int constante = 3;
        int[][] result = ExercicioXVII.multiplicarMatrizPorConstante(matriz, constante);
        assertNull(result);
    }

    @Test
    public void testeParaObterProdutoDeUmaMatrizVaziaPorConstante() {
        int[][] matriz = {};
        int constante = 3;
        int[][] result = ExercicioXVII.multiplicarMatrizPorConstante(matriz, constante);
        assertNull(result);
    }

    @Test
    public void testeParaSomarMatrizesRetangularesComAMesmaDimensao() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = {{2, 2, 3}, {-4, 5, 0}};
        int[][] expected = {{3, 4, 6}, {0, 10, 6}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaSomarMatrizesQuadradasComAMesmaDimensao() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}, {2, -3, 0}};
        int[][] matriz2 = {{2, 2, 3}, {-4, 5, 0}, {1, -15, 9}};
        int[][] expected = {{3, 4, 6}, {0, 10, 6}, {3, -18, 9}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullComMatrizesQuadradasComADimensaoDiferente() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}, {2, -3, 0}};
        int[][] matriz2 = {{2, 2, 3, 0}, {-4, 5, 0, 10}, {1, -15, 9, -4}, {1, 2, 0, 3}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullComMatrizesRetangularesComADimensaoDiferente() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = {{2, 2, 3}, {-4, 5, 0}, {1, -15, 9}, {1, 2, 0}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullComMatriz1SendoNull() {
        int[][] matriz1 = null;
        int[][] matriz2 = {{2, 2, 3}, {-4, 5, 0}, {1, -15, 9}, {1, 2, 0}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullComMatriz2SendoNull() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = null;
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullComMatriz1Tamanho0() {
        int[][] matriz1 = {};
        int[][] matriz2 = {{2, 2, 3}, {-4, 5, 0}, {1, -15, 9}, {1, 2, 0}};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullComMatriz2Tamanho0() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = {};
        int[][] result = ExercicioXVII.somaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaMultiplicarDuasMatrizes() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = {{1, 4}, {2, 5}, {3, 6}};
        int[][] expected = {{14, 32}, {32, 77}};
        double[][] result = ExercicioXVII.multiplicaMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullSeNumeroLinhasMatriz1EDiferenteNumeroColunasMatriz2() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}, {}};
        int[][] matriz2 = {{1, 4}, {2, 5}, {3, 6}};
        double[][] result = ExercicioXVII.multiplicaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullSeMatriz1Null() {
        int[][] matriz1 = null;
        int[][] matriz2 = {{1, 4}, {2, 5}, {3, 6}};
        double[][] result = ExercicioXVII.multiplicaMatrizes(matriz1, matriz2);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullSeMatriz2Null() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}, {}};
        int[][] matriz2 = null;
        double[][] result = ExercicioXVII.multiplicaMatrizes(matriz1, matriz2);
        assertNull(result);
    }
}