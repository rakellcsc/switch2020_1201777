package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXIVTest {

    @Test
    public void testeParaVerificarQueMatrizNaoERetangular() {
        int[][] matriz = {{1, 2, 3}, {1, 5, 3}, {-4, -5, 0}};
        assertFalse(ExercicioXIV.verificarSeMatrizERetangular(matriz));
    }

    @Test
    public void testeParaVerificarQueMatrizNaoERetangular_V1() {
        int[][] matriz = {{1, 2, 3}, {1}, {1, 5, 3}, {-4, -5, 0}};
        assertFalse(ExercicioXIV.verificarSeMatrizERetangular(matriz));
    }

    @Test
    public void testeParaVerificarQueMatrizNaoERetangular_V2() {
        int[][] matriz = {{1, 2, 3, 5}, {1, 5}, {1}, {1, 4}};
        assertFalse(ExercicioXIV.verificarSeMatrizERetangular(matriz));
    }

    @Test
    public void testeParaVerificarSeUmaMatrizNaoERetangular_V3() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}, {3}};
        boolean result = ExercicioXIV.verificarSeMatrizERetangular(matriz);
        assertFalse(result);
    }

    @Test
    public void testeParaVerificarSeUmaMatrizERetangular() {
        int[][] matriz = {{1, 3, 5}, {4, 5, 0}, {4, 0, 0}, {3, 8, 9}};
        boolean result = ExercicioXIV.verificarSeMatrizERetangular(matriz);
        assertTrue(result);
    }

    @Test
    public void testeParaVerificarQueMatrizSeMatrizVaziaERetangular() {
        int[][] matriz = {};
        assertFalse(ExercicioXIV.verificarSeMatrizERetangular(matriz));
    }
}
