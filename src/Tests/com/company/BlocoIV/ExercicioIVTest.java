package com.company.BlocoIV;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioIVTest {

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosParesDeUmArraydeNumeros() {
        int[] listaNumeros = {3, 6, 7, 8, 1};
        int expected = 2;
        int result = ExercicioIV.qtsAlgarismosPares(listaNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosParesDeUmArrayComNumerosNegativosEComZero() {
        int[] listaNumeros = {3, -6, 7, 8, 0, 2, 1, 44};
        int expected = 4;
        int result = ExercicioIV.qtsAlgarismosPares(listaNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComOsAlgarismosParesDeUmArrayDeNumeros() {
        int[] listaNumeros = {3, 6, 7, 8, 1, 44};
        int[] expected = {6, 8, 44};
        int[] result = ExercicioIV.retornaAlgarismosParesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayVazioDeUmArraySemAlgarismosPares() {
        int[] listaNumeros = {3, 1, 7, 5, 1};
        int[] expected = {};
        int[] result = ExercicioIV.retornaAlgarismosParesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsAlgarismosParesDeUmArrayComNumerosNegativosEComZero() {
        int[] listaNumeros = {3, -2, 6, 0, 1};
        int[] expected = {6, 0};
        int[] result = ExercicioIV.retornaAlgarismosParesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosImparesDeUmArraydeNumeros() {
        int[] listaNumeros = {3, 6, 7, 8, 1};
        int expected = 3;
        int result = ExercicioIV.qtsAlgarismosImpares(listaNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantidadeAlgarismosImparesDeUmArrayComNumerosNegativosEComZero() {
        int[] listaNumeros = {-3, 6, 7, 8, 1, 0};
        int expected = 2;
        int result = ExercicioIV.qtsAlgarismosImpares(listaNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayComOsAlgarismosImparesDeUmArrayDeNumeros() {
        int[] listaNumeros = {3, 6, 7, 8, 1};
        int[] expected = {3, 7, 1};
        int[] result = ExercicioIV.retornaAlgarismosImparesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayVazioDeUmArraySemAlgarismosImpares() {
        int[] listaNumeros = {2, 6, 8, 0};
        int[] expected = {};
        int[] result = ExercicioIV.retornaAlgarismosImparesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArrayComOsAlgarismosImparesDeUmArrayComNumerosNegativosEComZero() {
        int[] listaNumeros = {-1, -3, 8, 0, 5, 9};
        int[] expected = {5, 9};
        int[] result = ExercicioIV.retornaAlgarismosImparesDeUmArray(listaNumeros);
        assertArrayEquals(expected, result);
    }
}