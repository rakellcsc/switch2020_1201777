package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXIITest {

    @Test
    public void testeParaVerificarSeNumeroDeColunasEIgualEmTodasAsLinhasDaMatriz_V1() {
        int[][] matriz = {{1, 2, 3}, {2, 4, 5}, {6, 7, -4}, {8, 9, 0}};
        int expected = 3;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarSeNumeroDeColunasEIgualEmTodasAsLinhasDaMatriz_V2() {
        int[][] matriz = {{1, 2, -3, -10, -5}, {2, 4, 5, 4, 4}, {6, 7, -4, 7, 7}, {8, 9, 0, 3, 4}};
        int expected = 5;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarQuandoNumeroDeColunasNaoEIgualEmTodasAsLinhasDaMatriz_V1() {
        int[][] matriz = {{1, 2}, {2, 4}, {6, 7, -4}, {8, 9, 0}};
        int expected = -1;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarQuandoNumeroDeColunasNaoEIgualEmTodasAsLinhasDaMatriz_V2() {
        int[][] matriz = {{1, 2, 7}, {2, 4, 9}, {7, -4, 9}, {8}};
        int expected = -1;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNumeroDeColunasQuandoMatrizContemArraysVazios_V1() {
        int[][] matriz = {{}, {}, {}};
        int expected = 0;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaVerificarNumeroDeColunasQuandoMatrizContemArraysVazios_V2() {
        int[][] matriz = {{}, {3, 4}, {}};
        int expected = -1;
        int result = ExercicioXII.verificarNumeroDeColunasMatriz(matriz);
        assertEquals(expected, result);
    }
}