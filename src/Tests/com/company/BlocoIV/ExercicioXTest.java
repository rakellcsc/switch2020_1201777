package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXTest {

    @Test
    public void testeParaDeterminarMenorValorArray() {
        int[] arrayNumeros = {3, 56, 98, 4};
        Integer expected = 3;
        Integer result = ExercicioX.menorValorArray(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMenorValorArrayComNumerosNegativos() {
        int[] arrayNumeros = {3, 56, 98, 4, -59};
        Integer expected = -59;
        Integer result = ExercicioX.menorValorArray(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMenorValorArrayVazio() {
        int[] arrayNumeros = {};
        Integer result = ExercicioX.menorValorArray(arrayNumeros);
        assertNull(result);
    }

    @Test
    public void testeParaDeterminarMaiorValorArray() {
        int[] arrayNumeros = {3, 56, 98, 4, -59};
        Integer expected = 98;
        Integer result = ExercicioX.maiorValorArray(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMaiorValorArrayComNumerosNegativosEZero() {
        int[] arrayNumeros = {-3, -56, -98, 0, -4, -59};
        Integer expected = 0;
        Integer result = ExercicioX.maiorValorArray(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarMaiorValorArrayVazio() {
        int[] arrayNumeros = {};
        Integer result = ExercicioX.maiorValorArray(arrayNumeros);
        assertNull(result);
    }

    @Test
    public void testeParaCalcularMediaElementosArray() {
        double[] arrayNumeros = {3, 10, 46, 20, 4, 1};
        double expected = 14;
        double result = ExercicioX.mediaElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaElementosArrayComNumerosNegativos() {
        double[] arrayNumeros = {-3, -10, -46, -20, -4, -1};
        double expected = -14;
        double result = ExercicioX.mediaElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularMediaElementosArrayVazio() {
        double[] arrayNumeros = {};
        double expected = 0;
        double result = ExercicioX.mediaElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularProdutoElementosArray() {
        int[] arrayNumeros = {2, 4, 7, 5};
        double expected = 280;
        double result = ExercicioX.produtoElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularProdutoElementosDeArrayVazio() {
        int[] arrayNumeros = {};
        double expected = 0;
        double result = ExercicioX.produtoElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaCalcularProdutoElementosDeArrayComNumerosNegativos() {
        int[] arrayNumeros = {-3, -5, -1};
        double expected = -15;
        double result = ExercicioX.produtoElementosArray(arrayNumeros);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void testeParaRetornarUmArraySemElementosRepetidos_V1() {
        int[] arrayNumeros = {3, 5, 1, 3, 5, 3};
        int[] expected = {1, 3, 5};
        int[] result = ExercicioX.retornarArraySemNumerosRepetidos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArraySemElementosRepetidos_V2() {
        int[] arrayNumeros = {3, 5, 1, 3, 3, 3, 4, 5, 5, 4, 4, 4, 5};
        int[] expected = {1, 3, 4, 5};
        int[] result = ExercicioX.retornarArraySemNumerosRepetidos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArraySemElementosRepetidosDeUmArrayComNumerosNegativos() {
        int[] arrayNumeros = {1, 3, -3, 3, 5, 5};
        int[] expected = {-3, 1, 3, 5};
        int[] result = ExercicioX.retornarArraySemNumerosRepetidos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarUmArraySemElementosRepetidosDeUmArrayComNumerosNegativosV1() {
        int[] arrayNumeros = {1, 3, -3, 5, 3};
        int[] expected = {-3, 1, 3, 5};
        int[] result = ExercicioX.retornarArraySemNumerosRepetidos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void retornarArrayInvertidoDeTresNumeros() {
        int[] arrayNumeros = {1, 4, 5};
        int[] expected = {5, 4, 1};
        int[] result = ExercicioX.arrayInvertido(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void retornarArrayInvertidoDeDezNumeros() {
        int[] arrayNumeros = {1, 4, 5, -3, 8, 1, 9, 0, -10, 7};
        int[] expected = {7, -10, 0, 9, 1, 8, -3, 5, 4, 1};
        int[] result = ExercicioX.arrayInvertido(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void retornarArrayInvertidoDeUmArrayVazio() {
        int[] arrayNumeros = {};
        int[] expected = {};
        int[] result = ExercicioX.arrayInvertido(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantosNumerosPrimosNumArray() {
        int[] arrayNumeros = {1, 4, 5, 7};
        int expected = 2;
        int result = ExercicioX.quantidadeNumerosPrimos(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarQuantosNumerosPrimosNumArraySemNenhumNumeroPrimo() {
        int[] arrayNumeros = {1, 4, 8, 9};
        int expected = 0;
        int result = ExercicioX.quantidadeNumerosPrimos(arrayNumeros);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayNumerosPrimos() {
        int[] arrayNumeros = {1, 4, 5, 8, 9, 3};
        int[] expected = {5, 3};
        int[] result = ExercicioX.obterNumerosPrimos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayNumerosPrimosNumArrayVazio() {
        int[] arrayNumeros = {};
        int[] expected = {};
        int[] result = ExercicioX.obterNumerosPrimos(arrayNumeros);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarArrayNumerosPrimosNumArraySemNenhumNumeroPrimo() {
        int[] arrayNumeros = {1, 4, 6, 8, 15};
        int[] expected = {};
        int[] result = ExercicioX.obterNumerosPrimos(arrayNumeros);
        assertArrayEquals(expected, result);
    }
}