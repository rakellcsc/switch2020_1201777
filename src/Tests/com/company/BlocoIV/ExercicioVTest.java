package com.company.BlocoIV;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioVTest {

    @Test
    public void testeParaSomarAlgarismosParesDeUmNumero() {
        int numero = 36781;
        int expected = 14;
        int result = ExercicioV.somaAlgarismosParesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosParesDeUmNumeroComApenasAlgarismosImpares() {
        int numero = 39751;
        int expected = 0;
        int result = ExercicioV.somaAlgarismosParesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosParesDeUmNumeroNegativo() {
        int numero = -39751;
        int expected = 0;
        int result = ExercicioV.somaAlgarismosParesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosParesDoNumeroZero() {
        int numero = 0;
        int expected = 0;
        int result = ExercicioV.somaAlgarismosParesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosImparesDeUmNumero() {
        int numero = 36781;
        int expected = 11;
        int result = ExercicioV.somaAlgarismosImparesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosImparesDeUmNumeroComApenasAlgarismosPares() {
        int numero = 60842;
        int expected = 0;
        int result = ExercicioV.somaAlgarismosImparesDeUmNumero(numero);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaSomarAlgarismosImparesDeUmNumeroNegativo() {
        int numero = -458392;
        int expected = 0;
        int result = ExercicioV.somaAlgarismosImparesDeUmNumero(numero);
        assertEquals(expected, result);
    }
}