package com.company.BlocoIV;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioIITest {

    @Test
    public void testeParaRetornarVetorComDigitosDeUmNumeroInteiroPositivo() {
        int numero = 123678;
        int[] expected = {1, 2, 3, 6, 7, 8};
        int[] result = ExercicioII.retornaVetorComDigitosDoNumero(numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarVetorComDigitosDeUmNumeroNegativo() {
        int numero = -123678;
        int[] expected = {0};
        int[] result = ExercicioII.retornaVetorComDigitosDoNumero(numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarVetorComDigitosDoNumeroZero() {
        int numero = 0;
        int[] expected = {0};
        int[] result = ExercicioII.retornaVetorComDigitosDoNumero(numero);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarVetorComLetrasDaPalavra() {
        String palavra = "Ola";
        char[] expected = {'O', 'L', 'A'};
        char[] result = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);
        assertArrayEquals(expected, result);
    }
}