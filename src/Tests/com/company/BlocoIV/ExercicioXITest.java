package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXITest {

    @Test
    public void testeParaDeterminarProdutoEscalarDeDoisVetoresComOMesmoTamanho_V1() {
        int[] vetor1 = {1, 3};
        int[] vetor2 = {2, 4};
        int expected = 14;
        int result = ExercicioXI.produtoEscalarVetores(vetor1, vetor2);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarProdutoEscalarDeDoisVetoresComOMesmoTamanho_V2() {
        int[] vetor1 = {1, 3, 5, -2};
        int[] vetor2 = {2, 4, 0, 9};
        int expected = -4;
        int result = ExercicioXI.produtoEscalarVetores(vetor1, vetor2);
        assertEquals(expected, result);
    }

    @Test
    public void testeParaDeterminarProdutoEscalarDeDoisVetoresComTamanhosDiferentes() {
        int[] vetor1 = {1, 3, 5, -2, 8};
        int[] vetor2 = {2, 4, 0, 9};
        int expected = 0;
        int result = ExercicioXI.produtoEscalarVetores(vetor1, vetor2);
        assertEquals(expected, result);
    }
}