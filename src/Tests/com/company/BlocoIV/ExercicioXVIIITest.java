package com.company.BlocoIV;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExercicioXVIIITest {

    // testes para verificar a matriz máscara de 0's e 1's
    @Test
    public void testeParaProduzirMatrizMascaraDadaUmaMatrizQuadradaDeLetras() {
        char[][]matriz = {{'A', 'B', 'C'}, {'A', 'R', 'C'}, {'S', 'S','A'}};
        char letra = 'A';
        int[][] expected = {{1, 0, 0}, {1, 0, 0}, {0, 0, 1}};
        int[][] result = ExercicioXVIII.produzMatrizMascara(matriz, letra);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testeParaRetornarNullCasoMatrizDadaNaoSejaQuadrada() {
        char[][]matriz = {{'A', 'B', 'C'}, {'A', 'R'}};
        char letra = 'A';
        int[][] result = ExercicioXVIII.produzMatrizMascara(matriz, letra);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullCasoMatrizDadaSejaNull() {
        char[][]matriz = null;
        char letra = 'A';
        int[][] result = ExercicioXVIII.produzMatrizMascara(matriz, letra);
        assertNull(result);
    }

    @Test
    public void testeParaRetornarNullCasoMatrizDadaSejaVazia() {
        char[][]matriz = {};
        char letra = 'A';
        int[][] result = ExercicioXVIII.produzMatrizMascara(matriz, letra);
        assertNull(result);
    }

    // testes para verificar as direções das palavras encontradas
    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizDaEsquerdaParaDireita() {
        char[][]matriz = {{'D', 'D', 'I', 'A'},
                          {'B', 'I', 'A', 'F'},
                          {'A', 'R', 'C', 'N'},
                          {'S', 'S','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDaEsquerdaParaADireita(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizDaEsquerdaParaDireita() {
        char[][]matriz = {{'B', 'I', 'A', 'J'},
                          {'B', 'I', 'A', 'D'},
                          {'A', 'R', 'C', 'I'},
                          {'S', 'S','A', 'A'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDaEsquerdaParaADireita(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizDeCimaParaBaixo() {
        char[][]matriz = {{'B', 'I', 'A', 'J'},
                          {'B', 'D', 'A', 'F'},
                          {'A', 'I', 'C', 'N'},
                          {'S', 'A','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDeCimaParaBaixo(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizDeCimaParaBaixo() {
        char[][]matriz = {{'D', 'I', 'A', 'J'},
                          {'B', 'T', 'A', 'F'},
                          {'A', 'I', 'C', 'N'},
                          {'S', 'A','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDeCimaParaBaixo(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizDaDireitaParaEsquerda() {
        char[][]matriz = {{'B', 'I', 'A', 'K'},
                          {'B', 'D', 'A', 'F'},
                          {'A', 'I', 'D', 'D'},
                          {'S', 'A','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDaDireitaParaAEsquerda(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizDaDireitaParaEsquerda() {
        char[][]matriz = {{'B', 'I', 'A', 'K'},
                          {'B', 'D', 'A', 'F'},
                          {'A', 'A', 'O', 'D'},
                          {'S', 'A','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDaDireitaParaAEsquerda(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizDeBaixoParaCima() {
        char[][]matriz = {{'B', 'I', 'A', 'K'},
                          {'B', 'I', 'I', 'F'},
                          {'A', 'I', 'D', 'D'},
                          {'S', 'D','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDeBaixoParaCima(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizDeBaixoParaCima() {
        char[][]matriz = {{'D', 'I', 'A', 'K'},
                          {'I', 'I', 'I', 'F'},
                          {'A', 'I', 'O', 'D'},
                          {'S', 'A','I', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizDeBaixoParaCima(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizNaDiagonalParaCimaEParaDireita() {
        char[][]matriz = {{'B', 'I', 'V', 'A'},
                          {'B', 'I', 'I', 'F'},
                          {'J', 'D', 'I', 'D'},
                          {'S', 'D','A', 'F'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaCimaDireita(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizNaDiagonalParaCimaEParaDireita() {
        char[][]matriz = {{'B', 'I', 'V', 'A'},
                          {'B', 'I', 'S', 'F'},
                          {'J', 'D', 'I', 'D'},
                          {'S', 'D','A', 'F'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaCimaDireita(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizNaDiagonalParaBaixoEParaDireita() {
        char[][]matriz = {{'B', 'I', 'V', 'P'},
                          {'B', 'D', 'I', 'F'},
                          {'J', 'D', 'I', 'D'},
                          {'S', 'D','A', 'A'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoDireita(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizNaDiagonalParaBaixoEParaDireita() {
        char[][]matriz = {{'B', 'I', 'V', 'P'},
                          {'B', 'D', 'I', 'F'},
                          {'J', 'D', 'I', 'D'},
                          {'S', 'D','A', 'O'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoDireita(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizNaDiagonalParaCimaEParaEsquerda() {
        char[][]matriz = {{'A', 'I', 'V', 'P'},
                          {'B', 'I', 'I', 'F'},
                          {'J', 'D', 'D', 'D'},
                          {'S', 'D','A', 'P'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaCimaEsquerda(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizNaDiagonalParaCimaEParaEsquerda() {
        char[][]matriz = {{'A', 'I', 'V', 'P'},
                          {'B', 'I', 'I', 'F'},
                          {'J', 'P', 'J', 'L'},
                          {'S', 'H','A', 'D'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaCimaEsquerda(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizNaDiagonalParaBaixoEParaEsquerda() {
        char[][]matriz = {{'A', 'I', 'V', 'P'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'I', 'D', 'D'},
                          {'A', 'D','A', 'P'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoEsquerda(matriz, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizNaDiagonalParaBaixoEParaEsquerda() {
        char[][]matriz = {{'A', 'I', 'V', 'P'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'I', 'D', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoEsquerda(matriz, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDaEsquerdaParaADireita_V1() {
        char[][]matriz = {{'A', 'D', 'I', 'A'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'I', 'D', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 1;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDaEsquerdaParaADireita_V2() {
        char[][]matriz = {{'A', 'D', 'I', 'A'},
                          {'B', 'I', 'D', 'F'},
                          {'D', 'I', 'A', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 2;
        int colunaInicial= 0;
        int linhaFinal= 2;
        int colunaFinal= 2;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizSegundoAsCoordenadasDaEsquerdaParaADireita() {
        char[][]matriz = {{'A', 'D', 'I', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'I', 'D', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 1;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizMasNaoSegundoAsCoordenadasDadasDaEsquerdaParaADireita() {
        char[][]matriz = {{'A', 'D', 'I', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'D', 'I', 'A'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 1;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }


    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDaDireitaParaAEsquerda_V1() {
        char[][]matriz = {{'A', 'D', 'I', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'A', 'I', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 2;
        int colunaInicial= 3;
        int linhaFinal= 2;
        int colunaFinal= 1;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDaDireitaParaAEsquerda_V2() {
        char[][]matriz = {{'A', 'I', 'D', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'A', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 2;
        int linhaFinal= 0;
        int colunaFinal= 0;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizSegundoAsCoordenadasDaDireitaParaAEsquerda() {
        char[][]matriz = {{'T', 'O', 'D', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'A', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 2;
        int linhaFinal= 0;
        int colunaFinal= 0;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizMasNaoSegundoAsCoordenadasDadasDaDireitaParaAEsquerda() {
        char[][]matriz = {{'T', 'O', 'D', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'A', 'I', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 2;
        int linhaFinal= 0;
        int colunaFinal= 1;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDeCimaParaBaixo_V1() {
        char[][]matriz = {{'A', 'D', 'I', 'O'},
                          {'B', 'I', 'D', 'F'},
                          {'J', 'A', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 0;
        int colunaInicial= 1;
        int linhaFinal= 2;
        int colunaFinal= 1;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDeCimaParaBaixo_V2() {
        char[][]matriz = {{'A', 'J', 'I', 'O'},
                          {'B', 'P', 'D', 'F'},
                          {'J', 'A', 'I', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 1;
        int colunaInicial= 2;
        int linhaFinal= 3;
        int colunaFinal= 2;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizSegundoAsCoordenadasDeCimaParaBaixo() {
        char[][]matriz = {{'D', 'J', 'I', 'O'},
                          {'B', 'P', 'D', 'F'},
                          {'J', 'A', 'H', 'D'},
                          {'O', 'D','B', 'O'}};
        String palavra = "Dia";
        int linhaInicial= 1;
        int colunaInicial= 2;
        int linhaFinal= 3;
        int colunaFinal= 2;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizMasNaoSegundoAsCoordenadasDadasDeCimaParaBaixo() {
        char[][]matriz = {{'D', 'J', 'I', 'O'},
                          {'B', 'P', 'D', 'D'},
                          {'J', 'A', 'H', 'I'},
                          {'O', 'D','B', 'A'}};
        String palavra = "Dia";
        int linhaInicial= 1;
        int colunaInicial= 2;
        int linhaFinal= 3;
        int colunaFinal= 2;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDeBaixoParaCima_V1() {
        char[][]matriz = {{'A', 'D', 'P', 'O'},
                          {'B', 'A', 'D', 'F'},
                          {'J', 'I', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 3;
        int colunaInicial= 1;
        int linhaFinal= 1;
        int colunaFinal= 1;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeRetornarTrueSePalavraEstaNaMatrizSegundoAsCoordenadasDeBaixoParaCima_V2() {
        char[][]matriz = {{'A', 'D', 'P', 'A'},
                          {'B', 'A', 'D', 'I'},
                          {'J', 'P', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 2;
        int colunaInicial= 3;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertTrue(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraNaoEstaNaMatrizSegundoAsCoordenadasBaixoParaCima() {
        char[][]matriz = {{'A', 'D', 'P', 'R'},
                          {'B', 'A', 'D', 'P'},
                          {'J', 'P', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 2;
        int colunaInicial= 3;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }

    @Test
    public void testeParaRetornarFalseSePalavraEstaNaMatrizMasNaoSegundoAsCoordenadasDadasBaixoParaCima() {
        char[][]matriz = {{'A', 'D', 'P', 'R'},
                          {'I', 'A', 'D', 'P'},
                          {'D', 'P', 'F', 'D'},
                          {'O', 'D','A', 'P'}};
        String palavra = "Dia";
        int linhaInicial= 2;
        int colunaInicial= 3;
        int linhaFinal= 0;
        int colunaFinal= 3;
        boolean result = ExercicioXVIII.verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavra);
        assertFalse(result);
    }
}