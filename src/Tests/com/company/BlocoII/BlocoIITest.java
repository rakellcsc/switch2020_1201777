package com.company.BlocoII;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlocoIITest {

    @Test
    public void calcMedia_V1() {
        int nota1 = 10;
        int nota2 = 10;
        int nota3 = 10;
        int peso1 = 50;
        int peso2 = 25;
        int peso3 = 25;

        double expected = 10;

        double result = BlocoII.calcMedia(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void calcMedia_V2() {
        int nota1 = 10;
        int nota2 = 4;
        int nota3 = 4;
        int peso1 = 50;
        int peso2 = 25;
        int peso3 = 25;

        double expected = 7;

        double result = BlocoII.calcMedia(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void escreverDigitos_V1() {
        int numero = 897;
        int digito1 = 8;
        int digito2 = 9;
        int digito3 = 7;

        String expected = "O número " + numero + " tem os dígitos " + (digito1 + " " + digito2 + " " + digito3) + " e é um número ímpar.";

        String result = BlocoII.escreverDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void escreverDigitos_V2() {
        int numero = 456;
        int digito1 = 4;
        int digito2 = 5;
        int digito3 = 6;

        String expected = "O número " + numero + " tem os dígitos " + (digito1 + " " + digito2 + " " + digito3) + " e é um número par.";

        String result = BlocoII.escreverDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void escreverDigitos_V3() {
        int numero = 15;

        String expected = "O número " + numero + " não tem três dígitos.";

        String result = BlocoII.escreverDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void escreverDigitos_V4() {
        int numero = 5000;

        String expected = "O número " + numero + " não tem três dígitos.";

        String result = BlocoII.escreverDigitos(numero);

        assertEquals(expected, result);
    }

    @Test
    public void distanciaEntrePontos_V1() {
        int x1 = 1;
        int y1 = 2;
        int x2 = -2;
        int y2 = 5;

        double expected = 4.24;

        double result = BlocoII.distanciaEntrePontos(x1, y1, x2, y2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void distanciaEntrePontos_V2() {
        int x1 = 0;
        int y1 = 0;
        int x2 = 3;
        int y2 = 2;

        double expected = 3.61;

        double result = BlocoII.distanciaEntrePontos(x1, y1, x2, y2);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio04_V1() {
        double x = 10;

        double expected = 80;

        double result = BlocoII.exercicio04(x);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio04_V2() {
        double x = 0;

        double expected = 0;

        double result = BlocoII.exercicio04(x);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio04_V3() {
        double x = -1;

        double expected = -1;

        double result = BlocoII.exercicio04(x);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void calcVolume_V1() {
        double area = 4;
        double volume = 0.544;

        String expected = "O volume do cubo é: " + String.format("%.2f", volume);

        String result = BlocoII.calcVolume(area);

        assertEquals(expected, result);
    }

    @Test
    public void calcVolume_V2() {
        double area = -2;

        String expected = "O valor da área é inválido.";

        String result = BlocoII.calcVolume(area);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoCubo_V1() {
        double area = 4;

        String expected = "O cubo é pequeno.";

        String result = BlocoII.classificacaoCubo(area);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoCubo_V2() {
        double area = 8;

        String expected = "O cubo é médio.";

        String result = BlocoII.classificacaoCubo(area);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoCubo_V3() {
        double area = 100;

        String expected = "O cubo é grande.";

        String result = BlocoII.classificacaoCubo(area);

        assertEquals(expected, result);
    }

    @Test
    public void conversaoTempo_v1() {
        double segundos = 456;
        int horasParteInteira = 0;
        int minutosParteInteira = 7;
        int segundosRestantesParteInteira = 36;

        String expected = "Boa noite! Hora atual:  " + "[" + horasParteInteira + " : " + minutosParteInteira + " : " + segundosRestantesParteInteira + "]";

        String result = BlocoII.conversaoTempo(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void conversaoTempo_v2() {
        double segundos = 26000;
        int horasParteInteira = 7;
        int minutosParteInteira = 13;
        int segundosRestantesParteInteira = 20;

        String expected = "Bom dia! Hora atual: " + "[" + horasParteInteira + " : " + minutosParteInteira + " : " + segundosRestantesParteInteira + "]";

        String result = BlocoII.conversaoTempo(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void conversaoTempo_v3() {
        double segundos = 45000;
        int horasParteInteira = 12;
        int minutosParteInteira = 30;
        int segundosRestantesParteInteira = 0;

        String expected = "Boa tarde! Hora atual: " + "[" + horasParteInteira + " : " + minutosParteInteira + " : " + segundosRestantesParteInteira + "]";

        String result = BlocoII.conversaoTempo(segundos);

        assertEquals(expected, result);
    }

    @Test
    public void custoMaoDeObra_V1() {
        double area = 60;
        double salarioPorDia = 20;

        double expected = 75;

        double result = BlocoII.custoMaoDeObra(salarioPorDia, area);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoMaoDeObra_V2() {
        double area = 200;
        double salarioPorDia = 20;

        double expected = 250;

        double result = BlocoII.custoMaoDeObra(salarioPorDia, area);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoTotal() {
        double salarioPorDia = 20;
        double area = 350;
        double custoLitroTinta = 10;
        double rendimento = 15;

        double expected = 677.5;

        double result = BlocoII.custoTotal(salarioPorDia, area, custoLitroTinta, rendimento);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoDaTinta_V1() {
        double area = 150;
        double custoLitroTinta = 25;
        double rendimento = 100;

        double expected = 50;

        double result = BlocoII. custoDaTinta(area, custoLitroTinta, rendimento);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoDaTinta_V2() {
        double area = 20;
        double custoLitroTinta = 6;
        double rendimento = 3;

        double expected = 42;

        double result = BlocoII. custoDaTinta(area, custoLitroTinta, rendimento);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void custoMaoDeObra_V3() {
        double area = 20;
        double salarioPorDia = 64;

        double expected = 80;

        double result = BlocoII.custoMaoDeObra(salarioPorDia, area);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void numerosMultiplos1() {
        int x = 10;
        int y = 5;

        String expected = "O número " + x + " é múltiplo de " + y + ".";

        String result = BlocoII.numerosMultiplos1(x, y);

        assertEquals(expected, result);
    }

    @Test
    public void numerosMultiplos2() {

        int x = 10;
        int y = 5;

        String expected = "O número "+ y + " não é múltiplo de " + x + ".";

        String result = BlocoII.numerosMultiplos2(x, y);

        assertEquals(expected, result);
    }

    @Test
    public void ordemCrescente_V1() {
        int numero = 178;

        String expected = "A sequência de algarismos é crecente";

        String result = BlocoII.ordemCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void ordemCrescente_V2() {
        int numero = 965;

        String expected = "A sequência de algarismos é decrescente";

        String result = BlocoII.ordemCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void ordemCrescente_V3() {
        int numero = 905;

        String expected = "A sequência de algarismos não é crescente nem decrescente";

        String result = BlocoII.ordemCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void ordemCrescente_V4() {
        int numero = 1000;

        String expected = "O número não tem três dígitos.";

        String result = BlocoII.ordemCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void ordemCrescente_V5() {
        int numero = 15;

        String expected = "O número não tem três dígitos.";

        String result = BlocoII.ordemCrescente(numero);

        assertEquals(expected, result);
    }

    @Test
    public void calcDesconto_V1() {
        double preco = 40;

        double expected = 32;

        double result = BlocoII.calcDesconto(preco);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void calcDesconto_V2() {
        double preco = -30;

        double expected = 0;

        double result = BlocoII.calcDesconto(preco);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void calcDesconto_V3() {
        double preco = 150;

        double expected = 90;

        double result = BlocoII.calcDesconto(preco);

        assertEquals(expected, result, 0.01);
    }


    @Test
    public void classifTurma_V1() {
        double aprovados = 5;

        String expected = "Valor inválido";

        String result = BlocoII.classifTurma(aprovados);

        assertEquals(expected, result);
    }

    @Test
    public void classifTurma_V2() {
        double aprovados = 0.4;

        String expected = "Turma fraca";

        String result = BlocoII.classifTurma(aprovados);

        assertEquals(expected, result);
    }

    @Test
    public void classifTurma1_V1() {
        double aprovados = 0.7;
        double limite1 = 0.3;
        double limite2 = 0.6;
        double limite3 = 0.8;
        double limite4 = 0.9;

        String expected = "Turma razoável";

        String result = BlocoII.classifTurma1(aprovados, limite1, limite2, limite3, limite4);

        assertEquals(expected, result);
    }

    @Test
    public void classifTurma1_V2() {
        double aprovados = 0.8;
        double limite1 = 0.8;
        double limite2 = 0.2;
        double limite3 = 0.1;
        double limite4 = 0.9;

        String expected = "Os limites introduzidos não estão corretos";

        String result = BlocoII.classifTurma1(aprovados, limite1, limite2, limite3, limite4);

        assertEquals(expected, result);
    }

    @Test
    public void classifTurma1_V3() {
        double aprovados = 0.9;
        double limite1 = 0.1;
        double limite2 = 0.3;
        double limite3 = 0.5;
        double limite4 = 0.7;

        String expected = "Turma excelente";

        String result = BlocoII.classifTurma1(aprovados, limite1, limite2, limite3, limite4);

        assertEquals(expected, result);
    }

    @Test
    public void nivelPoluicao_V1() {
        double indicePoluicao = 0.2;

        String expected = "O indice de poluição é aceitável";

        String result = BlocoII.nivelPoluicao(indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void nivelPoluicao_V2() {
        double indicePoluicao = 0.4;

        String expected = "As indústrias do 1º grupo devem suspender as suas atividades.";

        String result = BlocoII.nivelPoluicao(indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void nivelPoluicao_V3() {
        double indicePoluicao = 0.5;

        String expected = "As indústrias do 1º e do 2º grupo devem suspender as suas atividades.";

        String result = BlocoII.nivelPoluicao(indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void nivelPoluicao_V4() {
        double indicePoluicao = 0.7;

        String expected = "As indústrias do 1º, 2º e do 3º grupo devem suspender as suas atividades.";

        String result = BlocoII.nivelPoluicao(indicePoluicao);

        assertEquals(expected, result);
    }

    @Test
    public void nivelPoluicao_V5() {
        double indicePoluicao = -1;

        String expected = "O indice de poluição introduzido não é válido. Deverá ser um valor positivo.";

        String result = BlocoII.nivelPoluicao(indicePoluicao);

        assertEquals(expected, result);
    }


    @Test
    public void tempoServicoJardinagemParaAreaCinquentaNumeroArvoresDezNumeroArbustosQuinze() {
        double area = 50;
        int numero_arvores = 10;
        int numero_arbustos = 15;

        double expected = 7.5;

        double result = BlocoII.tempoServicoJardinagem(area, numero_arvores, numero_arbustos);

        assertEquals(expected, result, 0.01);
    }


    @Test
    public void custoServicoJardinagemParaAreaCinquentaNumeroArvoresDezNumeroArbustosQuinze() {
        double area = 50;
        int numero_arvores = 10;
        int numero_arbustos = 15;

        double expected = 1000;

        double result = BlocoII.custoServicoJardinagem(area, numero_arvores, numero_arbustos);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void quilometrosPercorridosEstafetaParaQuatrocentasMilhasDia1TrezantasMilhasDia2DuzentasMilhasDia3SetecentasMilhasDia4TrezentasMilhasDia5() {
        double dia1 = 400;
        double dia2 = 300;
        double dia3 = 200;
        double dia4 = 700;
        double dia5 = 300;

        double expected = 236.17;

        double result = BlocoII.quilometrosPercorridosEstafeta(dia1, dia2, dia3, dia4, dia5);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void classificacaoTriangulosParaValorDeAMenosUmValorDeBTresValorDeCQuatro() {
        double a = -1;
        double b = 3;
        double c = 4;

        String expected = "Não é possível construir um triangulo com os valores dados.";

        String result = BlocoII.classificacaoTriangulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoTriangulosParaValorDeADezValorDeBTresValorDeCQuatro() {
        double a = 10;
        double b = 3;
        double c = 4;

        String expected = "Não é possível construir um triangulo com os valores dados.";

        String result = BlocoII.classificacaoTriangulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoTriangulosParaValorDeADeBDeCIgualATres() {
        double a = 3;
        double b = 3;
        double c = 3;

        String expected = "O triângulo é equilátero.";

        String result = BlocoII.classificacaoTriangulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoTriangulosParaValorDeADeBIgualATresEDeCIgualAQuatro() {
        double a = 3;
        double b = 3;
        double c = 4;

        String expected = "O triângulo é isósceles.";

        String result = BlocoII.classificacaoTriangulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void classificacaoTriangulosParaValorDeACincoValorDeBTresValorDeCQuatro() {
        double a = 5;
        double b = 3;
        double c = 4;

        String expected = "O triângulo é escaleno.";

        String result = BlocoII.classificacaoTriangulos(a, b, c);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualAVinteAngulo2IgualACinquentaAngulo3IgualACinquenta() {
        double angulo1 = 20;
        double angulo2 = 50;
        double angulo3 = 50;

        String expected = "Não é possível construir um triângulo com esses dados.";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualAMenosCincoAngulo2IgualACinquentaAngulo3IgualACinquenta() {
        double angulo1 = -5;
        double angulo2 = 50;
        double angulo3 = 50;

        String expected = "Não é possível construir um triângulo com esses dados.";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualANoventaAngulo2IgualAQuarentaAngulo3IgualACinquenta() {
        double angulo1 = 90;
        double angulo2 = 40;
        double angulo3 = 50;

        String expected = "O triângulo é retângulo";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualATrintaAngulo2IgualANoventaAngulo3IgualASessenta() {
        double angulo1 = 30;
        double angulo2 = 90;
        double angulo3 = 60;

        String expected = "O triângulo é retângulo";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualATrintaAngulo2IgualASessentaAngulo3IgualANoventa() {
        double angulo1 = 30;
        double angulo2 = 60;
        double angulo3 = 90;

        String expected = "O triângulo é retângulo";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualASessentaAngulo2IgualASessentaAngulo3IgualASessenta() {
        double angulo1 = 60;
        double angulo2 = 60;
        double angulo3 = 60;

        String expected = "O triângulo é acutângulo";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void angulosTriangulosParaAngulo1IgualACemAngulo2IgualAQuarentaAngulo3IgualAQuarenta() {
        double angulo1 = 100;
        double angulo2 = 40;
        double angulo3 = 40;

        String expected = "O triângulo é obtusângulo";

        String result = BlocoII.angulosTriangulos(angulo1, angulo2, angulo3);

        assertEquals(expected, result);
    }

    @Test
    public void chegadaComboioHoraMinuto() {
        int horaPartida = 14;
        int minutoPartida = 50;
        int duracaoHoras = 1;
        int duracaoMinutos = 30;
        int horasChegadaParteInteira = 16;
        int minutosChegadaParteInteira = 20;

        String expected = "O comboio chega hoje às " + "[" + horasChegadaParteInteira + " : " + minutosChegadaParteInteira + "]";

        String result = BlocoII.chegadaComboioHoraMinuto(horaPartida, minutoPartida, duracaoHoras, duracaoMinutos);

        assertEquals(expected, result);
    }

    @Test
    public void testChegadaComboioHoraMinuto_V1() {
        int horaPartida = 14;
        int minutoPartida = 50;
        int duracaoHoras = 1;
        int duracaoMinutos = 30;
        int horaChegadaHoras = 16;
        int horaChegadaMinutos = 20;

        String expected = "O comboio chega hoje às " + "[" + horaChegadaHoras + " : " + horaChegadaMinutos + "]";

        String result = BlocoII.chegadaComboioHoraMinuto(horaPartida, minutoPartida, duracaoHoras, duracaoMinutos);

        assertEquals(expected, result);
    }

    @Test
    public void testChegadaComboioHoraMinuto_V2() {
        int horaPartida = 20;
        int minutoPartida = 30;
        int duracaoHoras = 5;
        int duracaoMinutos = 0;
        int horaChegadaHoras = 1;
        int horaChegadaMinutos = 30;

        String expected = "O comboio chega amanhã às " + "[" + horaChegadaHoras + " : " + horaChegadaMinutos + "]";

        String result = BlocoII.chegadaComboioHoraMinuto(horaPartida, minutoPartida, duracaoHoras, duracaoMinutos);

        assertEquals(expected, result);
    }

    @Test
    public void conversaoTempoHorasSegundos_V1() {
        int horasInicio = 3;
        int minutosInicio = 45;
        int segundosInicio = 30;

        int expected = 13530;

        int result = (int) BlocoII.conversaoTempoHorasSegundos(horasInicio, minutosInicio, segundosInicio);

        assertEquals(expected, result);
    }

    @Test
    public void conversaoTempoHorasSegundos_V2() {
        int horasInicio = - 3;
        int minutosInicio = 45;
        int segundosInicio = 30;

        int expected = 0;

        int result = (int) BlocoII.conversaoTempoHorasSegundos(horasInicio, minutosInicio, segundosInicio);

        assertEquals(expected, result);
    }


    @Test
    public void tempoProcessamento() {
        int horasInicio = 3;
        int minutosInicio = 20;
        int segundosInicio = 30;
        int segundosDuracao = 3720;
        int horaFinal = 4;
        int minutoFinal = 22;
        int segundoFinal = 30;

        String expected = "[" + horaFinal + " : " + minutoFinal + " : " + segundoFinal + "]";

        String result = BlocoII.tempoProcessamento(horasInicio, minutosInicio, segundosInicio, segundosDuracao);

        assertEquals(expected, result);
    }

    @Test
    public void salarioEmpregado_V1() {
        int horas = 50;

        double expected = 455;

        double result = BlocoII.salarioEmpregado(horas);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void salarioEmpregado_V2() {
        int horas = 36;

        double expected = 270;

        double result = BlocoII.salarioEmpregado(horas);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void salarioEmpregado_V3() {
        int horas = 41;

        double expected = 320;

        double result = BlocoII.salarioEmpregado(horas);

        assertEquals(expected, result, 0.01);
    }

    @Test
    public void salarioEmpregado_V4() {
        int horas = 30;

        double expected = 0;

        double result = BlocoII.salarioEmpregado(horas);

        assertEquals(expected, result, 0.01);
    }


    @Test
    public void valorAluguerJardinagem() {
        String kit = "A";
        String diaSemana = "Quarta-feira";

        int expected = 30;

        int result = BlocoII.valorAluguerJardinagem(kit, diaSemana);

        assertEquals(expected, result);
    }
}


