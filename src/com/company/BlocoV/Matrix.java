package com.company.BlocoV;

import com.company.BlocoIV.ExercicioVI;
import com.company.BlocoIV.ExercicioXIII;
import com.company.BlocoIV.ExercicioXIV;

import java.util.Arrays;

public class Matrix {

    //attributes
    private int[][] matrix;

    //constructors
    public Matrix() {
        this.matrix = new int[0][0];
    }

    public Matrix(int[][] matrix) {
        this.matrix = createCopyMatrix(matrix);
    }

    //Getter, Setter, Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix1 = (Matrix) o;

        if (this.matrix.length != matrix1.matrix.length) {
            return false;
        }

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (this.matrix[i][j] != matrix1.matrix[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    //business methods
    /**
     * create copy of the matrix
     * @param matrix original matrix
     * @return copy of the original matrix
     */
    private int[][] createCopyMatrix(int[][] matrix) {
        int[][] copyMatrix;

        if (matrix == null) {
            copyMatrix = new int[0][0];
        } else {
            copyMatrix = new int[matrix.length][];

            for (int i = 0; i < matrix.length; i++) {
                copyMatrix[i] = new int[matrix[i].length];
                for (int j = 0; j < matrix[i].length; j++) {
                    copyMatrix[i][j] = matrix[i][j];
                }
            }
        }
        return copyMatrix;
    }

    /**
     * convert the objet into array
     * @return results an array
     */
    public int[][] toMatrix() {
        return this.matrix;
    }

    /**
     * this method checks if the matrix is empty
     */
    private void checkIfMatrixIsEmpty() {
        if (this.matrix.length == 0)
            throw new IllegalArgumentException("Empty matrix");
    }

    /**
     * add element to the original matrix
     * @param value number that we want to add to the matrix
     * @param line line where the value is added
     */
    public void addElementToMatrix(int value, int line) {

        if (line >= this.matrix.length || line < 0) {
            throw new IllegalArgumentException("The line is invalid");
        }

        int[] tempArrayLine = new int[this.matrix[line].length + 1];
        int index = 0;

        for (int element : this.matrix[line]) {
            tempArrayLine[index] = element;
            index++;
        }

        tempArrayLine[this.matrix[line].length] = value;

        this.matrix[line] = tempArrayLine;
    }

    /**
     * add element to the Vector in the matrix
     * @param value number that we want to add to the matrix
     * @param line line where the value is added
     */
    public void addElementToVectorInMatrix(int value, int line) {

        if (line >= this.matrix.length || line < 0) {
            throw new IllegalArgumentException("The line is invalid");
        }

        Vector newVetor = new Vector(this.matrix[line]);

        newVetor.addElement(value);

        this.matrix[line] = newVetor.toArray();
    }

    /**
     *
     * @return a null matrix
     */
    public boolean isNull() {
        return this.matrix == null;
    }

    /**
     * add a Vector to the original matrix
     * @param array array that we want to add to the matrix
     */
    private void addVector(int[] array) {
        if (isNull()) {
            int[][] newMatrix = new int[1][array.length];
            System.arraycopy(array, 0, newMatrix[0], 0, array.length);
            this.matrix = newMatrix;
        } else {
            int[][] newMatrix = new int[this.matrix.length + 1][];
            for (int i = 0; i < this.matrix.length; i++) {
                newMatrix[i] = new int[this.matrix[i].length];
                System.arraycopy(this.matrix[i], 0, newMatrix[i], 0, this.matrix[i].length);
            }
            newMatrix[this.matrix.length] = new int[array.length];
            System.arraycopy(array, 0, newMatrix[this.matrix.length], 0, array.length);
            this.matrix = newMatrix;
        }
    }

    /*public void addVectorToMatrix(Vector vector) {
        if (isNull()) {
            int[][] newMatrix = new int[1][vector.toArray().length];
            System.arraycopy(vector.toArray(), 0, newMatrix[0], 0, vector.toArray().length);
            this.matrix = newMatrix;
        } else {
            Vector[] newMatrix = new Vector[this.vector.length + 1];

            for (int i = 0; i < this.vector.length; i++) {
                Vector arr = new Vector();
                for (int j = 0; j < this.vector[i].toArray().length; j++) {
                    arr.addElement(this.vector[i].returnElementGivenPosition(j));
                }
                newMatrix[i] = arr;
            }

            newMatrix[this.vector.length] = vector;
            this.vector = newMatrix;
        }
    }*/

    /**
     * removes element from the matrix given its index
     * @param value number that we want to remove
     * @param index index of the number we want to remove
     */
    private void removeElementFromMatrix(int value, int index) {

        int lineIndex = 0;

        int[] tempArrayLine = new int[this.matrix[index].length - 1];
        boolean checkEqual = false;

        for (int element : this.matrix[index]) {
            if (element == value && !checkEqual) {
                checkEqual = true;
            } else {
                tempArrayLine[lineIndex] = element;
                lineIndex++;
            }
        }
        this.matrix[index] = tempArrayLine;
    }

    /**
     * removes the first element from the matrix given its value
     * @param element number that we want to remove
     */
    public void removeFirstElementFromMatrix(int element) {
        checkIfMatrixIsEmpty();

        boolean checkEqual = false;

        for (int i = 0; i < this.matrix.length && !checkEqual; i++) {
            for (int j = 0; j < this.matrix[i].length && !checkEqual; j++) {
                if (this.matrix[i][j] == element) {
                    removeElementFromMatrix(element, i);
                    checkEqual = true;
                }
            }
        }
    }

    /**
     * check is the matrix is empty
     * @return true if the matrix is empty and false if the matrix is not empty
     */
    public boolean checkMatrixEmpty() {
        if (this.matrix.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * gets the biggest value of the matrix
     * @return the biggest value of the matrix
     */
    public int getMaxValue() {
        checkIfMatrixIsEmpty();
        return Utilities.getBiggestValueOfMatrix(this.matrix);
    }

    /**
     * gets the smallest value of the matrix
     * @return the smallest value of the matrix
     */
    public int getMinValue() {
        checkIfMatrixIsEmpty();
        return Utilities.getSmallestValueOfMatrix(this.matrix);
    }

    /**
     * gets the mean of the elements in the matrix
     * @return the mean of the elements in the matrix
     */
    public double getMean() {
        checkIfMatrixIsEmpty();
        return Utilities.getMeanOfMatrix(this.matrix);
    }

    /**
     * gets the sum of each row in the matrix
     * @return the sum of each row in the matrix
     */
    public Vector getSumOfEachRow() {
        checkIfMatrixIsEmpty();

        Vector sumElementsRow = new Vector();

        for (int[] row : this.matrix) {
            int sum = Utilities.intSumElementsOfArray(row);
            sumElementsRow.addElement(sum);
        }

        return sumElementsRow;
    }

    /**
     * gets the sum of each column in the matrix
     * @return the sum of each column in the matrix
     */
    public Vector getSumOfEachColumn() {
        checkIfMatrixIsEmpty();

        int[] temp = new int[determineLengthOfColumn()];

        for (int[] row : this.matrix) {
            for (int i = 0; i < row.length; i++) {
                temp[i] += row[i];
            }
        }
        return new Vector(temp);
    }

    /**
     * determine the lenght of a column in the matrix
     * @return the lenght of a column in the matrix
     */
    private int determineLengthOfColumn() {
        int lengthColumn = this.matrix[0].length;
        for (int[] row : this.matrix) {
            if (lengthColumn < row.length) {
                lengthColumn = row.length;
            }
        }
        return lengthColumn;
    }

    /**
     * determine the row index of the array with the largest sum of the respective elements
     * @return the row index of the array with the largest sum of the respective elements
     */
    public Integer indexBiggestSumRow() {

        int[] sumOfEachRow = getSumOfEachRow().toArray();

        for (int i = 0; i < sumOfEachRow.length; i++) {
            if (sumOfEachRow[i] == Utilities.getBiggestValueOfArray(sumOfEachRow)) {
                return i;
            }
        }
        return null;
    }

    /**
     * check if the matrix is square
     * @return true if the matrix is square, false if the matrix is not square
     */
    public boolean checkIfSquareMatrix() {
        checkIfMatrixIsEmpty();

        return Utilities.verifyIsSquareMatrix(this.matrix);
    }

    /**
     * check if the matrix is symmetric
     * @return true if the matrix is symmetric, false if the matrix is not symmetric
     */
    public boolean checkIfSymmetricMatrix() {
        checkIfMatrixIsEmpty();
        checkIfSquareMatrix();

        if (Arrays.deepEquals(Utilities.getTransposeMatrix(this.matrix), this.matrix)) {
            return true;
        }
        return false;
    }

    /**
     * calculate the number of elements in the main diagonal that are different that zero
     * @return the number of elements in the main diagonal that are different that zero
     */
    public int qtsElementsDiferentThanZeroInDiagonal() {
        int count = 0;

        if (!checkIfSquareMatrix()) {
            return -1;
        } else {
            for (int i = 0; i < this.matrix.length; i++) {
                if (this.matrix[i][i] != 0)
                    count += 1;
            }
        }
        return count;
    }

    /**
     * check if the Main Diagonal of the matrix is equal to the Secondary Diagonal of the matrix
     * @return true is the Main and Secondary Diagonal are equal, false if not equal
     */
    public boolean checkIfEqualMainAndSecondaryDiagonal() {
        checkIfMatrixIsEmpty();

        return getElementsMainDiagonal().equals(getElementsSecondaryDiagonal());

    }

    /**
     * get elements from the Main Diagonal
     * @return a Vector with the elements of the Main Diagonal
     */
    private Vector getElementsMainDiagonal() {
        if (Utilities.verifyIfRectangularMatrix(this.matrix) || checkIfSquareMatrix()) {
            Vector elementsMainDiagonal = new Vector();

            for (int i = 0; i < this.matrix.length; i++) {
                elementsMainDiagonal.addElement(this.matrix[i][i]);
            }
            return elementsMainDiagonal;
        } else {
            throw new IllegalArgumentException("Matrix should be square or rectangular");
        }
    }

    /**
     * get elements from the Secondary Diagonal
     * @return a Vector with the elements of the Secondary Diagonal
     */
    private Vector getElementsSecondaryDiagonal() {
        if (Utilities.verifyIfRectangularMatrix(this.matrix) || checkIfSquareMatrix()) {

            Vector elementsSecondaryDiagonal = new Vector();

            for (int i = 0; i < this.matrix.length; i++) {
                elementsSecondaryDiagonal.addElement(this.matrix[i][this.matrix[i].length - i - 1]);
            }
            return elementsSecondaryDiagonal;
        } else {
            throw new IllegalArgumentException("Matrix should be square or rectangular");
        }
    }

    /**
     * get all the elements of the matrix whose number of digits is greater than the average number of digits of all elements of the matrix
     * @return a Vector with all the elements of the matrix whose number of digits is greater than the average number of digits of all elements of the matrix
     */
    public Vector getBiggestThanAverage() {
        checkIfMatrixIsEmpty();

        Vector biggestDigitsThanAverage = new Vector();

        for (int i = 0; i < this.matrix.length; i++) {
            for (int j = 0; j < this.matrix[i].length; j++) {
                if (Utilities.qtsDigits(this.matrix[i][j]) > averageOfDigitsFromElementOfTheMatrix()) {
                    biggestDigitsThanAverage.addElement(this.matrix[i][j]);
                }
            }
        }
        return biggestDigitsThanAverage;
    }

    /**
     * calculate the mean of digits of the elements in the matrix
     * @return the mean of digits of the elements in the matrix
     */
    private double averageOfDigitsFromElementOfTheMatrix() {
        double sumDigits = 0;
        int totalElements = 0;

        for (int[] array : this.matrix) {
            for (int element : array) {
                sumDigits += Utilities.qtsDigits(element);
            }
            totalElements += array.length;
        }
        return Utilities.roundNumber((sumDigits / totalElements), 2);
    }

    /**
     * gets all elements of the matrix
     * @return a Vector with all elements of the matrix
     */
    private Vector returnAllElementsMatrix() {

        Vector allElementsMatrix = new Vector();

        for (int[] array : this.matrix) {
            for (int element : array) {
                allElementsMatrix.addElement(element);
            }
        }
        return allElementsMatrix;
    }

    /**
     * get all the elements of the matrix whose percentage of even numbers is greater than the average of the percentage of even numbers of all elements of the matrix
     * @return a Vector with all the elements of the matrix whose percentage of even numbers is greater than the average of the percentage of even numbers of all elements of the matrix
     */
    public Vector getElementsWithPercentageOfEvenNumbersBiggerThanAverage() {

        return returnAllElementsMatrix().returnElementsWithPercentageOfEvenNumbersBiggerThanAverage();

    }

    /**
     * inverts the order of the elements of each row in the matrix
     */
    public void getInvertedElementsRows() {

        Matrix invertedMatrix = new Matrix();

        for (int i = 0; i < this.matrix.length; i++) {

            int[] invertLine = Utilities.invertedArray(this.matrix[i]);

            invertedMatrix.addVector(invertLine);

        }
        this.matrix = invertedMatrix.toMatrix();
    }

    /**
     * inverts the order of the elements of each columns in the matrix
     */
    public void getInvertedElementsColumns() {

        Matrix invertedColumns = new Matrix();

        for (int i = this.matrix.length - 1; i >= 0; i--) {

            invertedColumns.addVector(this.matrix[i]);

        }

        this.matrix = invertedColumns.toMatrix();
    }

    /**
     * rotates the matrix ninety positive degrees
     */
    public void rotateMatrixNinetyDegreesPositive() {

        int[][] transposeMatrix = Utilities.getTransposeMatrix(this.matrix);

        Matrix rotateResult = new Matrix(transposeMatrix);

        rotateResult.getInvertedElementsRows();

        this.matrix = rotateResult.toMatrix();
    }

    /**
     * rotates the matrix ninety negative degrees
     */
    public void rotateMatrixNinetyDegreesNegative() {

        int[][] transposeMatrix = Utilities.getTransposeMatrix(this.matrix);

        Matrix rotateResult = new Matrix(transposeMatrix);

        rotateResult.getInvertedElementsColumns();

        this.matrix = rotateResult.toMatrix();
    }

    /**
     * rotates the matrix one hundred and eighty degrees
     */
    public void rotateMatrixHundredEighty() {

        Matrix rotateMatrix = new Matrix(this.matrix);

        rotateMatrix.getInvertedElementsRows();

        rotateMatrix.getInvertedElementsColumns();

        this.matrix = rotateMatrix.toMatrix();
    }
}
