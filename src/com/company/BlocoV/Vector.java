package com.company.BlocoV;


import java.util.Arrays;

public class Vector {

    //atributo
    private int[] vetor;

    //construtor
    public Vector() {
        this.vetor = new int[0];
    }

    public Vector(int[] vetor) {
        this.vetor = createCopy(vetor);
    }

    //Getter, Setter and Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;

        if (this.vetor.length != vector.vetor.length) {
            return false;
        } else {
            for (int i = 0; i < this.vetor.length; i++) {
                if (this.vetor[i] != vector.vetor[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    //métodos de negócio

    /**
     * create copy of the array
     *
     * @param array original array
     * @return copy of the original array
     */
    private int[] createCopy(int[] array) {
        int[] copyArray;

        if (array == null) {
            copyArray = new int[0];
        } else {
            copyArray = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                copyArray[i] = array[i];
            }
        }
        return copyArray;
    }

    /**
     * this method checks if the array is empty
     */
    private void checkIfEmpty() {
        if (this.vetor.length == 0) {
            throw new IllegalArgumentException("Empty array");
        }
    }

    /*private void checkIfNull() {
        if (this.vetor == null) {
            throw new IllegalArgumentException("The array should not be null");
        }
    }*/

    /**
     * convert the objet into array
     *
     * @return an array
     */
    public int[] toArray() {
        return this.vetor;
    }

    /**
     * add element to the original array
     *
     * @param value number that we want to add to the array
     */
    public void addElement(int value) {

        int[] newArray = new int[this.vetor.length + 1];

        for (int i = 0; i < this.vetor.length; i++) {
            newArray[i] = this.vetor[i];
        }

        newArray[this.vetor.length] = value;

        this.vetor = newArray;
    }

    /**
     * removes the first element from the matrix given its value
     *
     * @param element number that we want to remove
     */
    public void removeElement(int element) {
        checkIfEmpty();

        int[] newArray = new int[this.vetor.length - 1];
        int index = 0;

        for (int i = 0; i < this.vetor.length; i++) {
            if (this.vetor[i] == element)
                continue;
            newArray[index] = this.vetor[i];
            index++;
        }
        this.vetor = newArray;
    }

    /**
     * gets the value of an element indicated by its position
     *
     * @param position position of the element we want to get
     * @return the value of an element given its position
     */
    public int returnElementGivenPosition(int position) {
        checkIfEmpty();

        if (position >= this.vetor.length || position < 0) {
            throw new IllegalArgumentException("Position invalid");
        }
        return this.vetor[position];
    }

    /**
     * gets Vector length
     *
     * @return Vector length
     */
    public int length() {
        return this.vetor.length;
    }

    /**
     * gets the biggest element of the array
     *
     * @return the biggest element of the array
     */
    public int returnMaxValue() {
        checkIfEmpty();

        int max = this.vetor[0];

        for (int element : this.vetor) {
            if (element > max) {
                max = element;
            }
        }

        return max;
    }

    /**
     * gets the smallest element of the array
     *
     * @return the smallest element of the array
     */
    public int returnMinValue() {
        checkIfEmpty();

        int min = this.vetor[0];

        for (int element : this.vetor) {
            if (element < min) {
                min = element;
            }
        }

        return min;
    }

    /**
     * gets the mean of the elements in the array
     *
     * @return the mean of the elements in the array
     */
    public double averageElementsOfArray() {
        checkIfEmpty();

        return Utilities.averageElementsOfArray(this.vetor);
    }

    /**
     * gets the mean of even numbers in the array
     *
     * @return the mean of even numbers in the array
     */
    public double averageEvenNumbers() {
        double sumEvenNumbers = 0;
        int countEvenNumbers = 0;

        for (int element : this.vetor) {
            if (Utilities.evenNumber(element)) {
                sumEvenNumbers += element;
                countEvenNumbers++;
            }
        }

        return Utilities.roundNumber((sumEvenNumbers / countEvenNumbers), 2);
    }

    /**
     * gets the mean of odd numbers in the array
     *
     * @return the mean of odd numbers in the array
     */
    public double averageOddNumbers() {
        double sumOddNumbers = 0;
        int countOddNumbers = 0;

        for (int element : this.vetor) {
            if (Utilities.oddNumber(element)) {
                sumOddNumbers += element;
                countOddNumbers++;
            }
        }
        return Utilities.roundNumber((sumOddNumbers / countOddNumbers), 2);
    }

    /**
     * gets the average of all multiples of a given number
     *
     * @param number is the number we will calculate its multiples average
     * @return the average of all multiples of a given number
     */
    public double averageOfMultiplesOfANumber(int number) {
        checkIfEmpty();

        double sumMultiples = 0;
        int countMultiples = 0;

        for (int element : this.vetor) {
            if (element % number == 0) {
                sumMultiples += element;
                countMultiples++;
            }
        }
        return Utilities.roundNumber((sumMultiples / countMultiples), 2);
    }

    /**
     * sort array elements in ascending order
     */
    public void sortAscending() {
        this.vetor = Arrays.stream(this.vetor).sorted().toArray();
    }

    /**
     * sort array elements in descending order
     */
    public void sortDescending() {
        int temp;

        for (int i = 0; i < this.vetor.length; i++) {
            for (int j = i + 1; j < this.vetor.length; j++) {
                if (this.vetor[i] < this.vetor[j]) {
                    temp = this.vetor[i];
                    this.vetor[i] = this.vetor[j];
                    this.vetor[j] = temp;
                }
            }
        }
    }

    /**
     * checks if the array is empty
     *
     * @return true if the array is empty, false if the array is not empty
     */
    public boolean checkIfArrayIsEmpty() {
        if (this.vetor.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks if the array has only one element
     *
     * @return true if the array has one element, false if the array has more than one element or if it's empty
     */
    public boolean checkIfArrayHasOneElement() {
        if (this.vetor.length == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks if the array has only even numbers
     *
     * @return true if the array has only even numbers, false otherwise
     */
    public boolean checkIfArrayHasOnlyEvenNumbers() {
        int count = 0;

        for (int element : this.vetor) {
            if (Utilities.evenNumber(element)) {
                count++;
            }
        }

        if (count == this.vetor.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks if the array has only odd numbers
     *
     * @return true if the array has only odd numbers, false otherwise
     */
    public boolean checkIfArrayHasOnlyOddNumbers() {
        int count = 0;

        for (int element : this.vetor) {
            if (Utilities.oddNumber(element)) {
                count++;
            }
        }

        if (count == this.vetor.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check if the array has duplicate elements
     *
     * @return true if the array has duplicates elements, false otherwise
     */
    public boolean checkIfArrayHasDuplicates() {
        sortAscending();

        int count = 0;

        for (int i = 0; i < this.vetor.length; i++) {
            for (int j = i + 1; j < this.vetor.length; j++) {
                if (this.vetor[i] == this.vetor[j]) {
                    count++;
                }
            }
        }

        if (count != 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * calculate the mean of digits of the elements in the array
     *
     * @return the mean of digits of the elements in the array
     */
    private double averageOfDigitsFromElementOfTheArray() {
        double sumDigits = 0;

        for (int element : this.vetor) {
            sumDigits += Utilities.qtsDigits(element);
        }

        return Utilities.roundNumber((sumDigits / this.vetor.length), 2);
    }

    /**
     * gets the elements of the array whose number of digits is greater than the average number of digits of all elements of the array
     *
     * @return a Vector with the elements of the array whose number of digits is greater than the average number of digits of all elements of the array
     */
    public Vector returnElementsWithMostDigitsThanAverageDigits() {
        Vector arrayElementsWithMostDigits = new Vector();

        for (int element : this.vetor) {
            if (Utilities.qtsDigits(element) > averageOfDigitsFromElementOfTheArray()) {
                arrayElementsWithMostDigits.addElement(element);
            }
        }
        return arrayElementsWithMostDigits;
    }

    /**
     * calculates the percentage of even digits of each element of the array
     *
     * @return the percentage of even digits of each element of the array
     */
    private double averagePercentageOfEvenNumbersOfEachElement() {
        double percentage = 0;

        for (int element : this.vetor) {
            percentage += (Utilities.qtsEvenDigitsInANumber(element) / Utilities.qtsDigits(element));
        }

        return (percentage / this.vetor.length);
    }

    /**
     * get all the elements of the array whose number of digits is greater than the average number of digits of all elements of the array
     *
     * @return a Vector with all the elements of the array whose number of digits is greater than the average number of digits of all elements of the array
     */
    public Vector returnElementsWithPercentageOfEvenNumbersBiggerThanAverage() {
        Vector arrayElementsWithBiggerPercentageOfEvenNumbers = new Vector();

        for (int element : this.vetor) {
            double percentageEvenDigits = Utilities.qtsEvenDigitsInANumber(element) / Utilities.qtsDigits(element);

            if (percentageEvenDigits > averagePercentageOfEvenNumbersOfEachElement()) {
                arrayElementsWithBiggerPercentageOfEvenNumbers.addElement(element);
            }
        }
        return arrayElementsWithBiggerPercentageOfEvenNumbers;
    }

    /**
     * gets all elements of the array whose digits are even numbers
     *
     * @return a Vector with all elements of the array whose digits are even numbers
     */
    public Vector returnElementsWithOnlyEvenDigits() {
        Vector arrayElementsWithEvenDigits = new Vector();

        for (int element : this.vetor) {
            if (Utilities.qtsEvenDigitsInANumber(element) == Utilities.qtsDigits(element)) {
                arrayElementsWithEvenDigits.addElement(element);
            }
        }
        return arrayElementsWithEvenDigits;
    }

    /**
     * convert a number into an array which each element correspond to a digit of the given number
     *
     * @param number that we want to convert into array
     * @return an array with the digits of the given number
     */
    private int[] numberToArray(int number) {
        int size = Utilities.qtsDigits(number);

        int[] arrayDigits = new int[size];

        for (int i = size - 1; i >= 0; i--) {
            arrayDigits[i] = number % 10;
            number /= 10;
        }
        return arrayDigits;
    }

    /**
     * checks if the array is sorted, given a number
     *
     * @param number is the number we want to convert into array to check if is sorted
     * @return true if the number is sorted, false otherwise
     */
    private boolean checkIfArrayIsSorted(int number) {
        int[] arrayDigits = numberToArray(number);

        if (arrayDigits.length == 1) {
            return true;
        }

        for (int i = 1; i < arrayDigits.length; i++) {
            if (arrayDigits[i] < arrayDigits[i - 1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * gets all elements in the array that are sorted
     *
     * @return a Vector with all elements in the array that are sorted
     */
    public Vector returnElementsWithSortDigits() {
        Vector arrayElementWithSortedDigits = new Vector();

        for (int element : this.vetor) {
            if (checkIfArrayIsSorted(element)) {
                arrayElementWithSortedDigits.addElement(element);
            }
        }
        return arrayElementWithSortedDigits;
    }

    /**
     * gets all palindrome numbers in the array
     *
     * @return a Vector with all palindrome numbers in the array
     */
    public Vector returnPalindromeElements() {
        Vector arrayPalindrome = new Vector();

        for (int element : this.vetor) {
            if (Utilities.checkIfPalindromeNumber(element)) {
                arrayPalindrome.addElement(element);
            }
        }
        return arrayPalindrome;
    }

    /**
     * checks if a number has equal digits
     *
     * @param number is the number we want to check if has equal digits
     * @return true if the number has equal digits, false otherwise
     */
    private boolean checkIfNumberHasEqualDigits(int number) {

        int[] arrayNumber = numberToArray(number);

        int firstDigit = arrayNumber[0];

        for (int element : arrayNumber) {
            if (element != firstDigit) {
                return false;
            }
        }
        return true;
    }

    /**
     * get all elements of the array with equal digits
     *
     * @return a Vector with all elements of the array with equal digits
     */
    public Vector returnElementsWithEqualDigits() {
        Vector arraySameDigits = new Vector();

        for (int element : this.vetor) {
            if (checkIfNumberHasEqualDigits(element)) {
                arraySameDigits.addElement(element);
            }
        }
        return arraySameDigits;
    }

    /**
     * Get all Amstrong numbers in the array.
     *
     * @return a Vector with all Amstrong numbers in the array.
     */
    public Vector getAmstrongNumberFromArray() {
        Vector arrayAmstrong = new Vector();

        for (int element : this.vetor) {
            if (Utilities.amstrongNumber(element)) {
                arrayAmstrong.addElement(element);
            }
        }
        return arrayAmstrong;
    }

    /**
     * Get all elements of the array that contain an ascending sequence of at least n digits.
     *
     * @param n number of digits of the sequence.
     * @return a Vector with all elements of the array that contain an ascending sequence of at least n digits.
     */
    public Vector getAscendingElementsWithMoreThanNDigits(int n) {

        Vector arrayAscElements = new Vector();

        for (int element : this.vetor) {
            if (checkIfArrayIsSorted(element) && numberToArray(element).length >= n) {
                arrayAscElements.addElement(element);
            }
        }
        return arrayAscElements;
    }

    /**
     * Verify if the given array is equal to the original array.
     *
     * @param vetor Vector that we want to compare with the original array.
     * @return true if arrays are equal, false otherwise.
     */
    public boolean verifyIfEqualVector(Vector vetor) {

        int[] newArray = vetor.toArray();

        if (newArray.length != this.vetor.length) {
            return false;
        }

        for (int i = 0; i < this.vetor.length; i++) {
            if (this.vetor[i] != newArray[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Inverts the order of the elements of the Vector.
     *
     * @return a Vector with elements in an inverted order comparing to the given Vector.
     */
    public Vector invertedVector() {

        return new Vector(Utilities.invertedArray(this.vetor));

    }
}
