package com.company.BlocoV;

import com.company.BlocoIII.BlocoIII;
import com.company.BlocoIV.ExercicioVI;
import com.company.BlocoIV.ExercicioXIII;
import com.company.BlocoIV.ExercicioXIV;

public class Utilities {

    public static double averageElementsOfArray(int[] arrayNumbers) {
        double average;

        average = roundNumber(sumElementsOfArray(arrayNumbers) / arrayNumbers.length, 2);

        return average;
    }

    public static double sumElementsOfArray(int[] numberList) {
        double sum = 0;

        for (int i = 0; i < numberList.length; i++) {
            sum += numberList[i];
        }

        return sum;
    }

    public static int intSumElementsOfArray(int[] numberList) {
        int sum = 0;

        for (int i = 0; i < numberList.length; i++) {
            sum += numberList[i];
        }

        return sum;
    }

    public static double roundNumber(double number, int decimalDigits) {
        double result;

        result = Math.round(number * Math.pow(10, decimalDigits)) / (Math.pow(10, decimalDigits));

        return result;
    }

    public static boolean evenNumber(int number) {
        boolean even = false;
        if (number < 0) {
            number = -number;
        }

        if (number % 2 == 0) {
            return true;
        }
        return even;
    }

    public static boolean oddNumber(int number) {
        boolean odd = false;

        if (number < 0) {
            number = -number;
        }

        if (number % 2 == 1) {
            return true;
        }
        return odd;
    }

    public static int qtsDigits(long number) {
        int qtsDigits;
        String numberString;

        if (number >= 0) {
            numberString = Long.toString(number);
            qtsDigits = numberString.length();
        } else {
            numberString = Long.toString(number);
            qtsDigits = numberString.length() - 1;
        }
        return qtsDigits;
    }

    public static double qtsEvenDigitsInANumber(long number) {
        int countEven = 0;
        int digit;

        if (number < 0) {
            number = -number;
        }

        while (number != 0) {
            digit = (int) (number % 10);
            if (evenNumber(digit)) {
                countEven++;
            }
            number = number / 10;
        }
        return countEven;
    }

    public static int invertOrder(long number) {
        int digit, inverse = 0;

        while (number != 0) {
            digit = (int) (number % 10);
            inverse = inverse * 10 + digit;
            number = number / 10;
        }
        return inverse;
    }

    public static boolean checkIfPalindromeNumber(int number) {
        boolean palindrome = false;

        if (number < 10) {
            return palindrome;
        }

        if (number == invertOrder(number)) {
            palindrome = true;
        }
        return palindrome;
    }

    public static boolean amstrongNumber(int number) {
        int digit, sumCube = 0, compareNumber = number;

        while (number != 0) {
            digit = number % 10;
            sumCube = (int) (sumCube + Math.pow(digit, 3));
            number = number / 10;
        }

        if (compareNumber == sumCube) {
            return true;
        }
        return false;
    }

    public static int getBiggestValueOfMatrix(int[][] matrix) {

        int max = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempMax = getBiggestValueOfArray(matrix[i]);
                if (tempMax > max) {
                    max = tempMax;
                }
            }
        }
        return max;
    }

    public static int getBiggestValueOfArray(int[] numberArray) {
        int max = numberArray[0];

        for (int i = 1; i < numberArray.length; i++) {
            if (numberArray[i] > max) {
                max = numberArray[i];
            }
        }
        return max;
    }

    public static int getSmallestValueOfMatrix(int[][] matrix) {

        int smallest = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length != 0) {
                int tempSmallest = getSmallestValueOfArray(matrix[i]);
                if (tempSmallest < smallest) {
                    smallest = tempSmallest;
                }
            }
        }
        return smallest;
    }

    public static int getSmallestValueOfArray(int[] numberArray) {
        int smallest = numberArray[0];

        for (int i = 1; i < numberArray.length; i++) {
            if (numberArray[i] < smallest) {
                smallest = numberArray[i];
            }
        }
        return smallest;
    }

    public static Double getMeanOfMatrix(int[][] matrix) {
        double sum = 0;
        double totalElements = 0;
        double mean;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sum += matrix[i][j];
            }
            totalElements += matrix[i].length;
        }

        mean = roundNumber(sum / totalElements, 2);

        return mean;
    }

    public static boolean verifyIsSquareMatrix(int[][] matrix) {

        if (matrix.length == 0) {
            return false;
        }

        for (int[] line : matrix) {
            if (line.length != matrix.length) {
                return false;
            }
        }
        return true;
    }

    public static boolean verifyIfRectangularMatrix(int[][] matrix) {

        if (matrix.length == 0) {
            return false;
        }

        for (int i = 0; i < matrix.length - 1; i++) {
            if (matrix.length != matrix[i].length && matrix[i].length == matrix[i + 1].length) {

            } else {
                return false;
            }
        }
        return true;
    }

    public static int[][] getTransposeMatrix(int[][] matrix) {

        if (matrix == null || matrix.length == 0) {
            return null;
        }

        boolean squareMatrix = verifyIsSquareMatrix(matrix);
        boolean rectangularMatrix = verifyIfRectangularMatrix(matrix);

        if (squareMatrix || rectangularMatrix) {

            int[][] transposeMatrix = new int[matrix[0].length][matrix.length];

            for (int i = 0; i < matrix[0].length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    transposeMatrix[i][j] = matrix[j][i];
                }
            }
            return transposeMatrix;
        } else {
            return null;
        }
    }

    public static int[] invertedArray(int[] array) {

        int[] invertedArray = new int[array.length];

        if (array.length == 0) {
            return array;
        }

        invertedArray[0] = array[array.length - 1];
        invertedArray[invertedArray.length - 1] = array[0];

        for (int i = 1; i < array.length - 1; i++) {
            invertedArray[i] = array[invertedArray.length - i - 1];
        }

        return invertedArray;
    }
}
