package com.company.BlocoV.SudokuGame;

import java.util.Objects;

public class SudokuCell {

    private int otherNumber;
    private boolean fixedNumber;

    public SudokuCell(int otherNumber, boolean fixedNumber) {
        if(otherNumber > 9 || otherNumber < 0){
            throw new IllegalArgumentException("Invalid Number");
        }
        this.otherNumber = otherNumber;
        this.fixedNumber = fixedNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SudokuCell)) return false;
        SudokuCell cellNumber = (SudokuCell) o;
        return fixedNumber == cellNumber.fixedNumber && otherNumber == cellNumber.otherNumber;
    }

    public boolean checkIfIsFixedNumber() {
        return fixedNumber;
    }

    public int getOtherNumber() {
        return this.otherNumber;
    }

    public void changeNumber(int value) {
        if(value > 9 || value <= 0 ){
            throw new IllegalArgumentException("Invalid number");
        }

        if(fixedNumber) {
            throw new IllegalArgumentException("This number cannot be changed");
        } else{
            this.otherNumber = value;
        }
    }

    public void removeNonFixedValue(){
        if(fixedNumber){
            throw new IllegalArgumentException("This number cannot be deleted");
        }
        this.otherNumber = 0;
    }
}

