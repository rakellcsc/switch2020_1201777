package com.company.BlocoV.SudokuGame;

import com.company.BlocoV.Utilities;

import java.util.Arrays;

public class SudokuGameGrid {

    private final SudokuCell[][] gameGrid = new SudokuCell[9][9];

    public SudokuGameGrid(int[][] gameMatrix) {
        validateInitialMatrix(gameMatrix);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SudokuGameGrid)) return false;
        SudokuGameGrid grid = (SudokuGameGrid) o;

        if (this.gameGrid.length != grid.gameGrid.length) {
            return false;
        }

        for (int i = 0; i < this.gameGrid.length; i++) {
            for (int j = 0; j < this.gameGrid[i].length; j++) {
                if (this.gameGrid[i][j].getOtherNumber() != grid.gameGrid[i][j].getOtherNumber() || this.gameGrid[i][j].checkIfIsFixedNumber() != grid.gameGrid[i][j].checkIfIsFixedNumber()) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfValidLengthSudokuMatrix(int[][] matrix) {

        if (matrix == null) {
            return false;
        }

        if (!Utilities.verifyIsSquareMatrix(matrix)) {
            return false;
        } else {
            return matrix.length == 9 || matrix[0].length == 9;
        }
    }

    public void validateInitialMatrix(int[][] matrix) {

        if (!checkIfValidLengthSudokuMatrix(matrix)) {
            throw new IllegalArgumentException("Invalid game grid");
        }

        boolean fixedNumber;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] != 0) {
                    fixedNumber = true;
                } else {
                    fixedNumber = false;
                }
                SudokuCell cell = new SudokuCell(matrix[i][j], fixedNumber);
                this.gameGrid[i][j] = cell;
            }
        }
    }

    public void addCell(int value, int line, int column) {

        SudokuCell cellAdd = new SudokuCell(value, false);

        int[][] matrix = new int[9][9];

        for (int i = 0; i < this.gameGrid.length; i++) {
            for (int j = 0; j < this.gameGrid[i].length; j++) {
               matrix[i][j] = this.gameGrid[i][j].getOtherNumber();
            }
        }
        if (this.gameGrid[line][column].checkIfIsFixedNumber()) {
            throw new IllegalArgumentException("This is a fixed number");
        }
        matrix[line][column] = cellAdd.getOtherNumber();
        validateInitialMatrix(matrix);
    }



}
