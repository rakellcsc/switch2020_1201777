package com.company.BlocoIV;

public class ExercicioXVI {

    /**
     * Este método permite calcular o determinante de uma matriz com N linhas e N colunas
     * @param matriz recebe a matriz de números inteiros sobre a qual se pretende calcular o determinante
     * @return Retorna o valor inteiro do determinante da matriz dada
     */
    public static Integer calculaDeterminanteMatrizNPorN(int[][] matriz) {

        if (matriz == null) {
            return null;
        }

        if (!(ExercicioXIII.verificarMatrizQuadrada(matriz))) {
            return null;
        } else {

            int determinante = 0;
            int numeroLinhas = matriz.length;

            if (numeroLinhas == 2) {
                determinante = calculadeterminanteMatrizDoisPorDois(matriz);
            } else {
                for (int i = 0; i < numeroLinhas; i++) {
                    int[][] matrizReduzida = identificaMatrizReduzida(matriz, 0, i);
                    determinante += Math.pow(-1, 2 + i) * matriz[0][i] * calculaDeterminanteMatrizNPorN(matrizReduzida);
                }
            }
            return determinante;
        }
    }

    /**
     * Este método permite calcular o determinante de uma matriz com 2 linhas e 2 colunas
     * @param matriz recebe a matriz de números inteiros sobre a qual se pretende calcular o determinante
     * @return Retorna o valor inteiro do determinante da matriz dada
     */
    public static Integer calculadeterminanteMatrizDoisPorDois(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        int numeroLinhas = matriz.length;
        int numeroColunas = matriz[0].length;

        if (numeroLinhas != 2 && numeroColunas != 2) {
            return null;
        } else {

            int determinante = 0;
            int multiplicacaoDiagonalPrincipal = ExercicioX.produtoElementosArray(ExercicioXV.indicarElementosDiagonalPrincipal(matriz));
            int multiplicacaoDiagonalSecundaria = ExercicioX.produtoElementosArray(ExercicioXV.indicarElementosDiagonalSecundaria(matriz));

            if (numeroLinhas == 2 && numeroColunas == 2) {
                for (int i = 0; i < numeroLinhas; i++) {
                    for (int j = 0; j < numeroColunas; j++) {
                        determinante = multiplicacaoDiagonalPrincipal - multiplicacaoDiagonalSecundaria;
                    }
                }
            }
            return determinante;
        }
    }

    /**
     * Este método permite reduzir uma matriz retirando-lhe a linha e coluna indicadas
     * @param matriz recebe a matriz de números inteiros que se pretende reduzir
     * @param coordenada1 recebe o valor da linha a eliminar
     * @param coordenada2 recebe o valor da coluna a eliminar
     * @return Retorna a matriz reduzida consoante o valor da linha e coluna escolhidas para serem retiradas à matriz dada inicialmente
     */
    public static int[][] identificaMatrizReduzida(int[][] matriz, int coordenada1, int coordenada2) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        if (!(ExercicioXIII.verificarMatrizQuadrada(matriz))) {
            return null;
        } else {

            int indiceNovaMatriz1 = 0;
            int lenght = matriz.length - 1;

            int[][] matrizReduzida = new int[lenght][lenght];
            boolean verifica;

            for (int i = 0; i < matriz.length; i++) {
                int indiceNovaMatriz2 = 0;
                verifica = false;
                for (int j = 0; j < matriz.length; j++) {

                    if (i != coordenada1 && j != coordenada2) {
                        matrizReduzida[indiceNovaMatriz1][indiceNovaMatriz2] = matriz[i][j];
                        indiceNovaMatriz2++;
                        verifica = true;
                    }
                }
                if (verifica) {
                    indiceNovaMatriz1++;
                }
            }
            return matrizReduzida;
        }
    }
}



