package com.company.BlocoIV;

import com.company.BlocoI.Altura;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ExercicioXVIII {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        char[][] matriz = new char[][]{{'A', 'D', 'P', 'A', 'P', 'E', 'L'},
                                       {'O', 'A', 'E', 'P', 'D', 'K', 'M'},
                                       {'R', 'P', 'D', 'D', 'X', 'R', 'C'},
                                       {'U', 'E', 'R', 'P', 'V', 'P', 'A'},
                                       {'O', 'T', 'A', 'M', 'I', 'L', 'G'},
                                       {'J', 'R', 'I', 'P', 'B', 'R', 'A'},
                                       {'A', 'R', 'U', 'O', 'S', 'E', 'T'}};

        for (char[] val : matriz) {
            System.out.println(val);
        }

        String[] palavrasValidas = new String[]{"PAPEL", "PEDRA", "TESOURA", "OURO"};

        String continua = "S";

        while (!continua.equals("N")) {

            System.out.print("Indique a palavra que pretende encontrar: ");
            String palavra = scan.next();

            String palavraMaiusculas = palavra.toUpperCase();

            for (String val : palavrasValidas) {

                boolean palavraValida = false;
                if (val.equals(palavraMaiusculas)) {
                    palavraValida = true;
                }

                if (palavraValida) {
                    System.out.println("A palavra inserida é válida");
                    System.out.print("Linha inicial: ");
                    int linhaInicial = scan.nextInt();
                    System.out.print("Linha final: ");
                    int linhaFinal = scan.nextInt();
                    System.out.print("Coluna inicial: ");
                    int colunaInicial = scan.nextInt();
                    System.out.print("Coluna Final: ");
                    int colunaFinal = scan.nextInt();

                    if (verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavraMaiusculas)) {
                        System.out.println("Bem jogado!");
                    } else if (verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavraMaiusculas)) {
                        System.out.println("Bem jogado!");
                    } else if (verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavraMaiusculas)) {
                        System.out.println("Bem jogado!");
                    } else if (verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavraMaiusculas)) {
                        System.out.println("Bem jogado!");
                    } else if (verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(matriz, linhaInicial, colunaInicial, linhaFinal, colunaFinal, palavraMaiusculas)) {
                        System.out.println("Bem jogado!");
                    } else {
                        System.out.println("Tenta de novo!");
                    }
                }
            }


            System.out.println("Pretende continuar a jogar? Insira S para Sim ou N para Não");
            continua = scan.next();
        }

    }


    public static int[][] produzMatrizMascara(char[][] matriz, char palavra) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        int numeroLinhas = matriz.length;

        if (!(ExercicioXIII.verificarMatrizDeLetrasQuadrada(matriz))) {
            return null;
        }

        char palavraLetrasMaiusculas = Character.toUpperCase(palavra);

        int[][] matrizMascara = new int[numeroLinhas][numeroLinhas];

        for (int i = 0; i < numeroLinhas; i++) {
            for (int j = 0; j < numeroLinhas; j++) {
                if (matriz[i][j] != palavraLetrasMaiusculas) {
                    matrizMascara[i][j] = 0;
                } else {
                    matrizMascara[i][j] = 1;
                }
            }
        }
        return matrizMascara;
    }

    // Métodos para verificar se a palavra dada está numa determinada direção na sopa de letras

    public static boolean verificarSePalavraEstaNaMatrizDaEsquerdaParaADireita(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j <= (matriz.length - arrayPalavra.length) && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                verifica = verificarSePalavraEstaNaDirecaoEsquerdaDireita(matriz, verifica, arrayPalavra, i, j);
            }
        }
        return verifica;
    }

    private static boolean verificarSePalavraEstaNaDirecaoEsquerdaDireita(char[][] matriz, boolean verifica, char[] arrayPalavra, int i, int j) {
        for (int k = 1; k < arrayPalavra.length && verifica; k++) {
            if (matriz[i][j + k] != arrayPalavra[k]) {
                verifica = false;
            } else {
                verifica = true;
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizDeCimaParaBaixo(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i <= (matriz.length - arrayPalavra.length); i++) {
            for (int j = 0; j <= matriz.length - 1 && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                verifica = verificarSePalavraEstaNaDirecaoCimaBaixo(matriz, verifica, arrayPalavra, i, j);
            }
        }
        return verifica;
    }

    private static boolean verificarSePalavraEstaNaDirecaoCimaBaixo(char[][] matriz, boolean verifica, char[] arrayPalavra, int i, int j) {
        for (int k = 1; k < arrayPalavra.length && verifica; k++) {
            if (matriz[i + k][j] != arrayPalavra[k]) {
                verifica = false;
            } else {
                verifica = true;
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizDaDireitaParaAEsquerda(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = matriz.length - 1; j >= arrayPalavra.length - 1 && !verifica; j--) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                verifica = verificarSePalavraEstaNaDirecaoDireitaEsquerda(matriz, verifica, arrayPalavra, i, j);
            }
        }
        return verifica;
    }

    private static boolean verificarSePalavraEstaNaDirecaoDireitaEsquerda(char[][] matriz, boolean verifica, char[] arrayPalavra, int i, int j) {
        for (int k = 1; k < arrayPalavra.length - 1 && verifica; k++) {
            if (matriz[i][j - k] != arrayPalavra[k]) {
                verifica = false;
            } else {
                verifica = true;
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizDeBaixoParaCima(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = matriz.length - 1; i >= arrayPalavra.length - 1; i--) {
            for (int j = 0; j <= matriz.length - 1 && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                verifica = verificarSePalavraEstaNaDirecaoBaixoCima(matriz, verifica, arrayPalavra, i, j);
            }
        }
        return verifica;
    }

    private static boolean verificarSePalavraEstaNaDirecaoBaixoCima(char[][] matriz, boolean verifica, char[] arrayPalavra, int i, int j) {
        for (int k = 1; k < arrayPalavra.length - 1 && verifica; k++) {
            if (matriz[i - k][j] != arrayPalavra[k]) {
                verifica = false;
            } else {
                verifica = true;
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizNaDiagonalParaCimaDireita(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < arrayPalavra.length && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                for (int k = 1; k < arrayPalavra.length && verifica; k++) {
                    if (matriz[i - k][j + k] != arrayPalavra[k]) {
                        verifica = false;
                    } else {
                        verifica = true;
                    }
                }
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoDireita(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i <= (matriz.length - arrayPalavra.length); i++) {
            for (int j = 0; j <= (matriz.length - arrayPalavra.length) && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                for (int k = 1; k < arrayPalavra.length && verifica; k++) {
                    if (matriz[i + k][j + k] != arrayPalavra[k]) {
                        verifica = false;
                    } else {
                        verifica = true;
                    }
                }
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizNaDiagonalParaCimaEsquerda(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < arrayPalavra.length && !verifica; j++) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                for (int k = 1; k < arrayPalavra.length && verifica; k++) {
                    if (matriz[i - k][j - k] != arrayPalavra[k]) {
                        verifica = false;
                    } else {
                        verifica = true;
                    }
                }
            }
        }
        return verifica;
    }

    public static boolean verificarSePalavraEstaNaMatrizNaDiagonalParaBaixoEsquerda(char[][] matriz, String palavra) {

        boolean verifica = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        for (int i = 0; i <= (matriz.length - arrayPalavra.length); i++) {
            for (int j = matriz.length - 1; j > (matriz.length - arrayPalavra.length) && !verifica; j--) {
                if (matriz[i][j] == arrayPalavra[0] && !verifica) {
                    verifica = true;
                }
                for (int k = 1; k < arrayPalavra.length && verifica; k++) {
                    if (matriz[i + k][j - k] != arrayPalavra[k]) {
                        verifica = false;
                    } else {
                        verifica = true;
                    }
                }
            }
        }
        return verifica;
    }

    // Métodos para as direções das palavras segundo as coordenadas dadas
    public static boolean verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaEsquerdaParaADireita(char[][] matriz, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal, String palavra) {

        boolean verificarPalavra = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        if (linhaInicial == linhaFinal && colunaInicial < colunaFinal) {

            for (int i = linhaInicial; i <= linhaFinal; i++) {
                for (int j = colunaInicial; j <= colunaInicial; j++) {
                    if (matriz[i][j] == arrayPalavra[0] && !verificarPalavra) {
                        verificarPalavra = true;
                    }
                    verificarPalavra = verificarSePalavraEstaNaDirecaoEsquerdaDireita(matriz, verificarPalavra, arrayPalavra, i, j);
                }
            }
        }
        return verificarPalavra;
    }

    public static boolean verificarSePalavraEstaNaMatrizNasCoordenadasDadasDaDireitaParaAEsquerda(char[][] matriz, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal, String palavra) {

        boolean verificarPalavra = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        if (linhaInicial == linhaFinal && colunaInicial > colunaFinal) {

            for (int i = linhaInicial; i <= linhaFinal; i++) {
                for (int j = colunaInicial; j >= colunaFinal && !verificarPalavra; j--) {
                    if (matriz[i][j] == arrayPalavra[0] && !verificarPalavra) {
                        verificarPalavra = true;
                    }
                    verificarPalavra = verificarSePalavraEstaNaDirecaoDireitaEsquerda(matriz, verificarPalavra, arrayPalavra, i, j);
                }
            }
        }
        return verificarPalavra;
    }

    public static boolean verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeCimaParaBaixo(char[][] matriz, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal, String palavra) {

        boolean verificarPalavra = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        if (colunaInicial == colunaFinal && linhaFinal > linhaInicial) {
            for (int i = linhaInicial; i <= linhaFinal; i++) {
                for (int j = colunaInicial; j <= colunaFinal && !verificarPalavra; j++) {
                    if (matriz[i][j] == arrayPalavra[0] && !verificarPalavra) {
                        verificarPalavra = true;
                    }
                    verificarPalavra = verificarSePalavraEstaNaDirecaoCimaBaixo(matriz, verificarPalavra, arrayPalavra, i, j);
                }
            }
        }
        return verificarPalavra;
    }

    public static boolean verificarSePalavraEstaNaMatrizNasCoordenadasDadasDeBaixoParaCima(char[][] matriz, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal, String palavra) {

        boolean verificarPalavra = false;

        char[] arrayPalavra = ExercicioII.retornaVetorComLetrasDaPalavra(palavra);

        if (colunaInicial == colunaFinal && linhaInicial > linhaFinal) {

            for (int i = linhaInicial; i >= linhaFinal; i--) {
                for (int j = colunaInicial; j <= colunaFinal && !verificarPalavra; j++) {
                    if (matriz[i][j] == arrayPalavra[0] && !verificarPalavra) {
                        verificarPalavra = true;
                    }
                    verificarPalavra = verificarSePalavraEstaNaDirecaoBaixoCima(matriz, verificarPalavra, arrayPalavra, i, j);
                }
            }
        }
        return verificarPalavra;
    }
}

