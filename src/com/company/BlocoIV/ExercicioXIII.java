package com.company.BlocoIV;

public class ExercicioXIII {

    public static boolean verificarMatrizQuadrada(int[][] matriz){

        if(matriz.length == 0){
            return false;
        }

        for(int[] linha : matriz){
            if(linha.length != matriz.length){
                return false;
            }
        }
        return true;
    }

    public static boolean verificarMatrizDeLetrasQuadrada(char[][] matriz){

        if(matriz.length == 0){
            return false;
        }

        for (int i = 0; i < matriz.length-1; i++) {
            if(matriz.length == matriz[i].length && matriz.length == matriz[i+1].length){
                return true;
            }
        }
        return false;
    }
}
