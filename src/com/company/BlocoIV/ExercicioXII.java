package com.company.BlocoIV;

public class ExercicioXII {

    public static int verificarNumeroDeColunasMatriz(int[][] matriz) {

        for (int i = 0; i < matriz.length - 1; i++) {
            if (matriz[i].length != matriz[i + 1].length) {
                return -1;
            }
        }
        return matriz[0].length;
    }
}
        
        

