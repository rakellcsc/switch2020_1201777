package com.company.BlocoIV;

public class ExercicioIV {

    public static int qtsAlgarismosPares(int[] listaNumeros) {
        int contarAlgarismosPares = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            if (listaNumeros[i] % 2 == 0 && listaNumeros[i] >= 0) {
                contarAlgarismosPares++;
            }
        }
        return contarAlgarismosPares;
    }

    public static int[] retornaAlgarismosParesDeUmArray(int[] listaNumeros) {

        int[] listaAlarismosPares = new int[qtsAlgarismosPares(listaNumeros)];
        int indicePares = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            if (listaNumeros[i] % 2 == 0 && listaNumeros[i] >= 0) {
                listaAlarismosPares[indicePares] = listaNumeros[i];
                indicePares++;
            }
        }
        return listaAlarismosPares;
    }

    public static int qtsAlgarismosImpares(int[] listaNumeros) {
        int contarAlgarismosImpares = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            if (listaNumeros[i] % 2 == 1 && listaNumeros[i] >= 0) {
                contarAlgarismosImpares++;
            }
        }

        return contarAlgarismosImpares;
    }

    public static int[] retornaAlgarismosImparesDeUmArray(int[] listaNumeros) {

        int[] listaAlgarismosImpares = new int[qtsAlgarismosImpares(listaNumeros)];
        int indiceImpares = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            if (listaNumeros[i] % 2 == 1 && listaNumeros[i] >= 0) {
                listaAlgarismosImpares[indiceImpares] = listaNumeros[i];
                indiceImpares++;
            }
        }

        return listaAlgarismosImpares;
    }
}
