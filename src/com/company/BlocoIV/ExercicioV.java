package com.company.BlocoIV;

public class ExercicioV {

    public static int somaAlgarismosParesDeUmNumero(int numero) {
        int digito, somaPares = 0;

        if (numero < 0){
            return 0;
        }

        while (numero != 0) {
            digito = numero % 10;
            if(digito % 2 == 0){
                somaPares += digito;
            }
            numero /= 10;
        }

        return somaPares;
    }

    public static int somaAlgarismosImparesDeUmNumero(int numero) {
        int digito, somaImpares = 0;

        if (numero < 0){
            return 0;
        }

        while (numero != 0) {
            digito = numero % 10;
            if(digito % 2 == 1){
                somaImpares += digito;
            }
            numero /= 10;
        }

        return somaImpares;
    }
}
