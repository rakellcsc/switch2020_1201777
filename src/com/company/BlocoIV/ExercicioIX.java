package com.company.BlocoIV;

import com.company.BlocoIII.BlocoIII;

public class ExercicioIX {

    public static int[] arrayDeCapicuas(int[] arrayNumeros){
        int contadorCapicua = 0;

        for (int i = 0; i < arrayNumeros.length; i++) {
            if(BlocoIII.numeroCapicua(arrayNumeros[i])){
                contadorCapicua ++;
            }
        }

        int[] arrayCapicua = new int[contadorCapicua];
        int indiceCapicua = 0;

        for (int i = 0; i < arrayNumeros.length; i++) {
            if(BlocoIII.numeroCapicua(arrayNumeros[i])){
                arrayCapicua[indiceCapicua] = arrayNumeros[i];
                indiceCapicua ++;
            }
        }

        return arrayCapicua;
    }

}
