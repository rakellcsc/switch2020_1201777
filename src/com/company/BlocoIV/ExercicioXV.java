package com.company.BlocoIV;

import com.company.BlocoIII.BlocoIII;

public class ExercicioXV {


    // Exercicio 15 a)
    public static int descobreMenorValorMatriz(int[][] matriz) {

        int menor = matriz[0][0];

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length != 0) {
                int menorProvisorio = ExercicioX.menorValorArray(matriz[i]);
                if (menorProvisorio < menor) {
                    menor = menorProvisorio;
                }
            }
        }

        return menor;
    }

    // Exercicio 15 b)
    public static int descobreMaiorValorMatriz(int[][] matriz) {

        int maior = matriz [0][0];

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i].length != 0) {
                int maiorProvisorio = ExercicioX.maiorValorArray(matriz[i]);
                if (maiorProvisorio > maior) {
                    maior = maiorProvisorio;
                }
            }
        }

        return maior;
    }

    // Exercicio 15 c)
    public static Double determinaMediaElementosMatriz(int[][] matriz) {
        double soma = 0;
        double numeroTotalElementos = 0;
        double media;

        if (matriz.length == 0) {
            return null;
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                soma += matriz[i][j];
            }
            numeroTotalElementos += matriz[i].length;
        }

        media = BlocoIII.arredondarNumeros(soma / numeroTotalElementos, 2);

        return media;
    }

    // Exercicio 15 d)
    public static Integer calcularProdutoDosElementosMatriz(int[][] matriz) {
        int produto = 1;

        if (matriz.length == 0) {
            return null;
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                produto *= matriz[i][j];
            }
        }

        return produto;
    }

    public static int calcularTotalElementosMatriz(int[][] matriz) {

        int numeroElementos = 0;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                numeroElementos++;
            }
        }
        return numeroElementos;
    }

    // Exercicio 15 e)
    public static int[] determinaElementosNaoRepetidos(int[][] matriz) {

        if (matriz == null) {
            return null;
        }

        if (matriz.length == 0) {
            int[] matrizVazia = new int[0];
            return matrizVazia;
        }

        int[] arrayNaoRepetidos = new int[calcularTotalElementosMatriz(matriz)];

        arrayNaoRepetidos[0] = matriz[0][0];

        int indiceNaoRepetidos = 1;

        boolean eDiferente = true;

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {

                for (int k = 0; k < indiceNaoRepetidos && eDiferente; k++) {
                    eDiferente = arrayNaoRepetidos[k] != matriz[i][j];
                }
                if (eDiferente) {
                    arrayNaoRepetidos[indiceNaoRepetidos] = matriz[i][j];
                    indiceNaoRepetidos++;
                }
                eDiferente = true;
            }
        }

        int[] arrayNaoRepetidosTruncado = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayNaoRepetidos, indiceNaoRepetidos);

        return arrayNaoRepetidosTruncado;
    }

    // Exercicio 15 f)
    public static int[] determinaOsElementosPrimosDaMatriz(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        int indiceNumerosPrimos = 0;

        int[] arrayNumeroPrimos = new int[calcularTotalElementosMatriz(matriz)];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (ExercicioX.ePrimo(matriz[i][j])) {
                    arrayNumeroPrimos[indiceNumerosPrimos] = matriz[i][j];
                    indiceNumerosPrimos++;
                }
            }
        }
        return ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayNumeroPrimos, indiceNumerosPrimos);
    }

    // Exercicio 15 g)
    public static int[] indicarElementosDiagonalPrincipal(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        boolean matrizQuadrada = ExercicioXIII.verificarMatrizQuadrada(matriz);
        boolean matrizRetangular = ExercicioXIV.verificarSeMatrizERetangular(matriz);

        if (matrizQuadrada || matrizRetangular) {

            int indiceDiagonalPrincipal = 0;
            int[] arrayNumerosDiagonalPrincipal = new int[matriz.length];

            for (int i = 0; i < matriz[0].length; i++) {
                arrayNumerosDiagonalPrincipal[indiceDiagonalPrincipal] = matriz[i][i];
                indiceDiagonalPrincipal++;
            }
            int[] arrayNumerosDiagonalPrincipalTruncado = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayNumerosDiagonalPrincipal, indiceDiagonalPrincipal);
            return arrayNumerosDiagonalPrincipalTruncado;
        } else {
            return null;
        }
    }

    // Exercicio 15 h)
    public static int[] indicarElementosDiagonalSecundaria(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        boolean matrizQuadrada = ExercicioXIII.verificarMatrizQuadrada(matriz);
        boolean matrizRetangular = ExercicioXIV.verificarSeMatrizERetangular(matriz);

        if (matrizQuadrada || matrizRetangular) {

            int indiceDiagonalSecundaria = 0;
            int[] arrayNumerosDiagonalSecundaria = new int[matriz.length];

            for (int i = 0; i < matriz[0].length; i++) {
                arrayNumerosDiagonalSecundaria[indiceDiagonalSecundaria] = matriz[i][matriz[i].length - i - 1];
                indiceDiagonalSecundaria++;
            }
            int[] arrayNumerosDiagonalSecundarioTruncado = ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayNumerosDiagonalSecundaria, indiceDiagonalSecundaria);
            return arrayNumerosDiagonalSecundarioTruncado;
        } else {
            return null;
        }
    }

    // Exercicio 15 i)
    public static Boolean verificarMatrizIdentidade(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        boolean matrizQuadrada = ExercicioXIII.verificarMatrizQuadrada(matriz);

        if (matrizQuadrada) {

            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    if ((i == j && matriz[i][j] != 1) || (i != j && matriz[i][j] != 0)) {
                        return false;
                    }
                }
            }
        } else {
            return null;
        }
        return true;
    }

    // Exercicio 15 j)

    /**
     * Este método determina a matriz inversa de uma determinada matriz
     *
     * @param matriz recebe a matriz cuja inversa se pretende encontrar
     * @return Retorna a matriz inversa da matriz dada
     */
    public static double[][] determinaMatrizInversa(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        int determinante = ExercicioXVI.calculaDeterminanteMatrizNPorN(matriz);

        if (determinante == 0) {
            return null;
        }

        int numeroLinhas = matriz.length;
        int[][] matrizAdjunta = determinaMatrizAdjunta(matriz);
        double[][] matrizInversa = new double[numeroLinhas][numeroLinhas];

        for (int i = 0; i < numeroLinhas; i++) {
            for (int j = 0; j < numeroLinhas; j++) {
                matrizInversa[i][j] = BlocoIII.arredondarNumeros((double) matrizAdjunta[i][j] / determinante, 2);
            }
        }
        return matrizInversa;
    }

    /**
     * Este método permite encontrar a matriz dos cofactores de uma determinada matriz
     * @param matriz recebe a matriz que se pretende descobrir os cofatores
     * @return Retorna a matriz dos cofatores da matriz dada
     */
    public static int[][] determinaMatrizCofatoresMatrizesNPorN(int[][] matriz) {

        if (matriz == null || matriz.length == 0 || !ExercicioXIII.verificarMatrizQuadrada(matriz)) {
            return null;
        }

        int numeroLinhas = matriz.length;
        int[][] matrizCofatores = new int[numeroLinhas][numeroLinhas];

        if (numeroLinhas == 2) {
            matrizCofatores = determinaMatrizCofatoresMatrizesDoisPorDois(matriz);
        } else {

            for (int i = 0; i < numeroLinhas; i++) {
                for (int j = 0; j < numeroLinhas; j++) {
                    int[][] matrizReduzida = ExercicioXVI.identificaMatrizReduzida(matriz, i, j);
                    double cofator = (Math.pow(-1, i + j) * ExercicioXVI.calculaDeterminanteMatrizNPorN(matrizReduzida));
                    matrizCofatores[i][j] = (int) cofator;
                }
            }
        }
        return matrizCofatores;
    }

    /**
     * Este método permite encontrar a matriz dos cofactores de uma matriz com duas linhas e duas colunas
     * @param matriz recebe a matriz que se pretende descobrir os cofatores
     * @return Retorna a matriz dos cofatores da matriz dada
     */
    public static int[][] determinaMatrizCofatoresMatrizesDoisPorDois(int[][] matriz) {

        if (matriz == null || matriz.length != 2 || !ExercicioXIII.verificarMatrizQuadrada(matriz)) {
            return null;
        }

        int numeroLinhas = matriz.length;

        int[][] matrizCofatores = new int[numeroLinhas][numeroLinhas];

        matrizCofatores[0][0] = matriz[1][1];
        matrizCofatores[0][1] = (-1) * matriz[1][0];
        matrizCofatores[1][0] = (-1) * matriz[0][1];
        matrizCofatores[1][1] = matriz[0][0];

        return matrizCofatores;
    }

    /**
     * Este método permite determinar a matriz adjunta de uma determinada matriz, ou seja, determina a transposta da matriz dos cofatores de uma dada matriz
     * @param matriz recebe a matriz sobre a qual se pretende determinar a sua matriz adjunta
     * @return Retorna a matriz adjunta da matriz dada
     */
    public static int[][] determinaMatrizAdjunta(int[][] matriz) {

        int[][] matrizAdjunta = determinaMatrizTransposta(determinaMatrizCofatoresMatrizesNPorN(matriz));

        return matrizAdjunta;
    }


    // Exercicio 15 k)
    public static int[][] determinaMatrizTransposta(int[][] matriz) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        boolean matrizQuadrada = ExercicioXIII.verificarMatrizQuadrada(matriz);
        boolean matrizRetangular = ExercicioXIV.verificarSeMatrizERetangular(matriz);

        if (matrizQuadrada || matrizRetangular) {

            int[][] matrizTransposta = new int[matriz[0].length][matriz.length];

            for (int i = 0; i < matriz[0].length; i++) {
                for (int j = 0; j < matriz.length; j++) {
                    matrizTransposta[i][j] = matriz[j][i];
                }
            }
            return matrizTransposta;
        } else {
            return null;
        }
    }
}
