package com.company.BlocoIV;

public class ExercicioXI {

    public static boolean verificarTamanhoVetores(int[] vetor1, int[] vetor2) {

        if (vetor1.length == vetor2.length) {
            return true;
        } else {
            return false;
        }
    }

    public static int produtoEscalarVetores(int[] vetor1, int[] vetor2) {

        int produtoEscalar = 0;

        if (verificarTamanhoVetores(vetor1, vetor2)) {
            for (int i = 0; i < vetor1.length; i++) {
                produtoEscalar += (vetor1[i] * vetor2[i]);
            }
        } else {
            return 0;
        }

        return produtoEscalar;
    }
}
