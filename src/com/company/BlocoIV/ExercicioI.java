package com.company.BlocoIV;

public class ExercicioI {

    public static int qtsAlgarismosDeUmNumero(int numero) {
        int contagemAlgarismos = 0;

        if (numero < 0) {
            return 0;
        }

        if (numero == 0) {
            return 1;
        }

        while (numero != 0) {
            numero /= 10;
            contagemAlgarismos++;
        }

        return contagemAlgarismos;
    }
}


