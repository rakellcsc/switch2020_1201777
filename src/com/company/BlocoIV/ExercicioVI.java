package com.company.BlocoIV;

public class ExercicioVI {

    public static int[] retornaArrayComDeterminadoNumeroDeElementos(int[] arrayInicial, int numeroElementos) {

        if(numeroElementos <= 0){
            return new int[0];
        }

        if(arrayInicial.length < numeroElementos){
            return arrayInicial;
        }

        int[] novoArray = new int[numeroElementos];

        for (int i = 0; i < novoArray.length; i++) {
            novoArray[i] = arrayInicial[i];
        }

        return novoArray;
    }
}
