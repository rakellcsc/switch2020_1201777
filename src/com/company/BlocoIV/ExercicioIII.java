package com.company.BlocoIV;

public class ExercicioIII {

    public static double somaElementosArrayDeNumeros(double[] listaNumeros){
        double soma = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            soma += listaNumeros[i];
        }

        return soma;
    }

    public static double somaElementosArrayDeNumerosInteiros(int[] listaNumeros){
        double soma = 0;

        for (int i = 0; i < listaNumeros.length; i++) {
            soma += listaNumeros[i];
        }

        return soma;
    }
}
