package com.company.BlocoIV;

public class ExercicioII {

    public static int qtsAlgarismosDeUmNumero(int numero) {
        int contagemAlgarismos = 0;

        if (numero < 0) {
            return 0;
        }

        if (numero == 0) {
            return 1;
        }

        while (numero != 0) {
            numero /= 10;
            contagemAlgarismos++;
        }

        return contagemAlgarismos;
    }

    public static int[] retornaVetorComDigitosDoNumero(int numero){
        int digito;

        if(numero <= 0){
            return new int[1];
        }

        int[] lista = new int[qtsAlgarismosDeUmNumero(numero)];

        while (numero != 0) {
            for (int i = lista.length -1; i >= 0; i--) {
                digito = numero % 10;
                numero /= 10;
                lista[i] = digito;
            }
        }
        return lista;
    }

    public static char[] retornaVetorComLetrasDaPalavra(String palavra){

        String letrasMaiusculas = palavra.toUpperCase();

        char[] arrayDeLetras = letrasMaiusculas.toCharArray();

        return arrayDeLetras;
    }
}
