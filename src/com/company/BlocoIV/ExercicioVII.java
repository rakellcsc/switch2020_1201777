package com.company.BlocoIV;

public class ExercicioVII {

    public static int contarMultiplosDeTresNumIntervalo(int minimo, int maximo) {
        int contaMultiplos = 0;
        int numero = minimo;

        if (maximo < minimo) {
            minimo = maximo;
            maximo = numero;
        }

        for (int i = minimo; i <= maximo; i++) {
            if (i % 3 == 0 && i >= 0) {
                contaMultiplos++;
            }
        }

        return contaMultiplos;
    }

    public static int[] retornaArrayComMultiplosDeTres(int minimo, int maximo) {
        int numero = minimo;

        if (maximo < minimo) {
            minimo = maximo;
            maximo = numero;
        }

        int[] arrayMultiplosDe3 = new int[contarMultiplosDeTresNumIntervalo(minimo, maximo)];

        int indiceMultiplos = 0;

        for (int i = minimo; i <= maximo; i++) {
            if (i % 3 == 0 && i >= 0) {
                arrayMultiplosDe3[indiceMultiplos] = i;
                indiceMultiplos++;
            }
        }
        return arrayMultiplosDe3;
    }

    public static int contarMultiplosDeUmNumeroNumIntervalo(int minimo, int maximo, int numero) {
        int contaMultiplos = 0;
        int altera = minimo;

        if (maximo < minimo) {
            minimo = maximo;
            maximo = altera;
        }

        for (int i = minimo; i <= maximo; i++) {
            if (i % numero == 0 && i >= 0) {
                contaMultiplos++;
            }
        }

        return contaMultiplos;
    }

    public static int[] retornaArrayComMultiplosDeUmNumero(int minimo, int maximo, int numero) {
        int altera = minimo;

        if (maximo < minimo) {
            minimo = maximo;
            maximo = altera;
        }

        if(numero <= 0){
            return new int[0];
        }

        int[] arrayMultiplosDeN = new int[contarMultiplosDeUmNumeroNumIntervalo(minimo, maximo, numero)];

        int indiceMultiplos = 0;

        for (int i = minimo; i <= maximo; i++) {
            if (i % numero == 0 && i >= 0) {
                arrayMultiplosDeN[indiceMultiplos] = i;
                indiceMultiplos++;
            }
        }
        return arrayMultiplosDeN;
    }
}
