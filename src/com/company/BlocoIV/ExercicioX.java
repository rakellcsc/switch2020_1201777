package com.company.BlocoIV;

import com.company.BlocoIII.BlocoIII;

import java.util.Arrays;

public class ExercicioX {

    public static Integer menorValorArray(int[] arrayNumeros) {
        if(arrayNumeros.length == 0){
            return null;
        }

        int menor = arrayNumeros[0];

        for (int i = 1; i < arrayNumeros.length; i++) {
                if (arrayNumeros[i] < menor) {
                    menor = arrayNumeros[i];
            }
        }
        return menor;
    }

    public static Integer maiorValorArray(int[] arrayNumeros) {
        if(arrayNumeros.length == 0){
            return null;
        }

        int maior = arrayNumeros[0];

        for (int i = 1; i < arrayNumeros.length; i++) {
            if (arrayNumeros[i] > maior) {
                maior = arrayNumeros[i];
            }
        }

        return maior;
    }

    public static double mediaElementosArray(double[] arrayNumeros) {
        double media;

        media = BlocoIII.arredondarNumeros(ExercicioIII.somaElementosArrayDeNumeros(arrayNumeros) / arrayNumeros.length, 2);

        return media;
    }

    public static double mediaElementosArrayInteiros(int[] arrayNumeros) {
        double media;

        media = BlocoIII.arredondarNumeros(ExercicioIII.somaElementosArrayDeNumerosInteiros(arrayNumeros) / arrayNumeros.length, 2);

        return media;
    }

    public static int produtoElementosArray(int[] arrayNumeros) {
        int produto = 1;

        if (arrayNumeros.length == 0) {
            return 0;
        }

        for (int i = 0; i < arrayNumeros.length; i++) {
            produto *= arrayNumeros[i];
        }

        return produto;
    }

    public static int[] retornarArraySemNumerosRepetidos(int[] arrayNumeros) {
        int quantidadeNaoRepetidos = 0;

        Arrays.sort(arrayNumeros);

        int[] arrayNumerosNaoRepetidos = new int[arrayNumeros.length];

        for (int i = 0; i < arrayNumeros.length; i++) {
            if (i == arrayNumeros.length-1) {
                arrayNumerosNaoRepetidos[quantidadeNaoRepetidos] = arrayNumeros[i];
                quantidadeNaoRepetidos++;
            } else {
                if ((arrayNumeros[i] != arrayNumeros[i + 1])) {
                    arrayNumerosNaoRepetidos[quantidadeNaoRepetidos] = arrayNumeros[i];
                    quantidadeNaoRepetidos++;
                }
            }
        }

        return ExercicioVI.retornaArrayComDeterminadoNumeroDeElementos(arrayNumerosNaoRepetidos, quantidadeNaoRepetidos);
    }

    public static int[] arrayInvertido(int[] arrayNumeros) {

        int[] arrayInvertido = new int[arrayNumeros.length];

        if (arrayNumeros.length == 0) {
            return arrayNumeros;
        }

        arrayInvertido[0] = arrayNumeros[arrayNumeros.length - 1];
        arrayInvertido[arrayInvertido.length - 1] = arrayNumeros[0];

        for (int i = 1; i < arrayNumeros.length - 1; i++) {
            arrayInvertido[i] = arrayNumeros[arrayInvertido.length - i - 1];
        }

        return arrayInvertido;
    }

    public static boolean ePrimo(int numero) {
        boolean primo = true;

        if (numero == 1) {
            primo = false;
        }

        for (int i = 2; i < numero; i++) {
            if (numero % i == 0) {
                primo = false;
                break;
            }
            primo = true;
        }
        return primo;
    }

    public static int quantidadeNumerosPrimos(int[] arrayNumeros) {
        int contaNumeroPrimos = 0;

        for (int i = 0; i < arrayNumeros.length; i++) {
            if (ePrimo(arrayNumeros[i]))
                contaNumeroPrimos++;
        }
        return contaNumeroPrimos;
    }

    public static int[] obterNumerosPrimos(int[] arrayNumeros) {

        int[] arrayNumeroPrimos = new int[quantidadeNumerosPrimos(arrayNumeros)];
        int indiceNumerosPrimos = 0;

        for (int i = 0; i < arrayNumeros.length; i++) {
            if (ePrimo(arrayNumeros[i])) {
                arrayNumeroPrimos[indiceNumerosPrimos] = arrayNumeros[i];
                indiceNumerosPrimos++;
            }
        }
        return arrayNumeroPrimos;
    }
}
