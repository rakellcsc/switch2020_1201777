package com.company.BlocoIV;

public class ExercicioXVII {

    // Exercicio 17 a)

    /**
     * Este método multiplica uma constante por uma matriz
     * @param matriz Este parâmetro recebe uma matriz
     * @param constante Este parâmetro recebe uma constante
     * @return Retorna uma matriz onde cada elemento resulta da multiplicação da matriz inicial pela constante
     */
    public static int[][] multiplicarMatrizPorConstante(int[][] matriz, int constante) {

        if (matriz == null || matriz.length == 0) {
            return null;
        }

        boolean matrizQuadrada = ExercicioXIII.verificarMatrizQuadrada(matriz);
        boolean matrizRetangular = ExercicioXIV.verificarSeMatrizERetangular(matriz);

        if (matrizQuadrada || matrizRetangular) {

            int[][] novaMatriz = new int[matriz.length][matriz[0].length];

            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    novaMatriz[i][j] = constante * matriz[i][j];
                }
            }
            return novaMatriz;
        } else {
            return null;
        }
    }

    // Exercicio 17 b)

    /**
     * Este método soma duas matrizes
     * @param matriz1 Este parâmetro recebe a primeira matriz
     * @param matriz2 Este parâmetro recebe a segunda matriz
     * @return Retorna uma matriz em que os seus elementos resultam da soma de cada um dos elementos das matrizes dadas
     */
    public static int[][] somaMatrizes(int[][] matriz1, int[][] matriz2) {

        if (matriz1 == null || matriz1.length == 0 || matriz2 == null || matriz2.length == 0) {
            return null;
        }

        boolean matriz1Quadrada = ExercicioXIII.verificarMatrizQuadrada(matriz1);
        boolean matriz2Quadrada = ExercicioXIII.verificarMatrizQuadrada(matriz2);
        boolean matriz1Retangular = ExercicioXIV.verificarSeMatrizERetangular(matriz1);
        boolean matriz2Retangular = ExercicioXIV.verificarSeMatrizERetangular(matriz2);

        int numeroLinhasMatriz1 = matriz1.length;
        int numeroColunasMatriz1 = matriz1[0].length;
        int numeroLinhasMatriz2 = matriz2.length;
        int numeroColunasMatriz2 = matriz2[0].length;

        int[][] matrizSoma = new int[numeroLinhasMatriz1][numeroColunasMatriz1];

        if (((matriz1Quadrada && matriz2Quadrada) || (matriz1Retangular && matriz2Retangular)) && numeroLinhasMatriz1 == numeroLinhasMatriz2 && numeroColunasMatriz1 == numeroColunasMatriz2) {

            for (int i = 0; i < numeroLinhasMatriz1; i++) {
                for (int j = 0; j < numeroColunasMatriz1; j++) {
                    matrizSoma[i][j] = matriz1[i][j] + matriz2[i][j];
                }
            }
        } else {
            return null;
        }
        return matrizSoma;
    }

    // Exercicio 17 c)

    /**
     * Este método multiplica duas matrizes
     * @param matriz1 Este parâmetro recebe a primeira matriz
     * @param matriz2 Este parâmetro recebe a segunda matriz
     * @return Retorna a matriz resultante da multiplicação das duas matrizes dadas
     */
    public static double[][] multiplicaMatrizes(int[][] matriz1, int[][] matriz2) {

        if (matriz1 == null || matriz1.length == 0 || matriz2 == null || matriz2.length == 0) {
            return null;
        }

        boolean matriz1Quadrada = ExercicioXIII.verificarMatrizQuadrada(matriz1);
        boolean matriz2Quadrada = ExercicioXIII.verificarMatrizQuadrada(matriz2);
        boolean matriz1Retangular = ExercicioXIV.verificarSeMatrizERetangular(matriz1);
        boolean matriz2Retangular = ExercicioXIV.verificarSeMatrizERetangular(matriz2);

        int numeroLinhasMatriz1 = matriz1.length;
        int numeroLinhasMatriz2 = matriz2.length;
        int numeroColunasMatriz2 = matriz2[0].length;

        double[][] matrizMultiplicacao = new double[numeroLinhasMatriz1][numeroColunasMatriz2];

        if (((matriz1Quadrada && matriz2Quadrada) || (matriz1Retangular && matriz2Retangular)) && numeroLinhasMatriz1 == numeroColunasMatriz2) {

            for (int i = 0; i < numeroLinhasMatriz1; i++) {
                for (int j = 0; j < numeroColunasMatriz2; j++) {
                    for (int k = 0; k < numeroLinhasMatriz2; k++) {
                        matrizMultiplicacao[i][j] += matriz1[i][k] * matriz2[k][j];
                    }
                }
            }
        } else {
            return null;
        }
        return matrizMultiplicacao;
    }
}
