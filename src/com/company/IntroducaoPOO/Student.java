package com.company.IntroducaoPOO;

public class Student {

    //atributos
    private String name;
    private int number;
    private int grade;


    //construtor
    public Student(String name, int number) {

        if (!this.isStudentNumberValid(number)) {
            throw new IllegalArgumentException("Student number needs to be a 7 digits number");
        }

        if (!this.isStudentNameValid(name)) {
            throw new IllegalArgumentException("Student name cannot be shortter than 5 char");
        }

        this.number = number;

        setName(name);

        this.grade = -1;
    }

    //métodos Getters e Setters
    public void setName(String name) {
        if (isStudentNameValid(name)) {
            this.name = name;
        }
    }

    public int getNumber(){
        return this.number;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        if (this.isStudentGradeValid(grade)) {
            this.grade = grade;
        }
    }

    //métodos de negócio
    private boolean isStudentNumberValid(int number) {

        if (number < 1000000 || number > 9999999) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isStudentNameValid(String name) {
        if (name == null) {
            return false;
        }
        if (name.length() < 5) {
            return false;
        } else {
            return true;
        }
    }

    public boolean doEvaluation(int grade) {
        if (this.isStudentGradeValid(grade) && !this.isEvaluated()) {
            this.grade = grade;
            return true;
        }
        return false;
    }

    private boolean isStudentGradeValid(int grade) {
        if (grade < 0 || grade > 20) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isEvaluated() {
        return grade >= 0;
    }

    public int compareToByNumber(Student other){
        if(this.number < other.number)
            return -1;
        if(this.number > other.number)
            return 1;
        return 0;
    }

    public int compareToByGrade(Student other){
        if(this.grade < other.grade)
            return -1;
        if(this.grade > other.grade)
            return 1;
        return 0;

    }

    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        Student student = (Student) o;
        return this.number == student.number;
    }



    /*public static boolean sortStudentByNumberAsc(Student[] student) {
        if (student == null) {
            return false;
        }

        Student tempStudent = null;

        for (int index1 = 0; index1 < student.length; index1++) {
            for (int index2 = index1 + 1; index2 < student.length; index2++) {
                if (student[index1].compareToByNumber(student[index2])>0) {
                    tempStudent = student[index1];
                    student[index1] = student[index2];
                    student[index2] = tempStudent;
                }
            }
        }
        return true;
    }

    public static boolean sortStudentByGradeDesc(Student[] student) {
        if (student == null) {
            return false;
        }

        Student tempStudent = null;

        for (int index1 = 0; index1 < student.length; index1++) {
            for (int index2 = index1 + 1; index2 < student.length; index2++) {
                if (student[index1].compareToByGrade(student[index2])<0) {
                    tempStudent = student[index1];
                    student[index1] = student[index2];
                    student[index2] = tempStudent;
                }
            }
        }
        return true;
    }
     */
}
