package com.company.IntroducaoPOO;

import java.util.ArrayList;

import java.io.File;
import java.io.IOException;

import java.lang.*;

import java.util.Arrays;

public class StudentList {

    //atributos
    private Student[] students;

    //construtor
    public StudentList() {
        this.students = new Student[0];
    }

    public StudentList(Student[] students) {
        if (students == null)
            throw new IllegalArgumentException("Students array should not be null");
        this.students = copyStudentFromArray(students, students.length);
    }

    //Getters e Setters
    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    //métodos de negócio
    public void sortStudentByNumberAsc() {

        Student tempStudent = null;

        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < students.length; index2++) {
                if (this.students[index1].compareToByNumber(this.students[index2]) > 0) {
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }

    public void sortStudentByGradeDesc() {

        Student tempStudent = null;

        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.students.length; index2++) {
                if (this.students[index1].compareToByGrade(this.students[index2]) < 0) {
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }

    public Student[] toArray()
    {
        return this.copyStudentFromArray(this.students, this.students.length);
    }

    private Student[] copyStudentFromArray(Student[] students, int size) {
        Student[] copyArray = new Student[size];
        for (int count = 0; (count < size) && (count < students.length); count++) {
            copyArray[count] = students[count];
        }
        return copyArray;
    }

    public boolean addStudent(Student student) {
        if (student == null)
            return false;
        for (int i = 0; i < students.length; i++) {
            if(student.equals(students[i])){
                return false;
            }
        }
        this.students = copyStudentFromArray(this.students, this.students.length + 1);
        this.students[this.students.length - 1] = student;
        return true;
    }

   /* public boolean remove(Student student) {
        if (student == null)
            return false;
        int idx = this.getIndexOf(student);
        if (idx < 0)
            return false;
        Student[] leftSide = this.copyStudentsFromArray(this.students,idx);
        Student[] rightSide = this.copyStudentsFromArray(this.students, idx+1, this.students.length-idx-1);
        this.students = join(leftSide, rightSide);
        return true;
    }

    */

    private Student[] copyStudentsFromArray(Student[] students, int size) {
        return this.copyStudentsFromArray(students, 0, size);
    }
    private Student[] copyStudentsFromArray(Student[] students, int start, int size) {
        Student[] copyArray = new Student[size];
        int count = 0;
        for(int idx=start; (count < size) && (idx < students.length); idx++){
            copyArray[idx-start] = students[idx];
            count++;
        }
        return copyArray;
    }
    private Student[] join(Student[] students1, Student[] students2) {
        int size = students1.length+students2.length;
        Student[] copyArray = new Student[size];
        for(int idx=0; (idx < students1.length); idx++){
            copyArray[idx] = students1[idx];
        }
        for(int idx=0; (idx < students2.length); idx++){
            copyArray[idx+students1.length] = students2[idx];
        }
        return copyArray;
    }

    /*private ArrayList<Student> copyStudentFromArray(Student[] students){
        ArrayList<Student> copyArray = new ArrayList<Student>();

        for(Student student: students){
            copyArray.add(student);
        }
        return copyArray;
     }
     */
}
