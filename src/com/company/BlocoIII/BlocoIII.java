package com.company.BlocoIII;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BlocoIII {

    public static void main(String[] args) {


    }

    /*---------------Exercicio 01---------------*/

    public static int fatorialNumero(int numero) {
        int i, resultado;

        if (numero <= 0) {
            return 0;
        } else {
            resultado = 1;

            for (i = numero; i > 1; i--) {
                resultado *= i;
            }
            return resultado;
        }
    }

    /*public static int[] fatorialNumeroVersaoRetornaArray(int numero) {
        int i, j, resultado;
        int[] listaDeNumeros;
        listaDeNumeros = new int[numero];

        resultado = 1;

        for (i = numero; i > 0; i--) {
            for (j = 0; j < i; j++) {
                listaDeNumeros[j] = resultado;
            }
            resultado *= i;
        }

        return listaDeNumeros;
    }*/

    /*---------------Exercicio 02---------------*/

    public static String percentagemPositivasMediaNegativasAlunos(int numeroAlunos, double[] notas) {
        double numeroNotasPositivas = 0, numeroNotasNegativas = 0, somaNotasNegativas = 0, mediaNotasNegativas, percentagemPositivas;
        String mediaNegativasNumeroPositivas;

        if (numeroAlunos <= 0) {
            return "Introduza um número de alunos válido.";
        } else {
            for (int i = 0; i < numeroAlunos; i++) {
                double nota = notas[i];

                if (nota >= 10) {
                    numeroNotasPositivas = numeroNotasPositivas + 1;
                } else if (nota < 10 && nota >= 0) {
                    numeroNotasNegativas = numeroNotasNegativas + 1;
                    somaNotasNegativas = somaNotasNegativas + notas[i];
                } else {
                    return "Dados inválidos.";
                }
            }
        }

        percentagemPositivas = (numeroNotasPositivas / numeroAlunos) * 100;
        mediaNotasNegativas = somaNotasNegativas / numeroNotasNegativas;

        mediaNegativasNumeroPositivas = "Percentagem de positivas: " + String.format("%.2f", percentagemPositivas) + ". Média de negativas: " + String.format("%.2f", mediaNotasNegativas);

        return mediaNegativasNumeroPositivas;
    }

    /*---------------Exercicio 03---------------*/

    public static String percentagemParesMediaImparesNumaLista(double[] listaNumeros) {
        double quantidadePares = 0, quantidadeImpares = 0, totalNumeros = 0, somaImpares = 0;
        double percentagemPares, mediaImpares;


        for (int i = 0; (i < listaNumeros.length) && (listaNumeros[i] > 0); i++) {
            double numero = listaNumeros[i];
            totalNumeros = totalNumeros + 1;

            if (numero % 2 == 0) {
                quantidadePares = quantidadePares + 1;
            } else {
                quantidadeImpares = quantidadeImpares + 1;
                somaImpares = (int) (somaImpares + listaNumeros[i]);
            }
        }

        percentagemPares = (quantidadePares / totalNumeros) * 100;
        mediaImpares = somaImpares / quantidadeImpares;

        return "A percentagem de números pares é " + String.format("%.2f", percentagemPares) + "% e a média dos números ímpares é " + String.format("%.2f", mediaImpares);
    }

    /*---------------Exercicio 04---------------*/

    public static int multiplosDeTresNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeMultiplos = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 3 == 0) {
                quantidadeMultiplos = quantidadeMultiplos + 1;
            }
            numeroLista = numeroLista + 1;
        }
        return quantidadeMultiplos;
    }

    public static int multiplosDeUmNumeroNumIntervalo(int nrADeterminarMultiplo, int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeMultiplos = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % nrADeterminarMultiplo == 0) {
                quantidadeMultiplos = quantidadeMultiplos + 1;
            }
            numeroLista = numeroLista + 1;
        }
        return quantidadeMultiplos;
    }

    public static int multiplosDeTresEDeCincoNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeMultiplosDe3 = 0, quantidadeMultiplosDe5 = 0, quantidadeMultiplos;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 3 == 0) {
                quantidadeMultiplosDe3 = quantidadeMultiplosDe3 + 1;
            } else if (numeroLista % 5 == 0) {
                quantidadeMultiplosDe5 = quantidadeMultiplosDe5 + 1;
            }
            numeroLista = numeroLista + 1;
        }

        quantidadeMultiplos = quantidadeMultiplosDe3 + quantidadeMultiplosDe5;

        return quantidadeMultiplos;
    }

    public static int multiplosDeDoisNumerosNumIntervalo(int numero1, int numero2, int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeMultiplosDeNumero1 = 0, quantidadeMultiplosDeNumero2 = 0, quantidadeMultiplos;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % numero1 == 0) {
                quantidadeMultiplosDeNumero1 = quantidadeMultiplosDeNumero1 + 1;
            } else if (numeroLista % numero2 == 0) {
                quantidadeMultiplosDeNumero2 = quantidadeMultiplosDeNumero2 + 1;
            }
            numeroLista = numeroLista + 1;
        }

        quantidadeMultiplos = quantidadeMultiplosDeNumero1 + quantidadeMultiplosDeNumero2;

        return quantidadeMultiplos;
    }

    public static int somaDosMultiplosDeDoisNumerosNumIntervalo(int numero1, int numero2, int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo;
        int somaMultiplosNumero1 = 0;
        int somaMultiplosNumero2 = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % numero1 == 0) {
                somaMultiplosNumero1 = somaMultiplosNumero1 + numeroLista;

            } else if (numeroLista % numero2 == 0) {
                somaMultiplosNumero2 = somaMultiplosNumero2 + numeroLista;
            }
            numeroLista = numeroLista + 1;
        }
        return somaMultiplosNumero1 + somaMultiplosNumero2;
    }

    /*---------------Exercicio 05---------------*/

    public static int somaNumerosParesNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo;
        int somaNumerosPares = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 2 == 0) {
                somaNumerosPares = somaNumerosPares + numeroLista;
            }
            numeroLista = numeroLista + 1;
        }
        return somaNumerosPares;

    }

    public static int quantidadeNumerosParesNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeNumerosPares = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 2 == 0) {
                quantidadeNumerosPares = quantidadeNumerosPares + 1;
            }
            numeroLista = numeroLista + 1;
        }
        return quantidadeNumerosPares;
    }

    public static int somaNumerosImparesNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo;
        int somaNumerosImpares = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 2 == 1) {
                somaNumerosImpares = somaNumerosImpares + numeroLista;
            }
            numeroLista = numeroLista + 1;
        }

        return somaNumerosImpares;
    }

    public static int quantidadeNumerosImparesNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numeroLista = minimoIntervalo, quantidadeNumerosImpares = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
            if (numeroLista % 2 == 1) {
                quantidadeNumerosImpares = quantidadeNumerosImpares + 1;
            }
            numeroLista = numeroLista + 1;
        }
        return quantidadeNumerosImpares;
    }

    public static int somaMultiplosDeUmNumeroNumIntervalo(int nrADeterminarMultiplo, int minimoIntervalo, int maximoIntervalo) {
        int somaMultiplos = 0;
        int numeroLista;

        if (maximoIntervalo < minimoIntervalo) {
            numeroLista = maximoIntervalo;
            for (int i = 0; i < (minimoIntervalo - maximoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    somaMultiplos = somaMultiplos + numeroLista;
                }
                numeroLista = numeroLista + 1;
            }
            return somaMultiplos;
        } else {
            numeroLista = minimoIntervalo;
            for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    somaMultiplos = somaMultiplos + numeroLista;
                }
                numeroLista = numeroLista + 1;
            }
            return somaMultiplos;
        }
    }

    public static int produtoMultiplosDeUmNumeroNumIntervalo(int nrADeterminarMultiplo, int minimoIntervalo, int maximoIntervalo) {
        int produtoMultiplos = 1;
        int numeroLista;

        if (maximoIntervalo < minimoIntervalo) {
            numeroLista = maximoIntervalo;
            for (int i = 0; i < (minimoIntervalo - maximoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    produtoMultiplos = produtoMultiplos * numeroLista;
                }
                numeroLista = numeroLista + 1;
            }
            return produtoMultiplos;
        } else {
            numeroLista = minimoIntervalo;
            for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    produtoMultiplos = produtoMultiplos * numeroLista;
                }
                numeroLista = numeroLista + 1;
            }
            return produtoMultiplos;
        }
    }

    public static double mediaMultiplosDeUmNumeroNumIntervalo(double nrADeterminarMultiplo, double minimoIntervalo, double maximoIntervalo) {
        double somaMultiplos = 0, quantidadeMultiplos = 0;
        double numeroLista;
        double mediaMultiplos;

        if (maximoIntervalo < minimoIntervalo) {
            numeroLista = maximoIntervalo;
            for (int i = 0; i < (minimoIntervalo - maximoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    somaMultiplos = somaMultiplos + numeroLista;
                    quantidadeMultiplos = quantidadeMultiplos + 1;
                }
                numeroLista = numeroLista + 1;
            }
            mediaMultiplos = somaMultiplos / quantidadeMultiplos;
            return mediaMultiplos;
        } else {
            numeroLista = minimoIntervalo;
            for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
                if (numeroLista % nrADeterminarMultiplo == 0) {
                    somaMultiplos = somaMultiplos + numeroLista;
                    quantidadeMultiplos = quantidadeMultiplos + 1;
                }
                numeroLista = numeroLista + 1;
            }
            mediaMultiplos = somaMultiplos / quantidadeMultiplos;
            return mediaMultiplos;
        }
    }

    public static double mediaDosMultiplosDeDoisNumerosNumIntervalo(double numero1, double numero2, double minimoIntervalo, double maximoIntervalo) {
        double numeroLista;
        double somaMultiplosNumero1 = 0, somaMultiplosNumero2 = 0, quantidadeMultiplos1 = 0, quantidadeMultiplos2 = 0;
        double mediaMultiplos;

        if (maximoIntervalo < minimoIntervalo) {
            numeroLista = maximoIntervalo;
            for (int i = 0; i < (minimoIntervalo - maximoIntervalo) + 1; i++) {
                if (numeroLista % numero1 == 0) {
                    somaMultiplosNumero1 = somaMultiplosNumero1 + numeroLista;
                    quantidadeMultiplos1 = quantidadeMultiplos1 + 1;
                } else if (numeroLista % numero2 == 0) {
                    somaMultiplosNumero2 = somaMultiplosNumero2 + numeroLista;
                    quantidadeMultiplos2 = quantidadeMultiplos2 + 1;
                }
                numeroLista = numeroLista + 1;
            }
            mediaMultiplos = (somaMultiplosNumero1 + somaMultiplosNumero2) / (quantidadeMultiplos1 + quantidadeMultiplos2);
            return mediaMultiplos;
        } else {
            numeroLista = minimoIntervalo;
            for (int i = 0; i < (maximoIntervalo - minimoIntervalo) + 1; i++) {
                if (numeroLista % numero1 == 0) {
                    somaMultiplosNumero1 = somaMultiplosNumero1 + numeroLista;
                    quantidadeMultiplos1 = quantidadeMultiplos1 + 1;
                } else if (numeroLista % numero2 == 0) {
                    somaMultiplosNumero2 = somaMultiplosNumero2 + numeroLista;
                    quantidadeMultiplos2 = quantidadeMultiplos2 + 1;
                }
                numeroLista = numeroLista + 1;
            }
        }
        mediaMultiplos = (somaMultiplosNumero1 + somaMultiplosNumero2) / (quantidadeMultiplos1 + quantidadeMultiplos2);
        return mediaMultiplos;
    }

    /*---------------Exercicio 06---------------*/

    public static int quantidadeAlgarismos(long numero) {
        int qtdAlgarismos;
        String numeroString;

        if (numero >= 0) {
            numeroString = Long.toString(numero);
            qtdAlgarismos = numeroString.length();
        } else {
            numeroString = Long.toString(numero);
            qtdAlgarismos = numeroString.length() - 1;
        }
        return qtdAlgarismos;
    }

    public static boolean numeroPar(int numero) {
        boolean par = false;
        if (numero < 0) {
            numero = -numero;
        }

        if (numero % 2 == 0) {
            return true;
        }
        return par;
    }

    public static boolean numeroImpar(int numero) {
        boolean impar = false;

        if (numero < 0) {
            numero = -numero;
        }

        if (numero % 2 == 1) {
            return true;
        }
        return impar;
    }

    public static int qtsAlgarismosParDeUmNumero(long numero) {
        int contagemPares = 0;
        int digito;

        if (numero < 0) {
            numero = -numero;
        }

        while (numero != 0) {
            digito = (int) (numero % 10);
            if (numeroPar(digito)) {
                contagemPares++;
            }
            numero = numero / 10;
        }
        return contagemPares;
    }

    public static int qtsAlgarismosImparesDeUmNumero(long numero) {
        int contagemImpares = 0;
        int digito;

        if (numero < 0) {
            numero = -numero;
        }

        while (numero != 0) {
            digito = (int) (numero % 10);
            if (numeroImpar(digito)) {
                contagemImpares++;
            }
            numero = numero / 10;
        }

        return contagemImpares;
    }

    public static double somaAlgarismosDeUmNumero(long numero) {
        double somaDigitos = 0;
        int digito;

        if (numero < 0) {
            numero = -numero;
        }

        while (numero > 0) {
            digito = (int) (numero % 10);
            somaDigitos = somaDigitos + digito;
            numero = numero / 10;
        }
        return somaDigitos;
    }

    public static double somaAlgarismosParesDeUmNumero(long numero) {
        double somaPares = 0;
        int digito;

        if (numero < 0) {
            numero = -numero;
        }

        while (numero != 0) {
            digito = (int) (numero % 10);
            if (numeroPar(digito)) {
                somaPares = somaPares + digito;
            }
            numero = numero / 10;
        }

        return somaPares;
    }

    public static double somaAlgarismosImparesDeUmNumero(long numero) {
        double somaImpares = 0;
        int digito;

        if (numero < 0) {
            numero = -numero;
        }

        while (numero != 0) {
            digito = (int) (numero % 10);
            if (numeroImpar(digito)) {
                somaImpares = somaImpares + digito;
            }
            numero = numero / 10;
        }

        return somaImpares;
    }

    public static int comprimentoDeUmNumero(long numero) {
        int comprimento = 0;

        while (numero != 0) {
            numero = numero / 10;
            comprimento = comprimento + 1;
        }
        return comprimento;
    }

    public static double arredondarNumeros(double numero, int casasDecimais) {
        double resultado;

        resultado = Math.round(numero * Math.pow(10, casasDecimais)) / (Math.pow(10, casasDecimais));

        return resultado;
    }


    public static double mediaAlgarismosDeUmNumero(long numero) {
        double media;

        media = arredondarNumeros(somaAlgarismosDeUmNumero(numero) / comprimentoDeUmNumero(numero), 2);

        return media;
    }

    public static double mediaAlgarismosParesDeUmNumero(long numero) {
        double mediaNumerosPares;

        mediaNumerosPares = arredondarNumeros(somaAlgarismosParesDeUmNumero(numero) / qtsAlgarismosParDeUmNumero(numero), 2);

        return mediaNumerosPares;
    }

    public static double mediaAlgarismosImparesDeUmNumero(long numero) {
        double mediaNumerosImpares;

        mediaNumerosImpares = arredondarNumeros(somaAlgarismosImparesDeUmNumero(numero) / qtsAlgarismosImparesDeUmNumero(numero), 2);

        return mediaNumerosImpares;
    }

    public static int ordemInversaNumero(long numero) {
        int digito, inverso = 0;

        while (numero != 0) {
            digito = (int) (numero % 10);
            inverso = inverso * 10 + digito;
            numero = numero / 10;
        }
        return inverso;
    }

    /*---------------Exercicio 07---------------*/

    public static boolean numeroCapicua(int numero) {
        boolean capicua = false;

        if (numero < 10) {
            return capicua;
        }

        if (numero == ordemInversaNumero(numero)) {
            capicua = true;
        }
        return capicua;
    }

    public static int primeiroCapicuaNumIntervalo(int minimoIntervalo, int maximoIntervalo) {

        for (int i = minimoIntervalo; i <= maximoIntervalo; i++) {

            if (numeroCapicua(i)) {
                return i;
            }
        }
        return 0;
    }

    public static int maiorCapicuaNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numero = minimoIntervalo, primeiroCapicua = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) - 1; i++) {

            if (numeroCapicua(numero)) {
                primeiroCapicua = numero;
            }
            numero = numero + 1;
        }
        return primeiroCapicua;
    }

    public static int qtsCapicuasNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numero = minimoIntervalo, quantidadeCapicua = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) - 1; i++) {

            if (numeroCapicua(numero)) {
                quantidadeCapicua += 1;
            }
            numero = numero + 1;
        }
        return quantidadeCapicua;
    }

    public static boolean numeroAmstrong(int numero) {
        int digito, somaCubos = 0, numeroComparar = numero;

        while (numero != 0) {
            digito = numero % 10;
            somaCubos = (int) (somaCubos + Math.pow(digito, 3));
            numero = numero / 10;
        }

        if (numeroComparar == somaCubos) {
            return true;
        }
        return false;
    }

    public static int primeiroNrAmstrongNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numero = minimoIntervalo, primeiroAmstrong = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) - 1; i++) {

            if (numeroAmstrong(numero)) {
                primeiroAmstrong = numero;
                break;
            }
            numero = numero + 1;
        }
        return primeiroAmstrong;
    }

    public static int qtsAmstrongNumIntervalo(int minimoIntervalo, int maximoIntervalo) {
        int numero = minimoIntervalo, quantidadeAmstrong = 0;

        for (int i = 0; i < (maximoIntervalo - minimoIntervalo) - 1; i++) {

            if (numeroAmstrong(numero)) {
                quantidadeAmstrong += 1;
            }
            numero = numero + 1;
        }
        return quantidadeAmstrong;
    }

    /*---------------Exercicio 08---------------*/

    public static void somaNumerosDeUmaLista() {

        int numero = 0, soma, menor, numero1, numero2, somaTotal;
        Scanner scan = new Scanner(System.in);

        System.out.print("Introduza um valor inteiro: ");
        numero1 = scan.nextInt();
        System.out.print("Introduza um valor inteiro: ");
        numero2 = scan.nextInt();

        soma = numero1 + numero2;

        if (numero1 < numero2) {
            menor = numero1;
        } else {
            menor = numero2;
        }

        while (numero <= soma) {
            soma += numero;
            System.out.print("Introduza um valor inteiro: ");
            numero = scan.nextInt();

            if (numero < menor) {
                menor = numero;
            }
        }

        somaTotal = soma + numero;

        System.out.println("A soma total dos números introduzidos foi " + somaTotal + " e o menor desses números foi " + menor);
    }

    /*---------------Exercicio 09---------------*/

    public static void salariosFuncionarios() {
        double salarioBase, horasExtra, salarioTotal, somaSalarios = 0, mediaSalarios, numeroTrabalhadores = 0;

        Scanner scan = new Scanner(System.in);

        do {
            System.out.println("Introduza o número de horas extra. Para sair introduza -1.");
            horasExtra = scan.nextInt();

            if (horasExtra < -1) {
                System.out.println("Dado inválido.");
            } else if (horasExtra >= 0) {
                System.out.println("Introduza o valor do seu salário: ");
                salarioBase = scan.nextInt();
                salarioTotal = salarioBase + (0.02 * horasExtra * salarioBase);
                System.out.println("O seu salário é de " + salarioTotal);

                somaSalarios = somaSalarios + salarioTotal;
                numeroTrabalhadores = numeroTrabalhadores + 1;
            }
        } while (horasExtra != -1);

        mediaSalarios = somaSalarios / numeroTrabalhadores;

        System.out.println("A média dos salários introduzidos é de " + mediaSalarios);
    }

    /*---------------Exercicio 10---------------*/

    public static void produtoNumerosDeUmaLista() {

        int numero = 1, produto, maior, numero1, numero2, produtoTotal;
        Scanner scan = new Scanner(System.in);

        System.out.print("Introduza um valor inteiro: ");
        numero1 = scan.nextInt();
        System.out.print("Introduza um valor inteiro: ");
        numero2 = scan.nextInt();

        produto = numero1 * numero2;

        if (numero1 > numero2) {
            maior = numero1;
        } else {
            maior = numero2;
        }

        while (numero <= produto) {
            produto *= numero;
            System.out.print("Introduza um valor inteiro: ");
            numero = scan.nextInt();

            if (numero > maior) {
                maior = numero;
            }
        }

        produtoTotal = produto * numero;

        System.out.println("A produto total dos números introduzidos foi " + produtoTotal + " e o maior desses números foi " + maior);
    }

    /*---------------Exercicio 11---------------*/

    public static String maneirasPossiveisObterX(int x) {
        int primeiroNumero = 0, segundoNumero = 0, LIMITE_SUPERIOR = 10, somaNumeros = 0;
        ArrayList<String> combinacoesPossiveis = new ArrayList<String>();
        int posicaoActualArray = 0;

        if (x < 0 || x > 20) {
            return "Erro. Insira um numero inteiro entre 0 e 20";
        }

        while (primeiroNumero <= LIMITE_SUPERIOR) {

            for (int i = 0; i <= LIMITE_SUPERIOR && somaNumeros <= x && segundoNumero <= LIMITE_SUPERIOR; i++) {
                somaNumeros = primeiroNumero + segundoNumero;
                if (somaNumeros == x) {
                    combinacoesPossiveis.add(posicaoActualArray, primeiroNumero + "+" + segundoNumero);
                    posicaoActualArray++;
                }
                segundoNumero++;
            }

            primeiroNumero++;

            segundoNumero = primeiroNumero;
            somaNumeros = 0;
        }

        String resultadoFinal = "As combinaçoes possiveis sao: ";

        for (int i = 0; i < combinacoesPossiveis.size(); i++) {
            resultadoFinal += combinacoesPossiveis.get(i) + " ";
        }

        System.out.println(resultadoFinal);
        return resultadoFinal;
    }

    /*---------------Exercicio 12---------------*/

    public static void exercicio12cicloFormulaResolvente() {
        String validacao = "";
        double a, b, c;

        Scanner scan = new Scanner(System.in);

        while (!validacao.equals("N")) {
            System.out.print("Valor de a: ");
            a = scan.nextDouble();
            System.out.print("Valor de b: ");
            b = scan.nextDouble();
            System.out.print("Valor de c: ");
            c = scan.nextDouble();

            System.out.println(formulaResolvente(a, b, c));

            System.out.println("Deseja continuar a resolver equações de 2º grau? Sim - S ou Não - N");
            validacao = scan.next();
        }
    }

    public static String formulaResolvente(double a, double b, double c) {
        double delta;

        double formulaResolvente1;
        double formulaResolvente2;

        delta = Math.pow(b, 2) - 4 * a * c;

        if (a == 0) {
            return "A equação não é de segundo grau.";
        } else {
            formulaResolvente1 = ((-b) + Math.sqrt(delta)) / (2 * a);
            formulaResolvente2 = ((-b) - Math.sqrt(delta)) / (2 * a);

            if (delta < 0) {
                return "A equação tem duas raízes imaginárias.";
            } else if (delta == 0) {
                return "A equação tem raíz dupla: " + formulaResolvente1;
            } else {
                return "A equação tem duas raízes reais: " + arredondarNumeros(formulaResolvente1, 2) + " e " + arredondarNumeros(formulaResolvente2, 2);
            }
        }
    }

    /*---------------Exercicio 13---------------*/

    public static void main_exercicio13() {

        Scanner ler = new Scanner(System.in);

        int codigoLido = 0;
        String classificacao = "";

        System.out.println("Indique o código do produto (de 1  a 15). Insira 0 para sair.");

        do {
            System.out.print("Insira o código: ");
            codigoLido = ler.nextInt();

            classificacao = verificaProduto(codigoLido);
            System.out.println(classificacao + "\n");
        } while (codigoLido != 0);

    }

    public static String verificaProduto(int codigoLido) {
        if (codigoLido < 0 || codigoLido > 15) {
            return "Código inválido";
        } else if (codigoLido == 1) {
            return "Classificação do produto: Alimento não perecível";
        } else if (codigoLido >= 2 && codigoLido <= 4) {
            return "Classificação do produto: Alimento perecível";
        } else if (codigoLido >= 5 && codigoLido <= 6) {
            return "Classificação do produto: Vestuário";
        } else if (codigoLido == 7) {
            return "Classificação do produto: Higiene pessoal";
        } else if (codigoLido >= 8 && codigoLido <= 15) {
            return "Classificação do produto: Limpeza e utensílios domésticos";
        } else
            return "Sair.";
    }

    /*---------------Exercicio 14---------------*/

    public static void exercicio14CambioMoeda() {
        int valor = 0;
        double quantia;
        String moeda;

        Scanner scan = new Scanner(System.in);

        while (valor >= 0) {
            System.out.println("Indique a quantia que pretende efetuar o câmbio.");
            quantia = scan.nextDouble();
            System.out.println("Introduza a moeda (D - Dólar; L - Libra; I - Iene; C - Coroa Sueca; F - Franco Suíço)");
            moeda = scan.next();

            System.out.println(cambioMoeda(quantia, moeda));

            System.out.println("Pretende efetuar novo câmbio? Se sim, introduza um número positivo. Se não, introduza um valor negativo.");
            valor = scan.nextByte();
        }
    }
    public static String cambioMoeda(double quantia, String moeda) {
        double cambio = 0;

        switch (moeda) {
            case "D":
                cambio = 1.534 * quantia;
                break;
            case "L":
                cambio = 0.774 * quantia;
                break;
            case "I":
                cambio = 161.480 * quantia;
                break;
            case "C":
                cambio = 9.593 * quantia;
                break;
            case "F":
                cambio = 1.601 * quantia;
                break;
        }
        return quantia + " euros " + " = " + arredondarNumeros(cambio, 2) + " " + moeda;
    }

    public static double calculoSalarioBruto(double salarioLiquido) {
        double salarioBruto = 0;

        if (salarioLiquido <= 0) {
            return 0;
        }

        if (salarioLiquido > 0 && salarioLiquido <= 500) {
            salarioBruto = 0.9 * salarioLiquido;
        } else if (salarioLiquido <= 1000) {
            salarioBruto = 0.85 * salarioLiquido;
        } else if (salarioLiquido > 1000) {
            salarioBruto = 0.8 * salarioLiquido;
        }
        return salarioBruto;
    }

    /*---------------Exercicio 15---------------*/

    public static void main_exercicio15() {

        Scanner ler = new Scanner(System.in);

        int notaQuantitativa = 0;
        String notaQualitativa = "";

        System.out.println("Indique a nota do aluno (de 0 a 20). Insira um número negativo para sair.");

        do {
            System.out.print("Insira a nota: ");
            notaQuantitativa = ler.nextInt();

            notaQualitativa = verificaNotaQualitativa(notaQuantitativa);
            System.out.println(notaQualitativa + "\n");
        } while (notaQuantitativa >= 0);
    }

    public static String verificaNotaQualitativa(int notaQuantitativa) {
        if (notaQuantitativa >= 0 && notaQuantitativa <= 4) {
            return "Mau";
        } else if (notaQuantitativa >= 5 && notaQuantitativa <= 9) {
            return "Medíocre";
        } else if (notaQuantitativa >= 10 && notaQuantitativa <= 13) {
            return "Suficiente";
        } else if (notaQuantitativa >= 14 && notaQuantitativa <= 17) {
            return "Bom";
        } else if (notaQuantitativa >= 18 && notaQuantitativa <= 20) {
            return "Muito bom";
        } else if (notaQuantitativa > 20) {
            return "Nota inválida";
        } else
            return "Sair.";
    }

    /*---------------Exercicio 16---------------*/

    public static double calculoSalarioLiquido(double salarioBruto) {
        double salarioLiquido = 0;

        if (salarioBruto <= 0) {
            return 0;
        }

        if (salarioBruto > 0 && salarioBruto <= 500) {
            salarioLiquido = 0.9 * salarioBruto;
        } else if (salarioBruto <= 1000) {
            salarioLiquido = 0.85 * salarioBruto;
        } else if (salarioBruto > 1000) {
            salarioLiquido = 0.8 * salarioBruto;
        }
        return salarioLiquido;
    }

    /*---------------Exercicio 17---------------*/

    public static void main_exercicio17() {

        Scanner ler = new Scanner(System.in);

        double pesoCanino = 0;
        String racaCanino;
        int pesoDadoRacaoCanino = 0;
        boolean racaoAdequada = false;

        do {
            System.out.println("Indique o peso do seu canino (em kg).");

            System.out.print("Insira o peso do canino (kg): ");
            pesoCanino = ler.nextDouble();
            racaCanino = verificaRacaCanino(pesoCanino);

            if (racaCanino != "") {
                System.out.println("Indique o peso da ração que dá ao seu canino (em gramas).");

                System.out.print("Insira o peso da ração (gramas): ");
                pesoDadoRacaoCanino = ler.nextInt();
                if (pesoDadoRacaoCanino > 0) {
                    racaoAdequada = verificaPesoRacaoAdequada(racaCanino, pesoDadoRacaoCanino);
                    if (racaoAdequada) {
                        System.out.println("A quantidade de comida que dá ao seu canino é adequada.");
                    } else {
                        System.out.println("A quantidade de comida que dá ao seu canino NÃO é adequada.");
                    }
                } else {
                    System.out.println("O peso da ração do seu canino não pode ser negativo nem nulo.");
                }
            } else {
                System.out.println("Sair.");
            }
        } while (pesoCanino <= 0);

    }

    public static String verificaRacaCanino(double pesoCanino) {
        if (pesoCanino > 0.0 && pesoCanino <= 10.0) {
            return "Pequena";
        } else if (pesoCanino > 10.0 && pesoCanino <= 25.0) {
            return "Media";
        } else if (pesoCanino > 25.0 && pesoCanino <= 45.0) {
            return "Grande";
        } else if (pesoCanino > 45.0) {
            return "Gigante";
        }
        return "";
    }

    public static boolean verificaPesoRacaoAdequada(String racaCanino, int pesoDadoRacaoCanino) {
        if (racaCanino.equals("Pequena") && pesoDadoRacaoCanino == 100) {
            return true;
        } else if (racaCanino.equals("Media") && pesoDadoRacaoCanino == 250) {
            return true;
        } else if (racaCanino.equals("Grande") && pesoDadoRacaoCanino == 300) {
            return true;
        } else if (racaCanino.equals("Gigante") && pesoDadoRacaoCanino == 500) {
            return true;
        }
        return false;
    }

    /*---------------Exercicio 18---------------*/

    public static boolean exercicio18VerificarCC(int numeroCC) {
        int soma = 0;
        boolean valido = false;

        for (int i = 1; i <= 9; i++) {
            soma = soma + ((numeroCC % 10) * i);
            numeroCC = numeroCC / 10;
        }

        if (soma % 11 == 0) {
            valido = true;
        }

        return valido;
    }

    /*---------------Exercicio 19---------------*/

    public static void main_exercicio19() {
        Scanner ler = new Scanner(System.in);

        int sequenciaNumeros = 0;
        int numerosOrganizados = 0;

        System.out.println("Indique a sequencia de numeros positivos inteiros que quer organizar.");

        System.out.print("Insira a sequencia de numeros: ");
        sequenciaNumeros = ler.nextInt();

        numerosOrganizados = organizaNumeros(sequenciaNumeros);
        System.out.println("Resultado dos numeros organizados com pares à direita e impares à esquerda: " + numerosOrganizados);
    }

    public static int organizaNumeros(int sequenciaNumeros) {
        int numerosOrganizados = 0, algarismo;
        String conversaoSequenciaNumeros = String.valueOf(sequenciaNumeros);
        String numerosPares = "";
        String numerosImpares = "";
        String concatenacao = "";

        for (int i = 0; i < conversaoSequenciaNumeros.length(); i++) {
            algarismo = sequenciaNumeros % 10;
            sequenciaNumeros /= 10;
            Math.round(sequenciaNumeros);

            if (algarismo % 2 == 0) {
                numerosPares += String.valueOf(algarismo);
            } else {
                numerosImpares += String.valueOf(algarismo);
            }
        }

        //organiza os numeros impares
        char[] auxiliar = numerosImpares.toCharArray();
        Arrays.sort(auxiliar);
        numerosImpares = new String(auxiliar);

        //organiza os numeros pares
        auxiliar = numerosPares.toCharArray();
        Arrays.sort(auxiliar);
        numerosPares = new String(auxiliar);

        concatenacao = numerosImpares + numerosPares;

        numerosOrganizados = Integer.parseInt(concatenacao);
        System.out.println(numerosOrganizados);
        return numerosOrganizados;
    }

    /*---------------Exercicio 20---------------*/

    public static String classificarNumeros(int numero) {

        int divisor, somaDivisores = 0;

        for (int i = 2; i <= numero; i++) {
            if (numero % i == 0) {
                divisor = numero / i;
                somaDivisores += divisor;
            }
        }
        return classificarNumeros(numero, somaDivisores);
    }

    public static String classificarNumeros(int numero, int somaDivisores) {

        if (numero == somaDivisores) {
            return "O número é perfeito.";
        } else if (numero < somaDivisores) {
            return "O número é abundante.";
        } else if (numero > somaDivisores) {
            return "O número é reduzido.";
        } else {
            return "";
        }
    }
}





