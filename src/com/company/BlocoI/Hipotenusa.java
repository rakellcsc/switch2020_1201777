package com.company.BlocoI;

public class Hipotenusa {

    private double cateto1 = -1;
    private double cateto2 = -1;

    public Hipotenusa(double cateto1, double cateto2) {

        if (!this.verificarValidadeCateto(cateto1) || !this.verificarValidadeCateto(cateto2)) {
            throw new IllegalArgumentException("Os catetos só podem tomar valores positivos");
        }
            this.cateto1 = cateto1;
            this.cateto2 = cateto2;
    }

    public double calcularHipotenusa() {

        return this.arredondarNumeros(Math.sqrt((Math.pow(cateto1, 2) + Math.pow(cateto2, 2))), 2);

    }

    private boolean verificarValidadeCateto(double cateto) {
        if (cateto <= 0) {
            return false;
        } else {
            return true;
        }
    }

    private double arredondarNumeros(double numero, int casasDecimais) {

        return Math.round(numero * Math.pow(10, casasDecimais)) / (Math.pow(10, casasDecimais));

    }
}
