package com.company.BlocoI;

public class Altura {

    //atributos
    private double tempo;

    //construtor

    public Altura(double tempo){

        if(!this.isTimeValid(tempo)){
            throw new IllegalArgumentException("Tempo inválido.");
        }

        this.tempo = tempo;
    }

    //métodos negócio
    public double calculaAlturaPredio(){
        return (9.8 * Math.pow(tempo, 2))/2;
    }

    private boolean isTimeValid(double tempo){
        if(tempo < 0){
            return false;
        } else{
            return true;
        }
    }
}
