package com.company.BlocoI;

public class BlocoI {

    public static void main(String[] args) {

        Altura altura = new Altura(3);

        System.out.println(altura.calculaAlturaPredio());

    }

    public static double exercicio03(double raio, double altura) {

        double capacidade;

        capacidade = Math.pow(raio, 2) * altura * Math.PI * 1000;

        return capacidade;
    }

    public static double exercicio04(double tempo) {

        double distancia;
        double velocidadeSom = 1224000;
        double segundos = 3600;

        distancia = velocidadeSom * (tempo / segundos);

        return distancia;
    }

    public static double exercicio05(double tempo) {

        double altura;
        double gravidade = 9.8;

        altura = (gravidade * tempo * tempo) / 2;

        return altura;
    }

    public static double exercicio06(double alturaPessoa, double sombraPessoa, double sombraEdificio) {

        double alturaEdificio;

        alturaEdificio = (alturaPessoa * sombraEdificio) / sombraPessoa;

        return alturaEdificio;
    }

    public static double exercicio07(double velocidade, double horas, double minutos, double segundos) {
        double tempo;
        double distancia;

        tempo = horas + (minutos / 60) + (segundos / 3600);
        distancia = velocidade * tempo;

        return distancia;
    }

    public static double exercicio08(double cabo1, double cabo2, double angulo) {

        double distanciaEntreTrabalhadores;

        distanciaEntreTrabalhadores = Math.sqrt((Math.pow(cabo1, 2)) + (Math.pow(cabo2, 2)) - 2 * cabo1 * cabo2 * Math.cos((angulo * Math.PI) / 180));

        return distanciaEntreTrabalhadores;
    }

    public static double exercicio09(double a, double b) {

        double perimetro;

        perimetro = 2 * a + 2 * b;

        return perimetro;
    }

    public static double exercicio10(double cateto1, double cateto2) {
        double hipotenusa;

        hipotenusa = Math.sqrt((Math.pow(cateto1, 2) + Math.pow(cateto2, 2)));

        return hipotenusa;
    }

    public static double exercicio11_Funcao(double x){
        double y;

        y = Math.pow(x, 2) - (3*x) + 1;

        return y;
    }

    public static double[] exercicio11_FormulaResolvente(double a, double b, double c) {
        double delta;
        double[] resultados;
        resultados = new double[2];

        double formulaResolvente1;
        double formulaResolvente2;

        delta = Math.pow(b, 2) - 4 * a * c;

        if (a == 0) {
            System.out.println("A equação não é de segundo grau.");
        }

        if (delta < 0) {
            System.out.print("A equação é impossível em R (conjunto dos números reais.");
        } else {
            formulaResolvente1 = ((-b) + Math.sqrt(delta)) / (2 * a);
            formulaResolvente2 = ((-b) - Math.sqrt(delta)) / (2 * a);

            resultados[0] = formulaResolvente1;
            resultados[1] = formulaResolvente2;

            System.out.println("Solução 1: " + formulaResolvente1);
            System.out.println("Solução 2: " + formulaResolvente2);
        }
        return resultados;
    }

    public static double exercicio12(double celsius){
        double fahrenheit;

        fahrenheit = 32 + (1.8 * celsius);

        return fahrenheit;
    }

    public static double exercicio13(double horas, double minutos){
        double converterMinutos;

        converterMinutos = (horas * 60) + minutos;

        return converterMinutos;
    }
}



