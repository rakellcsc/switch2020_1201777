package com.company.BlocoII;

import java.util.Scanner;

public class BlocoII {

    public static void main(String[] args) {
        // exercicio01();
        //exercicio02();
        //exercicio05();
        //exercicio11();
    }

    public static void exercicio01() {
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double media;

        Scanner scan = new Scanner(System.in);

        System.out.print("Nota 1: ");
        nota1 = scan.nextInt();

        System.out.print("Nota 2: ");
        nota2 = scan.nextInt();

        System.out.print("Nota 3: ");
        nota3 = scan.nextInt();

        System.out.print("Peso 1: ");
        peso1 = scan.nextInt();

        System.out.print("Peso 2: ");
        peso2 = scan.nextInt();

        System.out.print("Peso 3: ");
        peso3 = scan.nextInt();

        media = calcMedia(nota1, nota2, nota3, peso1, peso2, peso3);

        System.out.println("A média é: " + media);
    }

    public static double calcMedia(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        double mediaPesada;

        mediaPesada = (double) ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);

        if (mediaPesada >= 8) {
            System.out.println("O aluno cumpre os requisitos. APROVADO!");
        } else {
            System.out.println("O aluno não cumpre os requisitos. REPROVADO!");
        }

        return mediaPesada;
    }

    public static void exercicio02() {
        int numero;
        String digito;

        Scanner scan = new Scanner(System.in);

        System.out.println("Escreva um número com três dígitos: ");
        numero = scan.nextInt();

        digito = escreverDigitos(numero);

        System.out.println(digito);

    }


    public static String escreverDigitos(int numero) {
        int digito1, digito2, digito3;

        if (numero < 100 || numero > 999) {
            return "O número " + numero + " não tem três dígitos.";
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if (numero % 2 == 0) {
                return "O número " + numero + " tem os dígitos " + (digito1 + " " + digito2 + " " + digito3) + " e é um número par.";
            } else {
                return "O número " + numero + " tem os dígitos " + (digito1 + " " + digito2 + " " + digito3) + " e é um número ímpar.";
            }
        }
    }

    public static double distanciaEntrePontos(int x1, int y1, int x2, int y2) {
        double distancia;
        int eixoX;
        int eixoY;

        eixoX = (x2 - x1);
        eixoY = (y2 - y1);

        distancia = Math.sqrt((Math.pow(eixoX, 2) + Math.pow(eixoY, 2)));

        return distancia;
    }

    public static double exercicio04(double x) {
        double funcao;

        if (x < 0) {
            funcao = x;
        } else if (x == 0) {
            funcao = 0;
        } else {
            funcao = Math.pow(x, 2) - (2 * x);
        }
        return funcao;
    }

    public static void exercicio05() {
        double area;
        String valorVolume, classifCubo;

        Scanner scan = new Scanner(System.in);

        System.out.println("Qual é o valor da área?");
        area = scan.nextDouble();

        valorVolume = calcVolume(area);

        classifCubo = classificacaoCubo(area);

        System.out.println(valorVolume);
        System.out.println(classifCubo);

    }

    public static String calcVolume(double area) {
        double aresta, volume;

        if (area > 0) {
            aresta = Math.sqrt(area / 6);
            volume = Math.pow(aresta, 3);
            return "O volume do cubo é: " + String.format("%.2f", volume);
        } else {
            return "O valor da área é inválido.";
        }
    }

    public static String classificacaoCubo(double area) {
        double aresta, volume;
        aresta = Math.sqrt(area / 6);
        volume = Math.pow(aresta, 3);

        if (volume <= 1) {
            return "O cubo é pequeno.";
        } else if (volume > 1 && volume <= 2) {
            return "O cubo é médio.";
        } else {
            return "O cubo é grande.";
        }
    }

    public static String conversaoTempo(double segundos) {
        double horas, horasParteDecimal, minutos, minutosParteDecimal, segundosRestantes, segundosRestantesParteDecimal;
        int horasParteInteira, minutosParteInteira, segundosRestantesParteInteira;
        String tempo;

        horas = (segundos / 3600);
        horasParteDecimal = horas % 1;
        horasParteInteira = (int) (horas - horasParteDecimal);
        minutos = horasParteDecimal * 60;
        minutosParteDecimal = minutos % 1;
        minutosParteInteira = (int) (minutos - minutosParteDecimal);
        segundosRestantes = (minutosParteDecimal * 60);
        segundosRestantesParteDecimal = segundosRestantes % 1;
        segundosRestantesParteInteira = (int) (segundosRestantes - segundosRestantesParteDecimal);

        tempo = "[" + horasParteInteira + " : " + minutosParteInteira + " : " + segundosRestantesParteInteira + "]";

        if (segundos < 0 || segundos > 86401) {
            return "Valor inválido. O valor referente a 'segundos' deverá estar num intervalo entre 0 e 86400";
        } else {
            if (segundos >= 21600 && segundos < 43201) {
                return "Bom dia! Hora atual: " + tempo;
            } else if (segundos >= 43201 && segundos < 72001) {
                return "Boa tarde! Hora atual: " + tempo;
            } else {
                return "Boa noite! Hora atual:  " + tempo;
            }
        }
    }

    public static double custoDaTinta(double area, double custoLitroTinta, double rendimento) {
        double custoDaTinta;

        custoDaTinta = Math.ceil((area / rendimento)) * custoLitroTinta;

        return custoDaTinta;
    }

    public static double custoMaoDeObra(double salarioPorDia, double area) {
        double maoDeObra;

        maoDeObra = 0;

        if (area > 0 && area < 100) {
            maoDeObra = 1 * salarioPorDia * (area / 16);
        } else if (area >= 100 && area < 300) {
            maoDeObra = 2 * salarioPorDia * (area / 32);
        } else if (area >= 300 && area < 1000) {
            maoDeObra = 3 * salarioPorDia * (area / 48);
        } else if (area >= 1000) {
            maoDeObra = 4 * salarioPorDia * (area / 64);
        }

        return maoDeObra;
    }

    public static double custoTotal(double salarioPorDia, double area, double custoLitroTinta, double rendimento) {
        double custo, maoDeObra, custoDaTinta;

        custo = 0;

        custoDaTinta = Math.ceil((area / rendimento)) * custoLitroTinta;

        if (area > 0 && area < 100) {
            maoDeObra = 1 * salarioPorDia * (area / 16);
            custo = maoDeObra + custoDaTinta;
            return custo;
        } else if (area < 300) {
            maoDeObra = 2 * salarioPorDia * (area / 32);
            custo = maoDeObra + custoDaTinta;
            return custo;
        } else if (area < 1000) {
            maoDeObra = 3 * salarioPorDia * (area / 48);
            custo = maoDeObra + custoDaTinta;
            return custo;
        } else if (area >= 1000) {
            maoDeObra = 4 * salarioPorDia * (area / 64);
            custo = maoDeObra + custoDaTinta;
            return custo;
        }

        return custo;
    }

    public static String numerosMultiplos1(int x, int y) {

        if (x % y == 0) {
            return "O número " + x + " é múltiplo de " + y + ".";
        } else {
            return "O número " + x + " não é múltiplo de " + y + ".";
        }
    }

    public static String numerosMultiplos2(int x, int y) {

        if (y % x == 0) {
            return "O número " + y + " é múltiplo de " + x + ".";
        } else {
            return "O número " + y + " não é múltiplo de " + x + ".";
        }
    }

    public static String ordemCrescente(int numero) {

        int digito1, digito2, digito3;

        if (numero < 100 || numero > 999) {
            return "O número não tem três dígitos.";
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if (digito3 > digito2 && digito2 > digito1) {
                return "A sequência de algarismos é crecente";
            } else if (digito3 < digito2 && digito2 < digito1) {
                return "A sequência de algarismos é decrescente";
            } else {
                return "A sequência de algarismos não é crescente nem decrescente";
            }
        }
    }

    public static double calcDesconto(double preco) {
        double saldo, precoSaldo;

        if (preco <= 0) {
            return 0;
        }

        if (preco <= 50) {
            saldo = preco * 0.2;
        } else if (preco <= 100) {
            saldo = preco * 0.3;
        } else if (preco <= 200) {
            saldo = preco * 0.4;
        } else {
            saldo = preco * 0.6;
        }

        precoSaldo = preco - saldo;

        return precoSaldo;
    }

    public static String classifTurma(double aprovados) {

        if (aprovados < 0 || aprovados > 1) {
            return "Valor inválido";
        } else if (aprovados < 0.2) {
            return "Turma má";
        } else if (aprovados < 0.5) {
            return "Turma fraca";
        } else if (aprovados < 0.7) {
            return "Turma razoável";
        } else if (aprovados < 0.9) {
            return "Turma boa";
        } else {
            return "Turma excelente";
        }
    }

    public static void exercicio11() {
        double aprovados, limite1, limite2, limite3, limite4;
        String classificacao;

        Scanner scan = new Scanner(System.in);

        System.out.print("Introduza o valor de aprovados: ");
        aprovados = scan.nextDouble();

        System.out.print("Intervalo 1 de classificação (introduza um valor entre 0 e 1): ");
        limite1 = scan.nextDouble();

        System.out.print("Intervalo 2 de classificação (introduza um valor entre " + limite1 + " e 1): ");
        limite2 = scan.nextDouble();

        System.out.print("Intervalo 3 de classificação (introduza um valor entre " + limite2 + " e 1): ");
        limite3 = scan.nextDouble();

        System.out.print("Intervalo 4 de classificação (introduza um valor entre " + limite3 + " e 1): ");
        limite4 = scan.nextDouble();

        classificacao = classifTurma1(aprovados, limite1, limite2, limite3, limite4);

        System.out.println(classificacao);
    }

    public static String classifTurma1(double aprovados, double limite1, double limite2, double limite3, double limite4) {

        if ((limite1 < limite2 && limite2 < limite3 && limite3 < limite4) && (limite1 > 0 && limite1 < 1) && (limite2 > 0 && limite2 < 1) && (limite3 > 0 && limite3 < 1) && (limite4 > 0 && limite4 < 1)) {
            if (aprovados < 0 || aprovados > 1) {
                return "Valor inválido";
            } else if (aprovados < limite1) {
                return "Turma má";
            } else if (aprovados < limite2) {
                return "Turma fraca";
            } else if (aprovados < limite3) {
                return "Turma razoável";
            } else if (aprovados < limite4) {
                return "Turma boa";
            } else {
                return "Turma excelente";
            }
        } else {
            return "Os limites introduzidos não estão corretos";
        }
    }

    public static String nivelPoluicao(double indicePoluicao) {

        if (indicePoluicao < 0) {
            return "O indice de poluição introduzido não é válido. Deverá ser um valor positivo.";
        } else if (indicePoluicao >= 0 && indicePoluicao <= 0.3) {
            return "O indice de poluição é aceitável";
        } else if (indicePoluicao <= 0.4) {
            return "As indústrias do 1º grupo devem suspender as suas atividades.";
        } else if (indicePoluicao <= 0.5) {
            return "As indústrias do 1º e do 2º grupo devem suspender as suas atividades.";
        } else {
            return "As indústrias do 1º, 2º e do 3º grupo devem suspender as suas atividades.";
        }
    }

    public static double tempoServicoJardinagem(double area, int numero_arvores, int numero_arbustos) {
        double tempo_grama;
        double tempo_arvores;
        double tempo_arbustos;
        double tempo_total;

        tempo_grama = area * 300;
        tempo_arvores = numero_arvores * 600;
        tempo_arbustos = numero_arbustos * 400;

        tempo_total = (tempo_grama + tempo_arvores + tempo_arbustos) / 3600;

        return tempo_total;
    }

    public static double custoServicoJardinagem(double area, int numero_arvores, int numero_arbustos) {
        double custo_grama;
        double custo_arvores;
        double custo_arbustos;
        double custo_total;
        double custo_trabalho_hora;

        custo_grama = area * 10;
        custo_arvores = numero_arvores * 20;
        custo_arbustos = numero_arbustos * 15;
        custo_trabalho_hora = 10 * tempoServicoJardinagem(area, numero_arvores, numero_arbustos);


        custo_total = custo_grama + custo_arvores + custo_arbustos + custo_trabalho_hora;

        return custo_total;
    }

    public static double quilometrosPercorridosEstafeta(double dia1, double dia2, double dia3, double dia4, double dia5) {
        double media_dia_milhas;
        double media_dia_quilometros;

        media_dia_milhas = (dia1 + dia2 + dia3 + dia4 + dia5) / 5;
        media_dia_quilometros = media_dia_milhas / 1.609;

        return media_dia_quilometros;
    }

    public static String classificacaoTriangulos(double lado1, double lado2, double lado3) {
        if (lado1 > lado2 + lado3 || lado2 > lado1 + lado3 || lado3 > lado1 + lado2 || lado1 <= 0 || lado2 <= 0 || lado3 <= 0) {
            return "Não é possível construir um triangulo com os valores dados.";
        } else if (lado1 == lado2 && lado2 == lado3) {
            return "O triângulo é equilátero.";
        } else if (lado1 != lado2 && lado2 != lado3) {
            return "O triângulo é escaleno.";
        } else {
            return "O triângulo é isósceles.";
        }
    }

    public static String angulosTriangulos(double angulo1, double angulo2, double angulo3) {
        if (angulo1 <= 0 || angulo2 <= 0 || angulo3 <= 0 || (angulo1 + angulo2 + angulo3 != 180)) {
            return "Não é possível construir um triângulo com esses dados.";
        } else if (angulo1 == 90 || angulo2 == 90 || angulo3 == 90) {
            return "O triângulo é retângulo";
        } else if (angulo1 < 90 && angulo2 < 90 && angulo3 < 90) {
            return "O triângulo é acutângulo";
        } else {
            return "O triângulo é obtusângulo";
        }
    }

    public static int calculoChegadaComboioEmMinutos(int horaPartida, int minutoPartida, int duracaoHoras, int duracaoMinutos) {
        int tempoPartidaMinutos, tempoDuracaoMinutos, tempoChegadaMinutos;

        tempoPartidaMinutos = (horaPartida * 60) + minutoPartida;
        tempoDuracaoMinutos = (duracaoHoras * 60) + duracaoMinutos;

        tempoChegadaMinutos = tempoPartidaMinutos + tempoDuracaoMinutos;

        return tempoChegadaMinutos;
    }

    public static String chegadaComboioHoraMinuto(int horaPartida, int minutoPartida, int duracaoHoras, int duracaoMinutos) {
        int horaChegadaHoras, horaChegadaMinutos;
        double tempoChegadaMinutos;
        String tempoEDiaDeChegada, mensagem;

        tempoChegadaMinutos = calculoChegadaComboioEmMinutos(horaPartida, minutoPartida, duracaoHoras, duracaoMinutos);

        horaChegadaHoras = (int) (tempoChegadaMinutos / 60);
        horaChegadaMinutos = (int) (tempoChegadaMinutos % 60);

        if (horaChegadaHoras < 24) {
            mensagem = "O comboio chega hoje às ";
        } else {
            mensagem = "O comboio chega amanhã às ";
            horaChegadaHoras = horaChegadaHoras - 24;
        }

        tempoEDiaDeChegada = mensagem + "[" + horaChegadaHoras + " : " + horaChegadaMinutos + "]";

        return tempoEDiaDeChegada;
    }

    public static double conversaoTempoHorasSegundos(int horasInicio, int minutosInicio, int segundosInicio) {
        int horasParaSegundos, minutosParaSegundos, segundosIniciaisProcessamento;

        if (horasInicio < 0 || minutosInicio < 0 || segundosInicio < 0) {
            return 0;
        } else {
            horasParaSegundos = horasInicio * 3600;
            minutosParaSegundos = minutosInicio * 60;
            segundosIniciaisProcessamento = horasParaSegundos + minutosParaSegundos + segundosInicio;

            return segundosIniciaisProcessamento;
        }
    }

    public static String tempoProcessamento(int horasInicio, int minutosInicio, int segundosInicio, int segundosDuracao) {
        int segundosIniciaisProcessamento, tempoFinalSegundos, horaFinal, minutoFinal, segundoFinal;

        segundosIniciaisProcessamento = (int) conversaoTempoHorasSegundos(horasInicio, minutosInicio, segundosInicio);

        tempoFinalSegundos = segundosIniciaisProcessamento + segundosDuracao;

        horaFinal = tempoFinalSegundos / 3600;
        minutoFinal = (tempoFinalSegundos % 3600) / 60;
        segundoFinal = (tempoFinalSegundos % 3600) % 60;

        return "[" + horaFinal + " : " + minutoFinal + " : " + segundoFinal + "]";
    }

    public static double salarioEmpregado(int horas) {
        double valorHora, salario, numeroHorasSemana, valorMaximoExtra;

        valorHora = 7.5;
        numeroHorasSemana = 36;
        valorMaximoExtra = 50;

        if (horas < 36) {
            return 0;
        } else if (horas - numeroHorasSemana == 0) {
            salario = horas * valorHora;
        } else if ((horas - numeroHorasSemana) <= 5) {
            salario = ((horas - 36) * 10) + (numeroHorasSemana * valorHora);
        } else {
            salario = ((horas - 41) * 15) + (numeroHorasSemana * valorHora) + valorMaximoExtra;
        }
        return salario;
    }

    public static int valorAluguerJardinagem(String kit, String diaSemana) {
        int valorPagamento;

        valorPagamento = 0;

        switch (diaSemana) {
            case "Segunda-feira":
            case "Terça-feira":
            case "Quarta-feira":
            case "Quinta-feira":
            case "Sexta-feira":
                switch (kit) {
                    case "A":
                        valorPagamento = 30;
                        break;
                    case "B":
                        valorPagamento = 50;
                        break;
                    case "C":
                        valorPagamento = 100;
                        break;
                }
                break;

            case "Sábado":
            case "Domingo":
            case "Feriado":
                switch (kit) {
                    case "A":
                        valorPagamento = 40;
                        break;
                    case "B":
                        valorPagamento = 70;
                        break;
                    case "C":
                        valorPagamento = 140;
                        break;
                }
                break;
        }
        return valorPagamento;
    }
}